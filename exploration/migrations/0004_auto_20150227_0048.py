# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('exploration', '0003_remove_test_function_name'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='slide',
            options={'ordering': ['index']},
        ),
        migrations.AlterField(
            model_name='test',
            name='input_values',
            field=models.TextField(default='[]'),
        ),
        migrations.AlterField(
            model_name='test',
            name='output_values',
            field=models.TextField(default='[]'),
        ),
    ]
