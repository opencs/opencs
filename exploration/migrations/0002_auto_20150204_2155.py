# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('exploration', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='slide',
            name='replace_code',
            field=models.CharField(default='n', choices=[('n', 'do nothing'), ('r', 'replace code'), ('p', 'prepend to code'), ('a', 'append to code')], max_length=1),
        ),
    ]
