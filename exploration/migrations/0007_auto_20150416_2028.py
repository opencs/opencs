# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import exploration.models


class Migration(migrations.Migration):

    dependencies = [
        ('exploration', '0006_slide_allow_exception'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='slide',
            name='hint',
        ),
        migrations.AddField(
            model_name='slide',
            name='hints',
            field=exploration.models.ListField(default=[]),
            preserve_default=True,
        ),
    ]
