# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import exploration.models


class Migration(migrations.Migration):

    dependencies = [
        ('exploration', '0007_auto_20150416_2028'),
    ]

    operations = [
        migrations.CreateModel(
            name='ExplorationImage',
            fields=[
                ('id', models.AutoField(serialize=False, auto_created=True, primary_key=True, verbose_name='ID')),
                ('title', models.CharField(max_length=50)),
                ('image', models.ImageField(width_field='width', height_field='height', upload_to=exploration.models._upload_path)),
                ('width', models.IntegerField(editable=False)),
                ('height', models.IntegerField(editable=False)),
                ('slide', models.ForeignKey(to='exploration.Slide', related_name='image_set')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
    ]
