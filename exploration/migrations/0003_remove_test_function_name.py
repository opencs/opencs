# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('exploration', '0002_auto_20150204_2155'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='test',
            name='function_name',
        ),
    ]
