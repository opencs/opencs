# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('exploration', '0008_auto_20150520_1642'),
    ]

    operations = [
        migrations.AlterField(
            model_name='exploration',
            name='language',
            field=models.CharField(choices=[('python', 'Python'), ('web', 'Web Material')], max_length=6, default='python'),
        ),
    ]
