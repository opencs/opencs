# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('exploration', '0005_auto_20150304_2342'),
    ]

    operations = [
        migrations.AddField(
            model_name='slide',
            name='allow_exception',
            field=models.BooleanField(default=False),
            preserve_default=True,
        ),
    ]
