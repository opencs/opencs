# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Exploration',
            fields=[
                ('id', models.AutoField(auto_created=True, serialize=False, primary_key=True, verbose_name='ID')),
                ('title', models.CharField(max_length=50)),
                ('language', models.CharField(choices=[('python', 'Python')], max_length=6, default='python')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Slide',
            fields=[
                ('id', models.AutoField(auto_created=True, serialize=False, primary_key=True, verbose_name='ID')),
                ('index', models.SmallIntegerField()),
                ('text', models.TextField()),
                ('post_text', models.TextField(blank=True)),
                ('code', models.TextField(blank=True)),
                ('replace_code', models.BooleanField(default=True)),
                ('hint', models.TextField(blank=True)),
                ('exploration', models.ForeignKey(to='exploration.Exploration')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Test',
            fields=[
                ('id', models.AutoField(auto_created=True, serialize=False, primary_key=True, verbose_name='ID')),
                ('test_type', models.CharField(choices=[('stdio', 'standard in/out'), ('func', 'blackbox function'), ('cont', 'code contents')], max_length=5, default='stdio')),
                ('function_name', models.CharField(max_length=40)),
                ('input_values', models.TextField()),
                ('output_values', models.TextField()),
                ('slide', models.ForeignKey(to='exploration.Slide')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
    ]
