# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('exploration', '0008_explorationimage'),
        ('exploration', '0009_auto_20150601_1926'),
    ]

    operations = [
    ]
