# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('exploration', '0007_auto_20150416_2028'),
    ]

    operations = [
        migrations.CreateModel(
            name='CodeBlock',
            fields=[
                ('id', models.AutoField(primary_key=True, serialize=False, verbose_name='ID', auto_created=True)),
                ('filename', models.CharField(max_length=48, blank=True, default='')),
                ('filetype', models.CharField(max_length=4, default='html')),
                ('replace', models.CharField(choices=[('n', 'do nothing'), ('r', 'replace code'), ('p', 'prepend to code'), ('a', 'append to code')], max_length=1, default='r')),
                ('code', models.TextField(blank=True, default='')),
                ('show', models.BooleanField(default=True)),
                ('size', models.PositiveSmallIntegerField(default=12)),
                ('slide', models.ForeignKey(related_name='code_blocks', to='exploration.Slide')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.AddField(
            model_name='slide',
            name='left_pane_size',
            field=models.PositiveSmallIntegerField(default=6),
            preserve_default=True,
        ),
    ]
