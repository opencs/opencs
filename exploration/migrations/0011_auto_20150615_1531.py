# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import exploration.models


class Migration(migrations.Migration):

    dependencies = [
        ('exploration', '0010_merge'),
    ]

    operations = [
        migrations.AddField(
            model_name='slide',
            name='show_console',
            field=models.BooleanField(default=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='slide',
            name='show_rendered',
            field=models.BooleanField(default=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='slide',
            name='hints',
            field=exploration.models.ListField(blank=True, default=[]),
        ),
    ]
