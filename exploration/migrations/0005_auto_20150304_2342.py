# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('exploration', '0004_auto_20150227_0048'),
    ]

    operations = [
        migrations.AlterField(
            model_name='test',
            name='test_type',
            field=models.CharField(choices=[('stdio', 'standard in/out'), ('func', 'blackbox function'), ('cont', 'code contents'), ('ns', 'A numerical solution (in stdout)')], default='stdio', max_length=5),
        ),
    ]
