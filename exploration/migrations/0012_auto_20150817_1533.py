# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('exploration', '0011_auto_20150615_1531'),
    ]

    operations = [
        migrations.AlterField(
            model_name='codeblock',
            name='size',
            field=models.PositiveSmallIntegerField(default=6),
        ),
    ]
