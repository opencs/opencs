from django.contrib import admin

from courseware.admin import ModuleContentAdmin
from .models import Exploration, Slide, Test, CodeBlock

class SlideInline(admin.StackedInline):
    model = Slide
    show_change_link=True
    extra = 0



@admin.register(Exploration)
class ExplorationAdmin(ModuleContentAdmin):
    inlines = [SlideInline,]


class TestInline(admin.TabularInline):
    model = Test
    extra = 0

class CodeBlockInline(admin.TabularInline):
    model = CodeBlock
    extra = 0

@admin.register(Slide)
class SlideAdmin(admin.ModelAdmin):
    inlines = [
            TestInline,
            CodeBlockInline,
            ]
