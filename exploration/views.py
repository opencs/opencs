from django.shortcuts import render, get_object_or_404

from .models import Exploration, Slide

def index(request):
    exploration_list = Exploration.objects.all()
    return render(request, 'exploration/index.html', {'exploration_list': exploration_list})

def exploration(request, pk):
    exploration = get_object_or_404(Exploration, pk=pk)
    slides = exploration.slide_set.all()
    return render(request, 'exploration/exploration.html', {'exploration': exploration, 'slides': slides})
