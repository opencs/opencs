
"""
.. currentmodule:: courseware

The app to handle explorations, or guided coding exercises.

Each exploration is a collection of slides and test cases. The :class:`models.Slide` handles all data associated with an exercise within an exploration.

At the time of writing, only Python exercises are supported.
"""
