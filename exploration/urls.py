from django.conf.urls import patterns, url

from exploration import views

urlpatterns = patterns('', 
        url(r'^$', views.index, name='exploration/index'),
        url(r'^(?P<pk>\d+)/$', views.exploration, name='exploration/exploration'),
        )
