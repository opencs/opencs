from tastypie.resources import ModelResource
from tastypie import fields
from OpenCS.api import CamelCaseJSONSerializer

from exploration.models import Exploration, Slide, Test, CodeBlock

def register_to_api(api):
    """ Helper function to register these resources with an Api instance"""
    resources = [ExplorationResource, 
                 SlideResource, 
                 TestResource,
                 ]
    for resource in resources:
        api.register(resource())

def serialize_exploration(e):
    """ Serialize an exploration resource 

    This is particularly useful for injecting the data into a template for javascript
    """
    res = ExplorationResource()
    b = res.build_bundle(obj=e)
    data = res.full_dehydrate(b)
    return res.serialize(None, data, 'application/json')

class TestResource(ModelResource):
    class Meta:
        resource_name = 'exp-test'
        queryset = Test.objects.all()
        serializer = CamelCaseJSONSerializer()

class CodeBlockResource(ModelResource):
    class Meta:
        resource_name = 'exp-test'
        queryset = CodeBlock.objects.all()
        serializer = CamelCaseJSONSerializer()

class SlideResource(ModelResource):
    tests = fields.ToManyField(TestResource, 'test_set', full=True)
    codeBlocks = fields.ToManyField(CodeBlockResource, 'code_blocks', full=True)
    clear = fields.CharField(attribute='replace_code')
    hints = fields.ListField(attribute="hints")

    def dehydrate_codeBlocks(self, bundle):
        cbs = dict()
        for cb in bundle.data['codeBlocks']:
            cbs[cb.data['filetype']] = cb
        return cbs


    class Meta:
        resource_name = 'exp-slide'
        queryset = Slide.objects.all()
        excludes = ['replace_code',]
        serializer = CamelCaseJSONSerializer()

class ExplorationResource(ModelResource):
    # Setting full=True and full_list=False gives all the word detail in a detail request,
    # but only resource URI's for the list request
    slides = fields.ToManyField(SlideResource, 'slide_set', full=True, full_list=False)

    class Meta:
        resource_name = 'exploration'
        queryset = Exploration.objects.all()
        serializer = CamelCaseJSONSerializer(2)
