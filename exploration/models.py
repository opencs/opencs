from django.db import models

from django.contrib.contenttypes.fields import GenericRelation
from courseware.models import Module

# Use json to serialize the lists of things for tests
import json
import re
import ast

# XXX We need a list of hints, so here's a list field
# taken from http://stackoverflow.com/questions/5216162/how-to-create-list-field-in-django
class ListField(models.TextField, metaclass=models.SubfieldBase):

    """ A Python list field

    The data is serialized as a string, then evaluated with ast
    """

    __metaclass__ = models.SubfieldBase
    description = "Stores a python list"

    def __init__(self, *args, **kwargs):
        super(ListField, self).__init__(*args, **kwargs)

    def to_python(self, value):
        if not value:
            value = []

        if isinstance(value, list):
            return value

        converted =  ast.literal_eval(value)
        if not isinstance(converted, list):
            raise ValueError('value "%s" is not a list\n Original is %s' % (converted, repr(value)))
        return converted

    def get_prep_value(self, value):
        if value is None:
            return value

        return str(value)

    def value_to_string(self, obj):
        value = self._get_val_from_obj(obj)
        return self.get_db_prep_value(value)



class Exploration(models.Model):

    """A full multi-part code exploration

    There will be support for more languages in the future

    Attributes:
        language (CharField): one of ``["python",]``
        title (CharField): short title of the exploration (deprecated)
    """

    PYTHON = 'python'
    WEB = 'web'
    LANGUAGE_CHOICES = (
            (PYTHON, 'Python'),
            (WEB, 'Web Material'),
            )

    # Relations
    modules = GenericRelation(Module)

    # Attributes
    title = models.CharField(max_length=50)
    language = models.CharField(max_length=6, choices=LANGUAGE_CHOICES, default=PYTHON)

    # Functions
    def __str__(self):
        return str(self.title)

class Slide(models.Model):

    """One of the slides in an exploration
    
    Attributes:
        text (TextField): The slide's text contents
        post_text (TextField): The text to be added to the slide after running the code (presented as "notes)
        hints (ListField): A list of hints associated with the exercise
        allow_exception (BooleanField): If ``True``, the exercise is correct even with an exception. 
        left_pane_size (PositiveSmallIntegerField): relative size of the left pane, where ``6`` is 50%
        show_rendered (BooleanField): Whether the rendered pane should be shown
        show_console (BooleanField): Whether the console pane should be shown

        code (TextField): Default code for this slide
        replace_code (CharField): One of ``['n', 'r', 'p', 'a']`` for none, replace, prepend, append

    .e deprecated:: 0.3.0
        The ``code`` and ``replace_code`` attributes are deprecated. Use :class:`CodeBlock` instead.
    """

    NONE = 'n'
    REPLACE = 'r'
    PREPEND = 'p'
    APPEND = 'a'
    REPLACE_CHOICES = (
            (NONE,      'do nothing'),
            (REPLACE,   'replace code'),
            (PREPEND,   'prepend to code'),
            (APPEND,    'append to code'),
            )

    # Relations
    exploration = models.ForeignKey(Exploration)
    index = models.SmallIntegerField() # What order the slide is in in the exploration

    # Attributes
    text = models.TextField()
    post_text = models.TextField(blank=True)
    hints = ListField(default=[], blank=True)
    allow_exception = models.BooleanField(default=False)
    left_pane_size = models.PositiveSmallIntegerField(default=6)

    # These are going to be needed for the web course
    show_rendered = models.BooleanField(default=True)
    show_console = models.BooleanField(default=True)

    # XXX The following fields are to be deprecated in favour of ``CodeSample`` objects
    code = models.TextField(blank=True)
    replace_code = models.CharField(max_length=1, choices=REPLACE_CHOICES, default=NONE)
    
    # Functions
    def __str__(self):
        return "Slide " + str(self.index)
    
    _p_img = re.compile(r'(<img\s+alt="([^"]*)"\s+src=")([^"]*)')
    def save(self, *args, **kwargs):
        """
        Save the slide, first matching all the images to their source.

        The ``alt`` attribute for the ``<img>`` is matched against the ``SlideImage``'s ``title`` property
        """
        def _img_replace(match):
            "look up the src corresponding to the alt text"
            alt = match.group(2)
            src = self.image_set.filter(title=alt)
            if src:
                src = src[0].image.url
            else:
                src = ''
            return match.group(1) + src

        self.text = self._p_img.sub(_img_replace, self.text)
        self.post_text = self._p_img.sub(_img_replace, self.post_text)
        super().save(*args, **kwargs)

    class Meta:
        ordering = ['index']

# XXX I want this as a static method of ExplorationImage, but the name must be defined *before* the class definition
def _upload_path(instance, filename):
    """ Determine the path to upload an image to
    """
    return "images/exp-slides/%s/%s" % (instance.slide.exploration_id, filename)

class ExplorationImage(models.Model):

    """ An image to be included in a exploration slide

    Attributes:
        slide (Slide): The slide this image is used in
        title (CharField): The image title (to be matched with the ``alt`` attribute in the ``<img>`` tag)
        image (ImageField): The actual image
        width (IntegerField): width of the image
        height (IntegerField): height of the image
    """

    # Relations
    slide = models.ForeignKey(Slide, related_name='image_set')

    # Attributes
    title = models.CharField(max_length=50)
    image = models.ImageField(upload_to=_upload_path, 
                              height_field='height', width_field='width')
    width = models.IntegerField(editable=False)
    height = models.IntegerField(editable=False)

    # Functions
    def __str__(self):
        return self.title

class Test(models.Model):

    """ The test(s) associated with a slide

    Attributes:
        test_type (CharField): The type of test. One of ``['stdio', 'func', 'cont', 'ns']``
        input_values (TextField): Dependent on the ``test_type``:

            - list of input strings serialized in json for STDIO,
            - a function that tests the code for FUNCTION
            - list of necessary substrings serialized in json for CONTAINS
        output_values (TextField): Dependent on ``test_type``   

            - A list of strings for STDIO (paired with input_values), 
            - nothing for FUNCTION
            - list of illegal substrings for CONTAINS

        slide (ForeignKey): the slide the test is associated with
    """

    STDIO = 'stdio'
    FUNCTION = 'func'
    CONTAINS = 'cont'
    NUMSOL = 'ns'
    TEST_CHOICES = (
           (STDIO, 'standard in/out'),
            (FUNCTION, 'blackbox function'),
            (CONTAINS, 'code contents'),
            (NUMSOL, 'A numerical solution (in stdout)'),
            )

    # Relations
    slide = models.ForeignKey(Slide)

    # Attributes
    test_type = models.CharField(max_length=5, choices=TEST_CHOICES, default=STDIO)
    # XXX For now, we can let input/output values overload contains/notcontains
    input_values = models.TextField(default='[]')
    output_values = models.TextField(default='[]')

    # Controller functions
    def add_io(self, i, o):
        """ Add an stdio test case """
        if self.test_type == self.STDIO:
            iv = json.loads(self.input_values)
            ov = json.loads(self.output_values)
            iv.append(i)
            ov.append(o)
            self.input_values = json.dumps(iv)
            self.output_values = json.dumps(ov)
        else:
            raise Exception('Tried to set a test for the wrong test type')

    def add_contains(self, s):
        """ Add a 'contains' test case """
        if self.test_type == self.CONTAINS:
            iv = json.loads(self.input_values)
            iv.append(s)
            self.input_values = json.dumps(iv)
        else:
            raise Exception('Tried to set a test for the wrong test type')

    def add_omits(self, s):
        """ Add an 'omits' test case """
        if self.test_type == self.CONTAINS:
            ov = json.loads(self.output_values)
            ov.append(s)
            self.output_values = json.dumps(ov)
        else:
            raise Exception('Tried to set a test for the wrong test type')

    def set_numsol(self, s):
        """ Set the value for a numerical solution test"""
        if self.test_type == self.NUMSOL:
            self.output_values = s
        else:
            raise Exception('Tried to set a test for the wrong test type')

    def has_tests(self):
        """ Checks to see if there are actually tests associated with this object"""
        if self.test_type == self.FUNCTION:
            return not(self.input_values == '[]' )
        elif self.test_type == self.NUMSOL:
            return not(self.output_values == '[]' )
        else:
            return bool(json.loads(self.input_values) or json.loads(self.output_values))

    def set_function(self, s):
        """ set the testing suite for a function """
        if self.test_type == self.FUNCTION:
            self.input_values = s
        else:
            raise Exception('Tried to set a test for the wrong test type')

class CodeBlock(models.Model):
    """
    A code sample for use in html/css/js explorations

    .. note:: these will be used for all source-code in the future

    Attributes: 
        filename (CharField): the name of the 'file' for the source code (used in rendering html/css/js)
        filetype (CharField): Type of code, distinguished by standard extension: *py*, *html*, *css*, *js*
        replace (CharField): One of ``['n', 'r', 'p', 'a']`` for none, replace, prepend, append
        code (TextField): Default content of the code entry block
        show (BooleanField): Does this piece of code get shown?
        size (PositiveSmallIntegerField): relative size of the displayed pane, where ``6`` is default
    """
    
    NONE = 'n'
    REPLACE = 'r'
    PREPEND = 'p'
    APPEND = 'a'
    REPLACE_CHOICES = (
            (NONE,      'do nothing'),
            (REPLACE,   'replace code'),
            (PREPEND,   'prepend to code'),
            (APPEND,    'append to code'),
            )

    # relations:
    slide = models.ForeignKey(Slide, related_name="code_blocks")

    # attributes:
    filename = models.CharField(max_length=48, blank=True, default='')
    filetype = models.CharField(max_length=4, default='html')           # XXX 4 characters should cover all necessary extensions
    replace = models.CharField(max_length=1, choices=REPLACE_CHOICES, default=REPLACE)
    code = models.TextField(blank=True, default='')
    show = models.BooleanField(default=True)
    size = models.PositiveSmallIntegerField(default=6)




