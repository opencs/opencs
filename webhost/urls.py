from django.conf.urls import patterns, url

from . import views

# XXX csrf_exempt is used for testing. Remove from production if possible
from django.views.decorators.csrf import csrf_exempt

urlpatterns = [
        url(r'(?P<wid>[a-zA-Z\d_-]*)/$', csrf_exempt(views.WebContainerView.as_view()), name='webcontainer'),
        url(r'', csrf_exempt(views.WebContainerView.as_view()), name='webcontainer'),
        ]
