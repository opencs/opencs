from django.views.generic import View
from django.http import HttpResponse, HttpResponsePermanentRedirect

from django.conf import settings
import shutil
import os 
import os.path as path

import json

import hashlib
import time
import base64

class WebContainerView(View):
    """
    A view class to viewcreate or delete mini-websites
    """

    def get(self, request, *args, **kwargs):
        """
        This redirects to the webcontainer
        """
        try:
            wid = kwargs['wid']
        except KeyError as e:
            response = HttpResponse(status=405)
            response['allow'] = 'POST'
            return HttpResponse(status=405)

        if path.exists( path.join(settings.CONTAINER_ROOT, wid) ):
            url = self._create_url(wid)
            return HttpResponsePermanentRedirect(url)
        else:
            return HttpResponse(status=410)


    def delete(self, request, *args, **kwargs):
        """
        This handles deleting the webcontainer
        """
        try:
            wid = kwargs['wid']
        except KeyError as e:
            # This method is only supported if the request specifies an id
            response = HttpResponse(status=405)
            response['allow'] = 'POST'
            return HttpResponse(status=405)

        # This is the actual request handler
        folder = path.join(settings.CONTAINER_ROOT, wid)
        if path.isdir(folder):
            try:
                shutil.rmtree(folder)
                return HttpResponse(status=200)
            except:
                return HttpResponse(status=500)
        else:
            # If the directory isn't there, then it's permanently gone
            return HttpResponse(status=410)

    def post(self, request, *args, **kwargs):
        """
        Create a webcontainer (the most important response)
        """
        if 'wid' in kwargs:
            wid = kwargs['wid']
        else:
            wid = self._generate_wid(request)
        folder = path.join(settings.CONTAINER_ROOT, wid)
        try: 
            os.mkdir(folder)
        except (FileExistsError):
            wid = self._generate_wid(request)
            folder = path.join(settings.CONTAINER_ROOT, wid)
            # XXX If the folder still exists, then something bad probably happened
            os.mkdir(folder)
        try:
            data = json.loads(request.body.decode(request.encoding or 'utf-8'))
            index = None
            app = 'app.js'
            for f in data['files']:
                if path.splitext(f['name'])[1] == '.html':
                    index = f['name']
                    if 'addLog' in data and data['addLog']:
                        f['content'] = self._insert_logging(f['content'])
                if path.splitext(f['name'])[1] == '.js':
                    app = f['name']
                with open(path.join(folder, f['name']),'w') as handle:
                    handle.write(f['content'])
            if not index:
                # insert the default index
                index = 'index.html'
                print('adding default index')
                print(self._default_index)
                with open(path.join(folder, index),'w') as handle:
                    handle.write(self._default_index %(self._logging_script, app)) 
            url = self._create_url(wid, index)
            return HttpResponse(json.dumps({'id':wid, 'url':url}), status=200)
        except (Exception) as ex:
            print(ex)
            shutil.rmtree(folder)
            return HttpResponse('{"error": "Could not parse the request"}', status=400, content_type='application/json')

    def _create_url(self, wid, index='index.html'):
        # XXX on the current setup, it is served under MEDIA_ROOT, which doesn't allow indexing
        # index.html should be removed at some point
        return path.join(settings.CONTAINER_URL, wid, index)

    def _generate_wid_alternate(self, request):
        """
        Generates a unique id based on the request, and system time.

        It turns out the 2-byte precision used in the timestamp wasn't enough to handle a
            curl -X POST -d @test.json <url> & 
            curl -X POST -d @test.json <url>
        attempt.
        """
        ts = int(time.time() * 256)
        # the two most insignificant bytes of the timestamp are used to pad out
        # the hash to 18 bytes, which means no padding symbols are needed in
        # base64 encoding. The precision of the timestamp should prevent any
        # hash collisions from running the same demo
        digest = hashlib.md5(request.body).digest() + ts.to_bytes(8, 'little')[:2]
        return base64.urlsafe_b64encode(digest).decode('utf-8')

    def _generate_wid(self, request):
        """
        Generates a unique id based on the request, and system time.

        This just adds the timestamp to the content, and strips the padding,
        rather than use the timestamp as padding.
        """
        ts = str(time.time())
        # by concatenating the timestamp to the request body, it is made unique
        digest = hashlib.md5(request.body + bytes(ts, 'utf-8')).digest()
        # The padding characters are superfluous, because it will always be 16-byte
        return base64.urlsafe_b64encode(digest).strip(b'=').decode('utf-8')

    def _insert_logging(self, html):
        (pre, post) = html.split('<head>', 1)
        return pre + '<head>' + self._logging_script + post

    _logging_script = """
    <script>
    console.log('All console.log calls for this frame will now be directed to the parent window.\\nUse of console.debug is unaffected');
    console.log = function () {
        // Note: `postMessage` uses the [structured clone algorithm](https://developer.mozilla.org/en-US/docs/Web/Guide/API/DOM/The_structured_clone_algorithm) 
        // attempting to log a `Function` or `Error` object will raise an exception.
        // However, we can actually write to the parent window's namespace
        // using a shared memory scheme like this eliminates the need for cloning.
        args = Array.prototype.slice.call(arguments);
        window.top.consoleBuffer.push(args);
        window.top.postMessage({type: 'log'}, '*');
    };
    </script>
    """

    _default_index = """
    <!DOCTYPE html>
    <html lang="en">
      <head>
      %s
        <meta charset="utf-8">
      </head>
      <body>
      <script src="%s"></script>
      </body>
    </html>
    """
