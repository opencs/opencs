WARNING: use USE: https://git.uwaterloo.ca/opencs/opencs-full
====

this project was migrated to opencs-full that combines some projects to make things more simple.


OpenCS
======

auto generated docs: https://open.cs.uwaterloo.ca/documentation


This is the main website for the OpenCS project. The main component is a Django-based website. There is the additional ['python-socket'](https://git.uwaterloo.ca/opencs/python-socket) which is a websocket that handles Python execution requests.

Course materials, and various specific components are all held in the [OpenCS group](https://git.uwaterloo.ca/groups/opencs) at the University of Waterloo git host.

# Installation for local development

This site does depend on several submodules and Python 3.4. To get the freshly cloned repo into a working state, run

    git submodule update --init

and install the python dependencies (optionally in a virtualenv)

    pip install -r requirements.txt

The database will have to be initialized, and an admin account can be added:

    python manage.py migrate
    python manage.py createsuperuser

The development version can then be run by executing

    python python-server/server.py &
    python manage.py runserver --settings=OpenCS.settings.dev

Because this localhost setup is the primary dev environment, we have made it the default, and the `--settings` flag is optional. Otherwise, there would be a lot of typing.

Finally, flat pages should be created so that some links do not produce 404 errors. These pages can be found in the [opencs-materials repo](https://git.uwaterloo.ca/opencs/opencs-materials)i in the `resources` directory.

    python manage.py update-pages <page-file.md>

**Note (2015-04-22)**: In the local dev environment, the `python-socket` service is expected to be at `localhost:7000`. This can be changed in the settings file by setting the `PYSOCK_SUFFIX` variable.

# Deployment notes

A file `config.py` must be populated with values for the database, and other components. An `example_config.py` file is provided for reference.

Reference uWSGI and Nginx config files are respectively,

* `opencs.ini`
* `opencs_nginx.conf`

The python-socket service is located in its own [repository](https://git.uwaterloo.ca/opencs/python-socket). Details of how to set it up are in that repo.

For Nginx config, copy the contents of `opencs_nginx.conf` into the appropriate location of `/etc/nginx/` and change directories, urls, and ports as required.

A postgresql database for opencs will need to be created. The permissions on the database must match the config variables set in `config.py`.

After all of that is set up, a few commands need to be run to initialize the django database and put the static files in the proper location,

    python3 manage.py migrate --settings=OpenCS.settings.production
    python3 manage.py collectstatic --settings=OpenCS.settings.production

On the initial install, there may need to be some extra setup. First, to create any admin users (for the online admin)

    python manage.py createsuperuser --settings=OpenCS.settings.production
    
Next, content can be added with the `import-exploration`, `import-videos`, `import-quickclick`, `import-glossary`, and `update-pager` commands for the manage.py script. The course content (apart from videos) is located in the [opencs-materials](https://git.uwaterloo.ca/opencs/opencs-materials) repo. 

The scripts in the `scripts` directory are nice for convenience, handling file globbing, and reducing the amount of typing. On our deployment, they are put in the user's path.


# rendering CSS

install: `https://github.com/stylus/stylus`
```
pacman -Syu npm
apt-get install npm
npm install stylus -g
```

compile styl file to css
```
cd OpenCS/static
stylus style.styl
```
