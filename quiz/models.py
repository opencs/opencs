from django.db import models

from django.contrib.contenttypes.fields import GenericRelation
from courseware.models import Module

import re

class Quiz(models.Model):

    """
    Quiz model representing a multiple choice quiz
    
    Attributes:
        title (CharField): Intended as a description of the quiz. It is not used in the final product (deprecated)
        question_set (Manager(Question)): The set of questions in the quiz.
    """

    # Relations
    modules = GenericRelation(Module)

    # Attributes
    title = models.CharField(max_length=128)

    # Methods
    def all_questions(self):
        """Return a query of all the questions"""
        return self.question_set.all()

    def random_questions(self, number):
        """Return a list containing number random questions"""
        qs = list(self.question_set.all())
        return random.sample(qs, number)

    def __str__(self):
        return self.title

    class Meta:
        verbose_name = 'Quick-Click'

class Question(models.Model):

    """
    Question model for quizes.

    Each question is matched to a Quiz. 

    Attributes:
        quiz (Quiz): The quiz this question is in
        index (SmallIntegerField): The order of the question in the quiz.
        text (TextField): The text that will be displayed for the question.
        choice_set (Manager(Choice)): The set of choices for this question.
        answer_type (CharField) - The type of answer that is accepted. Can be one of:
            * ``'1'``: Single answer (multiple choice)
            * ``'M'``: Multiple answer (mulitple choice)
            * ``'S'``: Short-form answer (i.e. text input)
    """

    # Types of questions
    SINGLE = '1'
    MULTI  = 'M'
    SHORT  = 'S'
    TYPE_CHOICES = (
            (SINGLE, 'Single Answer'),
            (MULTI, 'Muliple Answer'),
            (SHORT, 'Short Form Answer'),
            )

    # Relations
    quiz = models.ForeignKey(Quiz)
    index = models.SmallIntegerField(default=0) # What order the questions are in

    # Attributes
    text = models.TextField()
    answer_type = models.CharField(max_length=1, choices=TYPE_CHOICES, default=SINGLE)

    # Methods
    def __str__(self):
        return self.text

    _p_img = re.compile(r'(<img\s+alt="([^"]*)"\s+src=")([^"]*)')
    def save(self, *args, **kwargs):
        """
        Save the question, first matching all the images to their source.

        The ``alt`` attribute for the ``<img>`` is matched against the ``QuizImage``'s ``title`` property
        """
        def _img_replace(match):
            "look up the src corresponding to the alt text"
            alt = match.group(2)
            src = self.image_set.filter(title=alt)
            if src:
                src = src[0].image.url
            else:
                src = ''
            return match.group(1) + src

        self.text = self._p_img.sub(_img_replace, self.text)
        super().save(*args, **kwargs)

    class Meta:
        ordering = ['index']

# XXX I want this as a static method of QuizImage, but the name must be defined *before* the class definition
def _upload_path(instance, filename):
    """ Determine the path to upload an image to
    """
    return "images/quiz/%s/%s" % (instance.question.quiz_id, filename)

class QuizImage(models.Model):

    """ An image to be included in a exploration slide

    Attributes:
        question (Question): The question this image is used in
        title (CharField): The image title (to be matched with the ``alt`` attribute in the ``<img>`` tag)
        image (ImageField): The actual image
        width (IntegerField): width of the image
        height (IntegerField): height of the image
    """

    # Relations
    question = models.ForeignKey(Question, related_name='image_set')

    # Attributes
    title = models.CharField(max_length=50)
    image = models.ImageField(upload_to=_upload_path, 
                              height_field='height', width_field='width')
    width = models.IntegerField(editable=False)
    height = models.IntegerField(editable=False)

    # Functions
    def __str__(self):
        return self.title

class Choice(models.Model):

    """
    One Choice for a Question.

    .. note: This model is overloaded to also handle short-form questions. The original design only permitted multiple choice. 
        As a result, the data is interpretted differently depending on question type. For short-form answers, the ``text`` field is the correct answer, and ``is_correct`` has no use.


    Attributes: 
        text (TextField): The text that will be displayed. **NB** if the question is a short-form answer, then this is the correct answer
        is_correct (BooleanField): whether this choice is a correct answer to question
        hint (TextField): The hint (to be displayed if the user's answer is incorrect).
        explanation (TextField): The explanation (to be displayed if the user's answer is correct).
    """

    # Relations
    question = models.ForeignKey(Question)
    index = models.SmallIntegerField(default=0) # What order the choices are in

    # Attributes
    text = models.TextField()
    is_correct = models.BooleanField(default=False)
    hint = models.TextField(blank=True, default="")
    explanation = models.TextField(blank=True, default="")

    # Methods
    def __str__(self):
        return self.text

    class Meta:
        ordering = ['index']
