
"""
The django-app to handle quizzes (or quick clicks).

A quiz is simply a collection of questions. Each question can be single-answer, multiple-choice, or a shortform answer. The choices for each question are stored in their own model, so that a question can have multiple answers.

.. note:: In some ways it may have been more efficient to have list-fields on each question for the choices/answers. However, using the choice object is more extensible. For example, a hint and explanation had to be connected to each choice.
"""
