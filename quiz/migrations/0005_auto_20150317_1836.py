# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('quiz', '0004_auto_20150305_2017'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='quiz',
            options={'verbose_name': 'Quick-Click'},
        ),
        migrations.AlterField(
            model_name='choice',
            name='text',
            field=models.TextField(),
        ),
    ]
