# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('quiz', '0001_initial'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='choice',
            options={'ordering': ['index']},
        ),
        migrations.AlterModelOptions(
            name='question',
            options={'ordering': ['index']},
        ),
        migrations.AddField(
            model_name='choice',
            name='index',
            field=models.SmallIntegerField(default=0),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='question',
            name='index',
            field=models.SmallIntegerField(default=0),
            preserve_default=True,
        ),
    ]
