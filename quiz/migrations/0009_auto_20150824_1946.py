# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('quiz', '0008_quizimage'),
    ]

    operations = [
        migrations.RenameField(
            model_name='quiz',
            old_name='text',
            new_name='title',
        ),
    ]
