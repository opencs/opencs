# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import quiz.models


class Migration(migrations.Migration):

    dependencies = [
        ('quiz', '0007_auto_20150415_1605'),
    ]

    operations = [
        migrations.CreateModel(
            name='QuizImage',
            fields=[
                ('id', models.AutoField(primary_key=True, serialize=False, auto_created=True, verbose_name='ID')),
                ('title', models.CharField(max_length=50)),
                ('image', models.ImageField(height_field='height', upload_to=quiz.models._upload_path, width_field='width')),
                ('width', models.IntegerField(editable=False)),
                ('height', models.IntegerField(editable=False)),
                ('question', models.ForeignKey(related_name='image_set', to='quiz.Question')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
    ]
