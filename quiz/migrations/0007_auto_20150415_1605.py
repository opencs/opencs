# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('quiz', '0006_auto_20150317_1839'),
    ]

    operations = [
        migrations.AddField(
            model_name='choice',
            name='explanation',
            field=models.TextField(blank=True, default=''),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='choice',
            name='hint',
            field=models.TextField(blank=True, default=''),
        ),
    ]
