# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('quiz', '0005_auto_20150317_1836'),
    ]

    operations = [
        migrations.AlterField(
            model_name='choice',
            name='hint',
            field=models.TextField(blank=True),
        ),
    ]
