# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('quiz', '0002_auto_20150227_0048'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='question',
            name='single_answer',
        ),
        migrations.AddField(
            model_name='question',
            name='answer_type',
            field=models.CharField(choices=[('1', 'Single Answer'), ('M', 'Muliple Answer'), ('S', 'Short Form Answer')], max_length=1, default='1'),
            preserve_default=True,
        ),
    ]
