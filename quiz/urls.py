from django.conf.urls import patterns, url

from quiz import views

urlpatterns = patterns('', 
        url(r'^$', views.index, name='quiz/index'),
        url(r'^(?P<pk>\d+)/$', views.quiz, name='quiz/quiz'),
        )
