from django.contrib import admin

from courseware.admin import ModuleContentAdmin
from .models import Quiz, Question, Choice

class QuestionInline(admin.StackedInline):
    model = Question

@admin.register(Quiz)
class QuizAdmin(ModuleContentAdmin):
    inlines = [QuestionInline,]


