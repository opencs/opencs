from django.shortcuts import render, get_object_or_404, get_list_or_404
from django.http import HttpResponse

from quiz.models import Quiz, Question, Choice

def index(request):
    quiz_list = Quiz.objects.all()
    return render(request, 'quiz/index.html', {'quiz_list': quiz_list})

def quiz(request, pk):
    quiz = get_object_or_404(Quiz, pk=pk)
    questions = quiz.all_questions()
    return render(request, 'quiz/quiz.html', {'quiz': quiz, 'question_list': questions})

