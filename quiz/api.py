# REST-api file for glossary

from tastypie.resources import ModelResource
from tastypie import fields
from OpenCS.api import CamelCaseJSONSerializer


from quiz.models import Quiz, Question, Choice

def register_to_api(api):
    """ Helper function to register these resources with an Api instance"""
    resources = [ChoiceResource, 
                 QuestionResource, 
                 QuizResource,
                 ]
    for resource in resources:
        api.register(resource())

def serialize_quiz(quiz):
    """ Serialize a quiz resource """
    res = QuizResource()
    b = res.build_bundle(obj=quiz)
    data = res.full_dehydrate(b)
    return res.serialize(None, data, 'application/json')

class ChoiceResource(ModelResource):

    class Meta:
        queryset = Choice.objects.all()
        serializer = CamelCaseJSONSerializer()

class QuestionResource(ModelResource):
    choices = fields.ToManyField(ChoiceResource, 'choice_set', full=True)
    # XXX singleAnswer field is included for backwards compatibility
    single_answer = fields.CharField(attribute='answer_type') 

    class Meta:
        queryset = Question.objects.all()
        serializer = CamelCaseJSONSerializer()

class QuizResource(ModelResource):
    # Setting full=True and full_list=False gives all the word detail in a detail request,
    # but only resource URI's for the list request
    # TODO: Maybe set use_in=detail if there's a performance problem with the list request
    questions = fields.ToManyField(QuestionResource, 'question_set', full=True, full_list=False)

    class Meta:
        queryset = Quiz.objects.all()
        serializer = CamelCaseJSONSerializer()
