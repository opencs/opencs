from django.contrib import admin
from django.utils.html import format_html
from django.core.urlresolvers import reverse

from courseware.admin import ModuleContentAdmin
from .models import SlideShow, Slide, SlideImage, VideoLesson


class ImageInline(admin.TabularInline):
    model = SlideImage
    extra = 0

@admin.register(Slide)
class SlideAdmin(admin.ModelAdmin):
    inlines = [ImageInline,]

class SlideInline(admin.TabularInline):
    fields = [
            'index',
            'text',
            ]

    model = Slide
    show_change_link=True
    extra = 0


@admin.register(VideoLesson)
class VideoAdmin(ModuleContentAdmin):
    pass

@admin.register(SlideShow)
class SlideShowAdmin(ModuleContentAdmin):
    inlines = [
            SlideInline,
            ]
    pass

