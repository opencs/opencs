from tastypie.resources import ModelResource
from tastypie import fields

from .models import SlideShow, Slide, VideoLesson

def register_to_api(api):
    """ Helper function to register these resources with an Api instance"""
    resources = [SlideShowResource,
                 SlideResource, 
                 VideoLessonResource, 
                 ]
    for resource in resources:
        api.register(resource())

def serialize_slideshow(e):
    """ Serialize a slideshow resource 

    This is particularly useful for injecting the data into a template for javascript
    """
    res = SlideShowResource()
    b = res.build_bundle(obj=e)
    data = res.full_dehydrate(b)
    return res.serialize(None, data, 'application/json')

class SlideResource(ModelResource):
    class Meta:
        queryset = Slide.objects.all()

class SlideShowResource(ModelResource):
    # Setting full=True and full_list=False gives all the word detail in a detail request,
    # but only resource URI's for the list request
    slides = fields.ToManyField(SlideResource, 'slide_set', full=True, full_list=False)

    class Meta:
        queryset = SlideShow.objects.all()

class VideoLessonResource(ModelResource):
    class Meta:
        queryset = VideoLesson.objects.all()
