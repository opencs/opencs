# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('slides', '0006_videolesson_srt'),
    ]

    operations = [
        migrations.RenameField(
            model_name='videolesson',
            old_name='srt',
            new_name='vtt',
        ),
    ]
