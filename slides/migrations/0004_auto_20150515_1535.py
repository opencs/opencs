# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('slides', '0003_auto_20150227_0048'),
    ]

    operations = [
        migrations.AlterField(
            model_name='slide',
            name='slideshow',
            field=models.ForeignKey(null=True, to='slides.SlideShow', blank=True),
        ),
    ]
