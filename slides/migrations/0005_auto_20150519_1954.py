# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('slides', '0004_auto_20150515_1535'),
    ]

    operations = [
        migrations.AlterField(
            model_name='slide',
            name='index',
            field=models.SmallIntegerField(default=0),
        ),
        migrations.AlterField(
            model_name='slideimage',
            name='slide',
            field=models.ForeignKey(to='slides.Slide', related_name='image_set'),
        ),
    ]
