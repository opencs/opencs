# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('slides', '0007_auto_20150709_1859'),
    ]

    operations = [
        migrations.AddField(
            model_name='videolesson',
            name='slides',
            field=models.FileField(null=True, blank=True, upload_to='videos/slides'),
        ),
        migrations.AddField(
            model_name='videolesson',
            name='transcript',
            field=models.TextField(blank=True),
        ),
        migrations.AlterField(
            model_name='videolesson',
            name='src',
            field=models.FileField(upload_to='videos'),
        ),
        migrations.AlterField(
            model_name='videolesson',
            name='vtt',
            field=models.FileField(null=True, blank=True, upload_to='videos'),
        ),
    ]
