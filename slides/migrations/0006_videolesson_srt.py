# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('slides', '0005_auto_20150519_1954'),
    ]

    operations = [
        migrations.AddField(
            model_name='videolesson',
            name='srt',
            field=models.FileField(upload_to='video', blank=True, null=True),
            preserve_default=True,
        ),
    ]
