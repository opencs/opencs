from django.db import models

from django.contrib.contenttypes.fields import GenericRelation
from courseware.models import Module

import re


class SlideShow(models.Model):

    """ A simple slideshow, specifically for an instructional segment

    Attributes:
        title (CharField): The displayed title of the slideshow
        slide_set (QueryManager): The slides in the show
    """
    # Relations
    modules = GenericRelation(Module)

    # Attributes
    title = models.CharField(max_length=50)

    def __str__(self):
        return self.title


class Slide(models.Model):

    """ One of the slides in a slideshow
    
    Attributes:
        index (SmallIntegerField): Which order the slide appears in
        text (TextField): The slide's text contents (in html)
        image_set (QueryManager): The images associated with the slide
    """

    # Relations
    slideshow = models.ForeignKey(SlideShow, blank=True, null=True)
    index = models.SmallIntegerField(default=0)

    # Attributes
    text = models.TextField()
    
    # Functions
    def __str__(self):
        return "Slide " + str(self.index)
    
    class Meta:
        ordering = ['index']


    _p_img = re.compile(r'(<img\s+alt="([^"]*)"\s+src=")([^"]*)')
    def save(self, *args, **kwargs):
        """
        Save the slide, first matching all the images to their source.

        The ``alt`` attribute for the ``<img>`` is matched against the ``SlideImage``'s ``title`` property
        """
        def _img_replace(match):
            "look up the src corresponding to the alt text"
            alt = match.group(2)
            src = self.image_set.filter(title=alt)
            if src:
                src = src[0].image.url
            else:
                src = ''
            return match.group(1) + src

        self.text = self._p_img.sub(_img_replace, self.text)
        super().save(*args, **kwargs)

        


# XXX I want this as a static method of SlideImage, but the name must be defined *before* the class definition
def _upload_path(instance, filename):
    """ Determine the path to upload an image to
    """
    return "images/slides/%s/%s" % (instance.slide.slideshow_id, filename)

class SlideImage(models.Model):

    """ An image to be included in a slide

    Attributes:
        slide (Slide): The slide this image is used in
        title (CharField): The image title (to be matched with the ``alt`` attribute in the ``<img>`` tag)
        image (ImageField): The actual image
        width (IntegerField): width of the image
        height (IntegerField): height of the image
    """

    # Relations
    slide = models.ForeignKey(Slide, related_name='image_set')

    # Attributes
    title = models.CharField(max_length=50)
    image = models.ImageField(upload_to=_upload_path, 
                              height_field='height', width_field='width')
    width = models.IntegerField(editable=False)
    height = models.IntegerField(editable=False)

    # Functions
    def __str__(self):
        return self.title

class VideoLesson(models.Model):

    """ A video lesson (instead of a slide show)

    Attributes:
        title (CharField): Title of the lesson (unused)
        src (FileField):   File for the video
        vtt (FileField):   The .vtt file for the video's subtitles
        slides (FileField):   A .pdf file of the slides used in the video
        transcript (TextField):   An html document to be used as the descriptive transcript
    """

    # Relations
    modules = GenericRelation(Module)

    # Attributes
    title = models.CharField(max_length=50)
    src = models.FileField(upload_to="videos")
    vtt = models.FileField(upload_to="videos", blank=True, null=True)
    slides = models.FileField(upload_to="videos/slides", blank=True, null=True)
    transcript = models.TextField(blank=True)

    def __str__(self):
        return self.title
