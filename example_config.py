# Configuration variables which should not be stored in the source tree
# or may change for different deployments

# the secret key is used by Django to prevent CSRF attacks, among other things
SECRET_KEY = ''

# These variables define the database hostname and passwords respectively
DB_HOST = ""
DB_NAME = ""
DB_USER = ""
DB_PASS = ""
DB_PORT = 5432

# These variables are server specific, and the user must have write access
STATIC_ROOT = ''
MEDIA_ROOT = ''

# These variables are similar to the above, but for the web container service
CONTAINER_ROOT = ''
CONTAINER_URL = ''

# For google analytics
ANALYTICS = 'analytics_key'
