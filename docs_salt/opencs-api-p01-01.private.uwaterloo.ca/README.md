
__Short description__: OpenCS API. The API does the heavy lifting when users run custom python code.





For more help, please see: https://git.uwaterloo.ca/opencs/opencs
        


Configuration Managment
===============================================================================

```
####################################################
fqdn: opencs-api-p01-01.private.uwaterloo.ca
os: Ubuntu
osfinger: Ubuntu-16.04
mem_total: 2000MB
num_cpus: 2
ipv4: ['127.0.0.1', '172.27.13.27']
master: salt.math.private.uwaterloo.ca
####################################################
```

This system is fully or partly managed using Salt.

The following sections are a rendered view of what the configuration managment system
manages on this system. Each item is handled in order from top to bottom unless some prequsits like `require`
or `after changes, run or update` force other ordering.




`file: uwl.hosts`
-----------------------------------------------------------------
 * state: file.managed
 * name: `/etc/hosts`


file stat /etc/hosts
```
gid: 0
group: root
mode: '0644'
size: 266
uid: 0
user: root
```
file data /etc/hosts
```
# __global
# define ipv4 after ipv6, so the ipv4 address is used when referencing localhost
::1                 localhost localhost.localdomain localhost6 localhost6.localdomain6
127.0.0.1           localhost localhost.localdomain localhost4 localhost4.localdomain4

```

`locale: _common.locale en_US.UTF-8`
-----------------------------------------------------------------
 * state: locale.present
 * name: `en_US.UTF-8`



`locale: _common.locale set default`
-----------------------------------------------------------------
 * state: locale.system
 * name: `en_US.UTF-8`

require:
 * [locale: _common.locale en_US.UTF-8](#locale-_commonlocale-en_USUTF-8)



`timezone: _common.timezone`
-----------------------------------------------------------------
 * state: timezone.system
 * name: `America/Toronto`


```
utc: true
```

`service: uwl.systemd-timesyncd`
-----------------------------------------------------------------
 * state: service.running
 * name: `systemd-timesyncd`

run or update after changes in:
 * [file: _common.ntp.systemd-timesyncd](#file-_commonntpsystemd-timesyncd)


```
enable: true
```

`file: _common.ntp.systemd-timesyncd`
-----------------------------------------------------------------
 * state: file.managed
 * name: `/etc/systemd/timesyncd.conf`

after changes, run or update:
 * [service: uwl.systemd-timesyncd](#service-uwlsystemd-timesyncd)


file stat /etc/systemd/timesyncd.conf
```
gid: 0
group: root
mode: '0644'
size: 79
uid: 0
user: root
```
file data /etc/systemd/timesyncd.conf
```
[Time]
NTP=ntp1.uwaterloo.ca ntp2.uwaterloo.ca ntp3.uwaterloo.ca
#FallbackNTP=

```

`pkg: uwl.tls package`
-----------------------------------------------------------------
 * state: pkg.installed
 * name: `uwl.tls package`

required in:
 * [cmd: uwl.tls CA update](#cmd-uwltls-CA-update)



```
install: ['ca-certificates']
```

`cmd: uwl.tls CA update`
-----------------------------------------------------------------
 * state: cmd.wait
 * name: `update-ca-certificates`

run or update after changes in:
 * [file: uwl.tls.file GlobalSign_Non-Public.pem](#file-uwltlsfile-GlobalSign_Non-Publicpem)

require:
 * [pkg: uwl.tls package](#pkg-uwltls-package)



`file: uwl.tls.file GlobalSign_Non-Public.pem`
-----------------------------------------------------------------
 * state: file.managed
 * name: `/usr/local/share/ca-certificates/GlobalSign_Non-Public.pem`

after changes, run or update:
 * [cmd: uwl.tls CA update](#cmd-uwltls-CA-update)


file stat /usr/local/share/ca-certificates/GlobalSign_Non-Public.pem
```
gid: 0
group: root
mode: '0644'
size: 3004
uid: 0
user: root
```
file data /usr/local/share/ca-certificates/GlobalSign_Non-Public.pem
```
XXXXXXX CERTIFICATE-----

XXXXXXX CERTIFICATE-----


```

`service: uwl.systemd-journald`
-----------------------------------------------------------------
 * state: service.running
 * name: `systemd-journald`

run or update after changes in:
 * [file: uwl.systemd-journald.config](#file-uwlsystemd-journaldconfig)


```
enable: true
```

`file: uwl.systemd-journald.config`
-----------------------------------------------------------------
 * state: file.managed
 * name: `/etc/systemd/journald.conf`

after changes, run or update:
 * [service: uwl.systemd-journald](#service-uwlsystemd-journald)


file stat /etc/systemd/journald.conf
```
gid: 0
group: root
mode: '0644'
size: 65
uid: 0
user: root
```
file data /etc/systemd/journald.conf
```
[Journal]
Storage=persistent
SystemMaxUse=2G
ForwardToSyslog=yes

```

`pkg: uwl.rsyslog`
-----------------------------------------------------------------
 * state: pkg.installed
 * name: `uwl.rsyslog`



```
install: ['rsyslog']
```

`service: uwl.rsyslog`
-----------------------------------------------------------------
 * state: service.running
 * name: `rsyslog`

run or update after changes in:
 * [pkg: uwl.rsyslog](#pkg-uwlrsyslog)
 * [file: uwl.rsyslog.config](#file-uwlrsyslogconfig)


```
enable: true
```

`file: uwl.rsyslog.config`
-----------------------------------------------------------------
 * state: file.managed
 * name: `/etc/rsyslog.d/99-salt.conf`

after changes, run or update:
 * [service: uwl.rsyslog](#service-uwlrsyslog)


file stat /etc/rsyslog.d/99-salt.conf
```
gid: 0
group: root
mode: '0644'
size: 185
uid: 0
user: root
```
file data /etc/rsyslog.d/99-salt.conf
```
## ist
#*.*   @@relentless.private.uwaterloo.ca:514
#*.*   @@retribution.private.uwaterloo.ca:514
## mfcf
#*.*  @@syslog.math.uwaterloo.ca:514
*.*   @@log.math.private.uwaterloo.ca:514

```

`pkg: uwl.telegraf`
-----------------------------------------------------------------
 * state: pkg.latest
 * name: `uwl.telegraf`


```
pkgs:
- telegraf
```

`service: uwl.telegraf`
-----------------------------------------------------------------
 * state: service.running
 * name: `telegraf`

run or update after changes in:
 * [pkg: uwl.telegraf](#pkg-uwltelegraf)
 * [file: uwl.telegraf.config](#file-uwltelegrafconfig)


```
enable: true
```

`file: uwl.telegraf.config`
-----------------------------------------------------------------
 * state: file.managed
 * name: `/etc/telegraf/telegraf.conf`

after changes, run or update:
 * [service: uwl.telegraf](#service-uwltelegraf)

require:
 * [pkg: uwl.telegraf](#pkg-uwltelegraf)


file stat /etc/telegraf/telegraf.conf
```
gid: 0
group: root
mode: '0644'
size: 1498
uid: 0
user: root
```
file data /etc/telegraf/telegraf.conf
```
# 0
# 00_global
[global_tags]
# 05_agent
[agent]
    interval = "60s"
    round_interval = true
    metric_batch_size = 1000
    metric_buffer_limit = 10000
    collection_jitter = "5s"
    flush_interval = "10s"
    flush_jitter = "5s"
    precision = ""
    debug = false
    quiet = false
    logfile = ""
    hostname = "opencs-api-p01-01.private.uwaterloo.ca"
    omit_hostname = false
# 10
# 10_outputs
# influxdb_math
[[outputs.influxdb]]
    urls = ["https://influxdb.math.uwaterloo.ca:8086"]
    database = "math"
    retention_policy = ""
    write_consistency = "any"
    timeout = "5s"
    username = "telegraf"
    password = XXXXXXX
# influxdb_test
#[[outputs.influxdb]]
#    urls = ["https://influxdb.math.uwaterloo.ca:8086"]
#    database = "test"
#    retention_policy = ""
#    write_consistency = "any"
#    timeout = "5s"
#    username = "test"
#    password = XXXXXXX
# socket_writer_udp
[[outputs.socket_writer]]
    # 8083 is a service that auto_tags using math inventory
    address = "udp://influxdb.math.uwaterloo.ca:8099"
    # data_format = "influx"
# 20
# 20_disk
# 20_exec
# 20_inputs
# cpu
[[inputs.cpu]]
    # percpu is too much info this day&age.
    percpu = false
    #totalcpu = true
    #collect_cpu_time = false
# defaults
[[inputs.diskio]]
[[inputs.kernel]]
[[inputs.mem]]
[[inputs.processes]]
[[inputs.swap]]
[[inputs.system]]
# disk
[[inputs.disk]]
    ignore_fs = ["tmpfs", "devtmpfs", "devfs", "cifs"]
    mount_points = ["/", "/home", "/boot"]

```

`pkg: uwl.nrpe`
-----------------------------------------------------------------
 * state: pkg.installed
 * name: `uwl.nrpe`



```
install: ['nagios-nrpe-server']
```

`service: uwl.nrpe`
-----------------------------------------------------------------
 * state: service.running
 * name: `nagios-nrpe-server`

run or update after changes in:
 * [pkg: uwl.nrpe](#pkg-uwlnrpe)
 * [file: math._state.nrpe](#file-math_statenrpe)


```
enable: true
```

`pkg: math._state.nrpe extra_checks_git`
-----------------------------------------------------------------
 * state: pkg.installed
 * name: `git`



```
install: git
```

`git: math._state.nrpe extra_checks_git`
-----------------------------------------------------------------
 * state: git.latest
 * name: `https://git.uwaterloo.ca/mfcf/nagios_checks.git`

require:
 * [pkg: math._state.nrpe extra_checks_git](#pkg-math_statenrpe-extra_checks_git)

required in:
 * [file: math._state.nrpe](#file-math_statenrpe)


```
target: /usr/local/nagios_checks
user: root
```

`pkg: math._state.nrpe`
-----------------------------------------------------------------
 * state: pkg.installed
 * name: `math._state.nrpe`



```
install: ['nagios-plugins']
```

`file: math._state.nrpe`
-----------------------------------------------------------------
 * state: file.managed
 * name: `/etc/nagios/nrpe.d/uw_share_nrpe.cfg`

after changes, run or update:
 * [service: uwl.nrpe](#service-uwlnrpe)

require:
 * [git: math._state.nrpe extra_checks_git](#git-math_statenrpe-extra_checks_git)


file stat /etc/nagios/nrpe.d/uw_share_nrpe.cfg
```
gid: 0
group: root
mode: '0644'
size: 32
uid: 0
user: root
```
file data /etc/nagios/nrpe.d/uw_share_nrpe.cfg
```
allowed_hosts=129.97.111.118/32

```

`pkg: uwl.postfix`
-----------------------------------------------------------------
 * state: pkg.installed
 * name: `uwl.postfix`



```
install: ['postfix']
```

`service: uwl.postfix`
-----------------------------------------------------------------
 * state: service.running
 * name: `postfix`

run or update after changes in:
 * [file: math._state.postfix](#file-math_statepostfix)

require:
 * [pkg: uwl.postfix](#pkg-uwlpostfix)


```
enable: true
```

`file: math._state.postfix virtual_aliases`
-----------------------------------------------------------------
 * state: file.managed
 * name: `/etc/postfix/virtual_aliases`


file stat /etc/postfix/virtual_aliases
```
gid: 0
group: root
mode: '0640'
size: 582
uid: 0
user: root
```
file data /etc/postfix/virtual_aliases
```
# disallow sender-specified routing. This is a must if you relay mail
/[%!@].*[%!@]/   550 Sender-specified routing rejected
#
# direct root mail
/^root@localhost/                 steverweber@gmail.com,s8weber@uwaterloo.ca
/^root@opencs-api-p01-01.private.uwaterloo.ca/   steverweber@gmail.com,s8weber@uwaterloo.ca
#
# truncate user name, then send email to campus server
#/^([^@]{3,8})[^@]*$/                          $(1)@uwaterloo.ca
/^([^@]{3,8})[^@]*@localhost$/                 $(1)@uwaterloo.ca
/^([^@]{3,8})[^@]*@opencs-api-p01-01.private.uwaterloo.ca$/   $(1)@uwaterloo.ca

```

`file: math._state.postfix`
-----------------------------------------------------------------
 * state: file.managed
 * name: `/etc/postfix/main.cf`

after changes, run or update:
 * [service: uwl.postfix](#service-uwlpostfix)


file stat /etc/postfix/main.cf
```
gid: 0
group: root
mode: '0644'
size: 1853
uid: 0
user: root
```
file data /etc/postfix/main.cf
```
# See /usr/share/postfix/main.cf.dist for a commented, more complete version
# Debian specific:  Specifying a file name will cause the first
# line of that file to be used as the name.  The Debian default
# is /etc/mailname.
#myorigin = /etc/mailname
smtpd_banner = $myhostname ESMTP $mail_name (Ubuntu)
biff = no
# appending .domain is the MUA's job.
append_dot_mydomain = no
# Uncomment the next line to generate "delayed mail" warnings
#delay_warning_time = 4h
readme_directory = no
# TLS parameters
smtpd_tls_cert_file = /etc/ssl/certs/ssl-cert-snakeoil.pem
smtpd_tls_key_file = /etc/ssl/private/ssl-cert-snakeoil.key
smtpd_use_tls = yes
smtpd_tls_session_cache_database = btree:${data_directory}/smtpd_scache
smtp_tls_session_cache_database = btree:${data_directory}/smtp_scache
# See /usr/share/doc/postfix/TLS_README.gz in the postfix-doc package for
# information on enabling SSL in the smtp client.
smtpd_relay_restrictions = permit_mynetworks permit_sasl_authenticated defer_unauth_destination
myhostname = opencs-api-p01-01.private.uwaterloo.ca
alias_maps = hash:/etc/aliases
alias_database = hash:/etc/aliases
myorigin = /etc/mailname
## commented out because custom settings cause:
## warning: /etc/postfix/main.cf, line XX: overriding earlier entry: mydestination=
#mydestination = $myhostname, opencs-api-p01-01.private.uwaterloo.ca, localhost
#mydestination = $myhostname, localhost
relayhost =
mynetworks = 127.0.0.0/8 [::ffff:127.0.0.0]/104 [::1]/128
mailbox_size_limit = 0
recipient_delimiter = +
inet_interfaces = all
inet_protocols = all
##### custom
mydestination = 
#forward_path = $home/.forward${recipient_delimiter}${extension}, $home/.forward
forward_path = $home/.forward_override
virtual_alias_domains = $myhostname, opencs-api-p01-01.private.uwaterloo.ca, localhost
virtual_alias_maps = regexp:/etc/postfix/virtual_aliases

```

`cmd: uwl.sysctl`
-----------------------------------------------------------------
 * state: cmd.wait
 * name: `sysctl --system`

run or update after changes in:
 * [file: uwl.sysctl.config](#file-uwlsysctlconfig)



`file: uwl.sysctl.config`
-----------------------------------------------------------------
 * state: file.managed
 * name: `/etc/sysctl.conf`

after changes, run or update:
 * [cmd: uwl.sysctl](#cmd-uwlsysctl)


file stat /etc/sysctl.conf
```
gid: 0
group: root
mode: '0644'
size: 267
uid: 0
user: root
```
file data /etc/sysctl.conf
```
# disable_ipv6
# ipv6 causes issues with sshd xforwarding.. slow 60sec timeout
net.ipv6.conf.all.disable_ipv6 = 1
net.ipv6.conf.default.disable_ipv6 = 1
net.ipv6.conf.lo.disable_ipv6 = 1
# kernel_panic
# set system to reboot 10 minutes after panic
kernel.panic = 600

```

`service: uwl.iptables disabled ufw`
-----------------------------------------------------------------
 * state: service.dead
 * name: `ufw`


```
enable: false
```

`pkg: uwl.iptables`
-----------------------------------------------------------------
 * state: pkg.installed
 * name: `uwl.iptables`



```
install: ['iptables-persistent', 'netfilter-persistent']
```

`service: uwl.iptables`
-----------------------------------------------------------------
 * state: service.running
 * name: `netfilter-persistent`

require:
 * [pkg: uwl.iptables](#pkg-uwliptables)


```
enable: true
```

`cmd: uwl.iptables.v4.config enforce iptables to be full match`
-----------------------------------------------------------------
 * state: cmd.run
 * name: `iptables-save | \grep -v -F '#' | sed '/^:/s@\[[0-9]\{1,\}:[0-9]\{1,\}\]@[0:0]@g' > /etc/iptables/rules.v4`


```
unless: iptables-save | \grep -v -F '#' | sed '/^:/s@\[[0-9]\{1,\}:[0-9]\{1,\}\]@[0:0]@g'
  | diff /etc/iptables/rules.v4 -
```

`file: uwl.iptables.v4.config`
-----------------------------------------------------------------
 * state: file.managed
 * name: `/etc/iptables/rules.v4`


file stat /etc/iptables/rules.v4
```
gid: 0
group: root
mode: '0640'
size: 478
uid: 0
user: root
```
file data /etc/iptables/rules.v4
```
*filter
:INPUT ACCEPT [0:0]
:FORWARD DROP [0:0]
:OUTPUT ACCEPT [0:0]
-A INPUT -i lo -j ACCEPT
-A INPUT -m state --state RELATED,ESTABLISHED -j ACCEPT
-A INPUT -p icmp -j ACCEPT
-A INPUT -p tcp -m state --state NEW -m tcp --dport 22 -j ACCEPT
-A INPUT -s 129.97.111.118/32 -p tcp -m state --state NEW -m tcp --dport 5666 -j ACCEPT
-A INPUT -s 129.97.206.15/32 -p tcp -m state --state NEW -m tcp --dport 7000 -j ACCEPT
-A INPUT -j REJECT --reject-with icmp-host-prohibited
COMMIT

```

`cmd: uwl.iptables.v4.config`
-----------------------------------------------------------------
 * state: cmd.wait_script
 * name: `salt://uwl/iptables/v4/script.sh`

run or update after changes in:
 * [file: uwl.iptables.v4.config](#file-uwliptablesv4config)

require:
 * [file: uwl.iptables.v4.config](#file-uwliptablesv4config)


```
stateful: true
template: jinja
```

`file: uwl.iptables.v6.config`
-----------------------------------------------------------------
 * state: file.managed
 * name: `/etc/iptables/rules.v6`

after changes, run or update:
 * [cmd: uwl.iptables.v6.config](#cmd-uwliptablesv6config)


file stat /etc/iptables/rules.v6
```
gid: 0
group: root
mode: '0600'
size: 72
uid: 0
user: root
```
file data /etc/iptables/rules.v6
```
*filter
:INPUT DROP [0:0]
:FORWARD DROP [0:0]
:OUTPUT DROP [0:0]
COMMIT

```

`cmd: uwl.iptables.v6.config`
-----------------------------------------------------------------
 * state: cmd.wait_script
 * name: `salt://uwl/iptables/v6/script.sh`

run or update after changes in:
 * [file: uwl.iptables.v6.config](#file-uwliptablesv6config)
 * [file: uwl.iptables.v6.config](#file-uwliptablesv6config)

require:
 * [file: uwl.iptables.v6.config](#file-uwliptablesv6config)


```
stateful: true
template: jinja
```

`file: uwl.motd remove legal notice`
-----------------------------------------------------------------
 * state: file.absent
 * name: `/etc/legal`



`file: uwl.motd /etc/update-motd.d/10-help-text`
-----------------------------------------------------------------
 * state: file.managed
 * name: `/etc/update-motd.d/10-help-text`


file stat /etc/update-motd.d/10-help-text
```
gid: 0
group: root
mode: '0755'
size: 10
uid: 0
user: root
```
file data /etc/update-motd.d/10-help-text
```
#!/bin/sh

```

`file: uwl.motd /etc/update-motd.d/00-header`
-----------------------------------------------------------------
 * state: file.managed
 * name: `/etc/update-motd.d/00-header`


file stat /etc/update-motd.d/00-header
```
gid: 0
group: root
mode: '0755'
size: 10
uid: 0
user: root
```
file data /etc/update-motd.d/00-header
```
#!/bin/sh

```

`file: uwl.motd static message`
-----------------------------------------------------------------
 * state: file.managed
 * name: `/etc/motd`


file stat /etc/motd
```
gid: 0
group: root
mode: '0644'
size: 602
uid: 0
user: root
```
file data /etc/motd
```
----------------------------------------------------------
OS     : Ubuntu-16.04
CORES  : 2
MEMORY : 1GiB available to users
FQDN   : opencs-api-p01-01.private.uwaterloo.ca


More help, please see:
w3m https://uwaterloo.ca/math-faculty-computing-facility

Policy and computer ethics:
w3m https://uwaterloo.ca/ist/about/policies-standards-and-guidelines

If you have any problems using your UNIX, NEXUS, or Mac
accounts, ask the consultants for assistance.
Phone 888-4567 x36323, visit MC3017, or email mfcfhelp@uwaterloo.ca

Happy computing!

----------------------------------------------------------

```

`pkg: uwl.lldpd`
-----------------------------------------------------------------
 * state: pkg.installed
 * name: `uwl.lldpd`



```
install: ['lldpd']
```

`service: uwl.lldpd`
-----------------------------------------------------------------
 * state: service.running
 * name: `lldpd`

run or update after changes in:
 * [pkg: uwl.lldpd](#pkg-uwllldpd)


```
enable: true
```

`group: uwl._state.user managed opencs`
-----------------------------------------------------------------
 * state: group.present
 * name: `opencs`


```
gid: 888
system: true
```

`user: uwl._state.user managed opencs`
-----------------------------------------------------------------
 * state: user.present
 * name: `opencs`


```
gid: 888
home: /home/opencs
shell: /bin/bash
system: true
uid: 888
```

`file: uwl._state.user managed opencs file /home/opencs/.ssh/id_rsa.pub`
-----------------------------------------------------------------
 * state: file.managed
 * name: `/home/opencs/.ssh/id_rsa.pub`


file stat /home/opencs/.ssh/id_rsa.pub
```
gid: 888
group: opencs
mode: '0600'
size: 416
uid: 888
user: opencs
```
file data /home/opencs/.ssh/id_rsa.pub
```
ssh-rsa XXXXXXX opencs@opencs-app-1-prod.salt.math

```

`file: uwl._state.user managed opencs file /home/opencs/.ssh/authorized_keys`
-----------------------------------------------------------------
 * state: file.managed
 * name: `/home/opencs/.ssh/authorized_keys`


file stat /home/opencs/.ssh/authorized_keys
```
gid: 888
group: opencs
mode: '0600'
size: 1456
uid: 888
user: opencs
```
file data /home/opencs/.ssh/authorized_keys
```
# local
## ssh-rsa XXXXXXX jladan@taurine
## ssh-rsa XXXXXXX jladan@cemcmac14.math.uwaterloo.ca
ssh-rsa XXXXXXX smtosh@uwaterloo.ca
ssh-rsa XXXXXXX smtosh@uwaterloo.ca

```

`file: uwl._state.user managed opencs file /home/opencs/.ssh/id_rsa`
-----------------------------------------------------------------
 * state: file.managed
 * name: `/home/opencs/.ssh/id_rsa`


file stat /home/opencs/.ssh/id_rsa
```
gid: 888
group: opencs
mode: '0600'
size: 1679
uid: 888
user: opencs
```
file data /home/opencs/.ssh/id_rsa
```
XXXXXXX RSA PRIVATE KEY-----

```

`file: uwl._state.user managed root file /root/.ssh/authorized_keys`
-----------------------------------------------------------------
 * state: file.managed
 * name: `/root/.ssh/authorized_keys`


file stat /root/.ssh/authorized_keys
```
gid: 0
group: root
mode: '0600'
size: 2650
uid: 0
user: root
```
file data /root/.ssh/authorized_keys
```
# __global
ssh-rsa XXXXXXX root@mfcf.math

ssh-rsa XXXXXXX s8weber@mbp

# _cscf
ssh-dss XXXXXXX root@cscf-admin.host
# _cscf_ft_staff_admin
# todo
# _mfcf
ssh-rsa XXXXXXX root@mfcf.math

ssh-rsa XXXXXXX s8weber@mbp
# local
ssh-rsa XXXXXXX s8weber@mbp


```

`pkg: _common.pkgs`
-----------------------------------------------------------------
 * state: pkg.installed
 * name: `_common.pkgs`



```
install: ['lsof', 'dnsutils', 'wget', 'curl', 'git', 'man', 'vim', 'ed', 'nano', 'screen', 'tmux', 'sshfs']
```

`pkg: _common.cpu_microcode`
-----------------------------------------------------------------
 * state: pkg.installed
 * name: `_common.cpu_microcode`



```
install: ['intel-microcode', 'amd64-microcode']
```

`test: _common.cpu_microcode`
-----------------------------------------------------------------
 * state: test.fail_without_changes
 * name: `REQUIRE REBOOT AFTER MICROCODE CHANGES!!!`


```
onchanges:
- pkg: _common.cpu_microcode
```

`file: math._state.nat script`
-----------------------------------------------------------------
 * state: file.managed
 * name: `/usr/local/maintenance/mfcf-nat.sh`


file stat /usr/local/maintenance/mfcf-nat.sh
```
gid: 0
group: root
mode: '0700'
size: 2211
uid: 0
user: root
```
file data /usr/local/maintenance/mfcf-nat.sh
```
#!/bin/bash
set -e

# require a function name as argument
[ "$#" -eq 1 ] || { echo "1 argument required. One of: [check_nat, stop_nat, start_nat, service_mode]" 1>&2; exit 127; }


DEFAULT_GW="172.27.13.1"
NAT="172.27.13.254"
NAT_TABLE="nat"
NAT_ID="411"


function _is_nat_in_route {
    ip rule list | grep -q "from all lookup $NAT_TABLE" \
        && ip route list table $NAT_TABLE | grep -q "default via $NAT" \
        && return 0
    return 1
}


function check_nat {
    # example: ping -c 1 google.ca > /dev/null 2>&1 || ping -c 1 yahoo.com > /dev/null 2>&1 || { echo 'check internet failed' 1>&2; return 1; }
    ping -c 1 google.ca > /dev/null 2>&1 || ping -c 1 yahoo.com > /dev/null 2>&1 || { echo 'check internet failed' 1>&2; return 1; }
}


function start_nat {
    if ! _is_nat_in_route; then
        if ! grep -q "$NAT_ID $NAT_TABLE" /etc/iproute2/rt_tables; then echo "$NAT_ID $NAT_TABLE" >> /etc/iproute2/rt_tables; fi
        ip route flush table $NAT_TABLE || true

# ip_route start

ip route add 129.97.0.0/16 table $NAT_TABLE via $DEFAULT_GW
ip route add 192.168.0.0/16 table $NAT_TABLE via $DEFAULT_GW
ip route add 172.16.0.0/12 table $NAT_TABLE via $DEFAULT_GW
ip route add 10.0.0.0/8 table $NAT_TABLE via $DEFAULT_GW
ip route add default table $NAT_TABLE via $NAT

# ip_route end

        ip rule add from 0.0.0.0/0 table $NAT_TABLE
        ip route list table $NAT_TABLE
        echo "Route table $NAT_TABLE recreated"
    fi
    check_nat || return $?
}


function stop_nat {
    if _is_nat_in_route; then
        ip route flush table $NAT_TABLE || true
        while ip rule list | grep -q "from all lookup $NAT_TABLE"; do
            ip rule del from 0.0.0.0/0 table $NAT_TABLE || true
            ((c++)) && ((c==10)) && break
        done
        echo "Route table $NAT_TABLE deleted"
    fi
}


function service_mode {
    echo 'Recreating NAT route'
    # disable the nat to force route table to be rebuilt
    stop_nat || echo 'hit unknowen issue while stop_nat' 1>&2
    start_nat || echo 'hit unknowen issue while start_nat' 1>&2
    echo "Starting NAT health check loop"
    while true; do
        start_nat || exit 3
        sleep 1000
    done
}


# run arg 1 function
$1

```

`cmd: uwl.systemd reload units`
-----------------------------------------------------------------
 * state: cmd.watch
 * name: `systemctl daemon-reload`

run or update after changes in:
 * [file: cscf.service.opencs.01.api service](#file-cscfserviceopencs01api-service)
 * [file: math._state.nat.enabled](#file-math_statenatenabled)



`file: math._state.nat.enabled`
-----------------------------------------------------------------
 * state: file.managed
 * name: `/etc/systemd/system/mfcf-nat.service`

after changes, run or update:
 * [cmd: uwl.systemd reload units](#cmd-uwlsystemd-reload-units)

require:
 * [file: math._state.nat script](#file-math_statenat-script)


file stat /etc/systemd/system/mfcf-nat.service
```
gid: 0
group: root
mode: '0644'
size: 373
uid: 0
user: root
```
file data /etc/systemd/system/mfcf-nat.service
```
[Unit]
Description=Service that launches some scripts that ensure system has nat configured
After=network.target

[Service]
Type=simple
ExecStart=/usr/local/maintenance/mfcf-nat.sh service_mode
ExecStartPost=/bin/sleep 1
ExecStop=/usr/local/maintenance/mfcf-nat.sh stop_nat
TimeoutStartSec=10s
Restart=on-failure
RestartSec=10s
Nice=5

[Install]
WantedBy=multi-user.target

```

`service: math._state.nat.enabled`
-----------------------------------------------------------------
 * state: service.running
 * name: `mfcf-nat`

run or update after changes in:
 * [file: math._state.nat script](#file-math_statenat-script)
 * [file: math._state.nat.enabled](#file-math_statenatenabled)


```
enable: true
```

`pkg: uwl.sshd`
-----------------------------------------------------------------
 * state: pkg.installed
 * name: `uwl.sshd`



```
install: ['openssh-server']
```

`service: uwl.sshd`
-----------------------------------------------------------------
 * state: service.running
 * name: `ssh`

require:
 * [pkg: uwl.sshd](#pkg-uwlsshd)


```
enable: true
```

`pkg: cscf.service.opencs.01.api package`
-----------------------------------------------------------------
 * state: pkg.installed
 * name: `cscf.service.opencs.01.api package`



```
install: ['git', 'python3-pip']
```

`ssh_known_hosts: cscf.service.opencs.01.api package`
-----------------------------------------------------------------
 * state: ssh_known_hosts.present
 * name: `git.uwaterloo.ca`


```
fingerprint: b9:6a:a9:3b:f6:d3:36:19:89:f6:0f:3d:2e:6c:a8:e2
user: opencs
```

`file: cscf.service.opencs.01.api package`
-----------------------------------------------------------------
 * state: file.directory
 * name: `/srv/python-socket`


```
group: opencs
mode: 775
user: opencs
```

`git: cscf.service.opencs.01.api package`
-----------------------------------------------------------------
 * state: git.latest
 * name: `gitlab@git.uwaterloo.ca:opencs/python-socket.git`

after changes, run or update:
 * [cmd: cscf.service.opencs.01.api ensure requirements](#cmd-cscfserviceopencs01api-ensure-requirements)
 * [service: cscf.service.opencs.01.api service](#service-cscfserviceopencs01api-service)

require:
 * [ssh_known_hosts: cscf.service.opencs.01.api package](#ssh_known_hosts-cscfserviceopencs01api-package)


```
branch: production
rev: production
submodules: true
target: /srv/python-socket
user: opencs
```

`cmd: cscf.service.opencs.01.api ensure requirements`
-----------------------------------------------------------------
 * state: cmd.wait
 * name: `pip3 install -r requirements.txt`

run or update after changes in:
 * [git: cscf.service.opencs.01.api package](#git-cscfserviceopencs01api-package)


```
cwd: /srv/python-socket
```

`file: cscf.service.opencs.01.api service`
-----------------------------------------------------------------
 * state: file.managed
 * name: `/etc/systemd/system/opencs-pysocket.service`

after changes, run or update:
 * [service: cscf.service.opencs.01.api service](#service-cscfserviceopencs01api-service)
 * [cmd: uwl.systemd reload units](#cmd-uwlsystemd-reload-units)


file stat /etc/systemd/system/opencs-pysocket.service
```
gid: 0
group: root
mode: '0644'
size: 424
uid: 0
user: root
```
file data /etc/systemd/system/opencs-pysocket.service
```
[Unit]
Description=opencs-pysocket

[Service]
Type=simple
ExecStart=/bin/su opencs -c "python3 /srv/python-socket/server.py --port=7000 --address=0.0.0.0"
ExecStartPost=/bin/sleep 1
#ExecStart=/bin/sh -c "PYTHONHOME=/ /usr/sbin/chroot --userspec=888:888 /srv/python3jail python3 /srv/server.py --port=7000 --address=0.0.0.0"
Restart=on-failure
RestartSec=2s
RuntimeMaxSec=800min
Nice=5

[Install]
WantedBy=multi-user.target

```

`service: cscf.service.opencs.01.api service`
-----------------------------------------------------------------
 * state: service.running
 * name: `opencs-pysocket`

run or update after changes in:
 * [git: cscf.service.opencs.01.api package](#git-cscfserviceopencs01api-package)
 * [file: cscf.service.opencs.01.api service](#file-cscfserviceopencs01api-service)

require:
 * [file: cscf.service.opencs.01.api service](#file-cscfserviceopencs01api-service)


```
enable: true
```




Other information
=====================================================================================

```

salt grain: ip_interfaces
-----------------------------------------------------------------
[('ens160', ['172.27.13.27']), ('lo', ['127.0.0.1'])]


salt grain: hwaddr_interfaces
-----------------------------------------------------------------
[('ens160', '00:50:56:84:77:6a'), ('lo', '00:00:00:00:00:00')]


# ip address show
-----------------------------------------------------------------
1: lo: <LOOPBACK,UP,LOWER_UP> mtu 65536 qdisc noqueue state UNKNOWN group default qlen 1
    link/loopback 00:00:00:00:00:00 brd 00:00:00:00:00:00
    inet 127.0.0.1/8 scope host lo
2: ens160: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc mq state UP group default qlen 1000
    link/ether 00:50:56:84:77:6a brd ff:ff:ff:ff:ff:ff
    inet 172.27.13.27/24 brd 172.27.13.255 scope global ens160


# ip route list table all
-----------------------------------------------------------------
default via 172.27.13.254 dev ens160  table nat 
10.0.0.0/8 via 172.27.13.1 dev ens160  table nat 
129.97.0.0/16 via 172.27.13.1 dev ens160  table nat 
172.16.0.0/12 via 172.27.13.1 dev ens160  table nat 
192.168.0.0/16 via 172.27.13.1 dev ens160  table nat 
default via 172.27.13.1 dev ens160 
172.27.13.0/24 dev ens160  proto kernel  scope link  src 172.27.13.27 
broadcast 127.0.0.0 dev lo  table local  proto kernel  scope link  src 127.0.0.1 
local 127.0.0.0/8 dev lo  table local  proto kernel  scope host  src 127.0.0.1 
local 127.0.0.1 dev lo  table local  proto kernel  scope host  src 127.0.0.1 
broadcast 127.255.255.255 dev lo  table local  proto kernel  scope link  src 127.0.0.1 
broadcast 172.27.13.0 dev ens160  table local  proto kernel  scope link  src 172.27.13.27 
local 172.27.13.27 dev ens160  table local  proto kernel  scope host  src 172.27.13.27 
broadcast 172.27.13.255 dev ens160  table local  proto kernel  scope link  src 172.27.13.27 
unreachable default dev lo  table unspec  proto kernel  metric 4294967295  error -101 pref medium
unreachable default dev lo  table unspec  proto kernel  metric 4294967295  error -101 pref medium


# iptables-save
-----------------------------------------------------------------
*filter
:INPUT ACCEPT [0:0]
:FORWARD DROP [0:0]
:OUTPUT ACCEPT [0:0]
-A INPUT -i lo -j ACCEPT
-A INPUT -m state --state RELATED,ESTABLISHED -j ACCEPT
-A INPUT -p icmp -j ACCEPT
-A INPUT -p tcp -m state --state NEW -m tcp --dport 22 -j ACCEPT
-A INPUT -s 129.97.111.118/32 -p tcp -m state --state NEW -m tcp --dport 5666 -j ACCEPT
-A INPUT -s 129.97.206.15/32 -p tcp -m state --state NEW -m tcp --dport 7000 -j ACCEPT
-A INPUT -j REJECT --reject-with icmp-host-prohibited
COMMIT


# ip6tables-save
-----------------------------------------------------------------
*filter
:INPUT DROP [0:0]
:FORWARD DROP [0:0]
:OUTPUT DROP [0:0]
COMMIT




# mount
-----------------------------------------------------------------
sysfs on /sys type sysfs (rw,nosuid,nodev,noexec,relatime)
proc on /proc type proc (rw,nosuid,nodev,noexec,relatime)
udev on /dev type devtmpfs (rw,nosuid,relatime,size=1006284k,nr_inodes=251571,mode=755)
devpts on /dev/pts type devpts (rw,nosuid,noexec,relatime,gid=5,mode=620,ptmxmode=000)
tmpfs on /run type tmpfs (rw,nosuid,noexec,relatime,size=204816k,mode=755)
/dev/sda1 on / type ext4 (rw,relatime,errors=remount-ro,data=ordered)
securityfs on /sys/kernel/security type securityfs (rw,nosuid,nodev,noexec,relatime)
tmpfs on /dev/shm type tmpfs (rw,nosuid,nodev)
tmpfs on /run/lock type tmpfs (rw,nosuid,nodev,noexec,relatime,size=5120k)
tmpfs on /sys/fs/cgroup type tmpfs (ro,nosuid,nodev,noexec,mode=755)
cgroup on /sys/fs/cgroup/systemd type cgroup (rw,nosuid,nodev,noexec,relatime,xattr,release_agent=/lib/systemd/systemd-cgroups-agent,name=systemd)
pstore on /sys/fs/pstore type pstore (rw,nosuid,nodev,noexec,relatime)
cgroup on /sys/fs/cgroup/devices type cgroup (rw,nosuid,nodev,noexec,relatime,devices)
cgroup on /sys/fs/cgroup/cpuset type cgroup (rw,nosuid,nodev,noexec,relatime,cpuset)
cgroup on /sys/fs/cgroup/net_cls,net_prio type cgroup (rw,nosuid,nodev,noexec,relatime,net_cls,net_prio)
cgroup on /sys/fs/cgroup/cpu,cpuacct type cgroup (rw,nosuid,nodev,noexec,relatime,cpu,cpuacct)
cgroup on /sys/fs/cgroup/pids type cgroup (rw,nosuid,nodev,noexec,relatime,pids)
cgroup on /sys/fs/cgroup/freezer type cgroup (rw,nosuid,nodev,noexec,relatime,freezer)
cgroup on /sys/fs/cgroup/blkio type cgroup (rw,nosuid,nodev,noexec,relatime,blkio)
cgroup on /sys/fs/cgroup/perf_event type cgroup (rw,nosuid,nodev,noexec,relatime,perf_event)
cgroup on /sys/fs/cgroup/memory type cgroup (rw,nosuid,nodev,noexec,relatime,memory)
cgroup on /sys/fs/cgroup/hugetlb type cgroup (rw,nosuid,nodev,noexec,relatime,hugetlb)
systemd-1 on /proc/sys/fs/binfmt_misc type autofs (rw,relatime,fd=25,pgrp=1,timeout=0,minproto=5,maxproto=5,direct)
hugetlbfs on /dev/hugepages type hugetlbfs (rw,relatime)
debugfs on /sys/kernel/debug type debugfs (rw,relatime)
fusectl on /sys/fs/fuse/connections type fusectl (rw,relatime)
mqueue on /dev/mqueue type mqueue (rw,relatime)


```
