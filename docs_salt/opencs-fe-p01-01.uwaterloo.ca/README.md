
__Short description__: OpenCS frontend. This include http cache and loadbalance for the other opencs systems.





For more help, please see: https://git.uwaterloo.ca/opencs/opencs
        


Configuration Managment
===============================================================================

```
####################################################
fqdn: opencs-fe-p01-01.uwaterloo.ca
os: Ubuntu
osfinger: Ubuntu-16.04
mem_total: 2000MB
num_cpus: 2
ipv4: ['127.0.0.1', '129.97.206.15']
master: salt.math.private.uwaterloo.ca
####################################################
```

This system is fully or partly managed using Salt.

The following sections are a rendered view of what the configuration managment system
manages on this system. Each item is handled in order from top to bottom unless some prequsits like `require`
or `after changes, run or update` force other ordering.




`file: uwl.hosts`
-----------------------------------------------------------------
 * state: file.managed
 * name: `/etc/hosts`


file stat /etc/hosts
```
gid: 0
group: root
mode: '0644'
size: 266
uid: 0
user: root
```
file data /etc/hosts
```
# __global
# define ipv4 after ipv6, so the ipv4 address is used when referencing localhost
::1                 localhost localhost.localdomain localhost6 localhost6.localdomain6
127.0.0.1           localhost localhost.localdomain localhost4 localhost4.localdomain4

```

`locale: _common.locale en_US.UTF-8`
-----------------------------------------------------------------
 * state: locale.present
 * name: `en_US.UTF-8`



`locale: _common.locale set default`
-----------------------------------------------------------------
 * state: locale.system
 * name: `en_US.UTF-8`

require:
 * [locale: _common.locale en_US.UTF-8](#locale-_commonlocale-en_USUTF-8)



`timezone: _common.timezone`
-----------------------------------------------------------------
 * state: timezone.system
 * name: `America/Toronto`


```
utc: true
```

`service: uwl.systemd-timesyncd`
-----------------------------------------------------------------
 * state: service.running
 * name: `systemd-timesyncd`

run or update after changes in:
 * [file: _common.ntp.systemd-timesyncd](#file-_commonntpsystemd-timesyncd)


```
enable: true
```

`file: _common.ntp.systemd-timesyncd`
-----------------------------------------------------------------
 * state: file.managed
 * name: `/etc/systemd/timesyncd.conf`

after changes, run or update:
 * [service: uwl.systemd-timesyncd](#service-uwlsystemd-timesyncd)


file stat /etc/systemd/timesyncd.conf
```
gid: 0
group: root
mode: '0644'
size: 79
uid: 0
user: root
```
file data /etc/systemd/timesyncd.conf
```
[Time]
NTP=ntp1.uwaterloo.ca ntp2.uwaterloo.ca ntp3.uwaterloo.ca
#FallbackNTP=

```

`pkg: uwl.tls package`
-----------------------------------------------------------------
 * state: pkg.installed
 * name: `uwl.tls package`

required in:
 * [cmd: uwl.tls CA update](#cmd-uwltls-CA-update)



```
install: ['ca-certificates']
```

`cmd: uwl.tls CA update`
-----------------------------------------------------------------
 * state: cmd.wait
 * name: `update-ca-certificates`

run or update after changes in:
 * [file: uwl.tls.file GlobalSign_Non-Public.pem](#file-uwltlsfile-GlobalSign_Non-Publicpem)

require:
 * [pkg: uwl.tls package](#pkg-uwltls-package)



`file: uwl.tls.file GlobalSign_Non-Public.pem`
-----------------------------------------------------------------
 * state: file.managed
 * name: `/usr/local/share/ca-certificates/GlobalSign_Non-Public.pem`

after changes, run or update:
 * [cmd: uwl.tls CA update](#cmd-uwltls-CA-update)


file stat /usr/local/share/ca-certificates/GlobalSign_Non-Public.pem
```
gid: 0
group: root
mode: '0644'
size: 3004
uid: 0
user: root
```
file data /usr/local/share/ca-certificates/GlobalSign_Non-Public.pem
```
XXXXXXX CERTIFICATE-----

XXXXXXX CERTIFICATE-----


```

`file: uwl.tls.file private /etc/nginx/ssl_open.cs.uwaterloo.ca.pem`
-----------------------------------------------------------------
 * state: file.managed
 * name: `/etc/nginx/ssl_open.cs.uwaterloo.ca.pem`

require:
 * [pkg: uwl.nginx](#pkg-uwlnginx)

required in:
 * [file: cscf.service.opencs.01.fe.nginx_site](#file-cscfserviceopencs01fenginx_site)


file stat /etc/nginx/ssl_open.cs.uwaterloo.ca.pem
```
gid: 0
group: root
mode: '0640'
size: 5841
uid: 0
user: root
```
file data /etc/nginx/ssl_open.cs.uwaterloo.ca.pem
```
XXXXXXX RSA PRIVATE KEY-----

XXXXXXXIod3+QeAAm
q2aos9J8EXbN/mAIvJEOzwgZUQFMBGi/jwmHBW56jB5OjD5GGTa0GorbvlMxEOJx
nfVwx57WlwBob8wIpcl9o11xTT/4XWJ1hxRFddKWzCpXml5yTi2w73J/YW9KIkgj
8wSt7ngtVqiPnY/KisqcNDLWgpGoz0M58wXspUfkNy3u+LjbalxgtHZvTYBEYPjF
3hugo42Tqdz+xkm9ggJ/bRBAvh8oRIsfVHAuVNnQMsLthTOe/vwnnzVyfrxTCWu9
vmNlRpTI+ySJDyBCmCvqLtYNGrSo/wpNlB7ZUHaaYl9L2k4=
-----END CERTIFICATE-----

XXXXXXX CERTIFICATE-----


```

`file: uwl.tls.file private /etc/nginx/ssl_opencs.uwaterloo.ca.pem`
-----------------------------------------------------------------
 * state: file.managed
 * name: `/etc/nginx/ssl_opencs.uwaterloo.ca.pem`

require:
 * [pkg: uwl.nginx](#pkg-uwlnginx)

required in:
 * [file: cscf.service.opencs.01.fe.nginx_site](#file-cscfserviceopencs01fenginx_site)


file stat /etc/nginx/ssl_opencs.uwaterloo.ca.pem
```
gid: 0
group: root
mode: '0640'
size: 5837
uid: 0
user: root
```
file data /etc/nginx/ssl_opencs.uwaterloo.ca.pem
```
XXXXXXX RSA PRIVATE KEY-----

XXXXXXXJcQkEfDv8w
fYv1oFwVhbZ96D/PTg4N+CrhUTqmQa6cmBn6SSvZ1B+faGtRK18i93pCUr2gp4r1
l41qdCfzuNoO0mXSJBawGCmtAitdL6PLE1hmObNTxoknl9Sm/hzkFe4udgv1pK6p
YJ02DbM1/QUebf3YAoSGotNs0SMre1qg6EQnr1bltWabg/csL0RGTr1+XpgF+rJd
l5xeUyGxMcL5/rGiWMGOJH7rNxCsd19JUHHJ3Xl5xzEPY9qQpZmQc2odksQ7nIRT
NNQj/ahU+o7w1J60gZhsvB8oBBP/+RC9fyXPaK+VVjk/
-----END CERTIFICATE-----

XXXXXXX CERTIFICATE-----


```

`file: uwl.tls.file cert /etc/nginx/ssl_dhparam4096.pem`
-----------------------------------------------------------------
 * state: file.managed
 * name: `/etc/nginx/ssl_dhparam4096.pem`

require:
 * [pkg: uwl.nginx](#pkg-uwlnginx)

required in:
 * [file: cscf.service.opencs.01.fe.nginx_site](#file-cscfserviceopencs01fenginx_site)


file stat /etc/nginx/ssl_dhparam4096.pem
```
gid: 0
group: root
mode: '0644'
size: 769
uid: 0
user: root
```
file data /etc/nginx/ssl_dhparam4096.pem
```
XXXXXXX DH PARAMETERS-----

```

`service: uwl.systemd-journald`
-----------------------------------------------------------------
 * state: service.running
 * name: `systemd-journald`

run or update after changes in:
 * [file: uwl.systemd-journald.config](#file-uwlsystemd-journaldconfig)


```
enable: true
```

`file: uwl.systemd-journald.config`
-----------------------------------------------------------------
 * state: file.managed
 * name: `/etc/systemd/journald.conf`

after changes, run or update:
 * [service: uwl.systemd-journald](#service-uwlsystemd-journald)


file stat /etc/systemd/journald.conf
```
gid: 0
group: root
mode: '0644'
size: 65
uid: 0
user: root
```
file data /etc/systemd/journald.conf
```
[Journal]
Storage=persistent
SystemMaxUse=2G
ForwardToSyslog=yes

```

`pkg: uwl.rsyslog`
-----------------------------------------------------------------
 * state: pkg.installed
 * name: `uwl.rsyslog`



```
install: ['rsyslog']
```

`service: uwl.rsyslog`
-----------------------------------------------------------------
 * state: service.running
 * name: `rsyslog`

run or update after changes in:
 * [pkg: uwl.rsyslog](#pkg-uwlrsyslog)
 * [file: uwl.rsyslog.config](#file-uwlrsyslogconfig)


```
enable: true
```

`file: uwl.rsyslog.config`
-----------------------------------------------------------------
 * state: file.managed
 * name: `/etc/rsyslog.d/99-salt.conf`

after changes, run or update:
 * [service: uwl.rsyslog](#service-uwlrsyslog)


file stat /etc/rsyslog.d/99-salt.conf
```
gid: 0
group: root
mode: '0644'
size: 185
uid: 0
user: root
```
file data /etc/rsyslog.d/99-salt.conf
```
## ist
#*.*   @@relentless.private.uwaterloo.ca:514
#*.*   @@retribution.private.uwaterloo.ca:514
## mfcf
#*.*  @@syslog.math.uwaterloo.ca:514
*.*   @@log.math.private.uwaterloo.ca:514

```

`pkg: uwl.telegraf`
-----------------------------------------------------------------
 * state: pkg.latest
 * name: `uwl.telegraf`


```
pkgs:
- telegraf
```

`service: uwl.telegraf`
-----------------------------------------------------------------
 * state: service.running
 * name: `telegraf`

run or update after changes in:
 * [pkg: uwl.telegraf](#pkg-uwltelegraf)
 * [file: uwl.telegraf.config](#file-uwltelegrafconfig)


```
enable: true
```

`file: uwl.telegraf.config`
-----------------------------------------------------------------
 * state: file.managed
 * name: `/etc/telegraf/telegraf.conf`

after changes, run or update:
 * [service: uwl.telegraf](#service-uwltelegraf)

require:
 * [pkg: uwl.telegraf](#pkg-uwltelegraf)


file stat /etc/telegraf/telegraf.conf
```
gid: 0
group: root
mode: '0644'
size: 1489
uid: 0
user: root
```
file data /etc/telegraf/telegraf.conf
```
# 0
# 00_global
[global_tags]
# 05_agent
[agent]
    interval = "60s"
    round_interval = true
    metric_batch_size = 1000
    metric_buffer_limit = 10000
    collection_jitter = "5s"
    flush_interval = "10s"
    flush_jitter = "5s"
    precision = ""
    debug = false
    quiet = false
    logfile = ""
    hostname = "opencs-fe-p01-01.uwaterloo.ca"
    omit_hostname = false
# 10
# 10_outputs
# influxdb_math
[[outputs.influxdb]]
    urls = ["https://influxdb.math.uwaterloo.ca:8086"]
    database = "math"
    retention_policy = ""
    write_consistency = "any"
    timeout = "5s"
    username = "telegraf"
    password = XXXXXXX
# influxdb_test
#[[outputs.influxdb]]
#    urls = ["https://influxdb.math.uwaterloo.ca:8086"]
#    database = "test"
#    retention_policy = ""
#    write_consistency = "any"
#    timeout = "5s"
#    username = "test"
#    password = XXXXXXX
# socket_writer_udp
[[outputs.socket_writer]]
    # 8083 is a service that auto_tags using math inventory
    address = "udp://influxdb.math.uwaterloo.ca:8099"
    # data_format = "influx"
# 20
# 20_disk
# 20_exec
# 20_inputs
# cpu
[[inputs.cpu]]
    # percpu is too much info this day&age.
    percpu = false
    #totalcpu = true
    #collect_cpu_time = false
# defaults
[[inputs.diskio]]
[[inputs.kernel]]
[[inputs.mem]]
[[inputs.processes]]
[[inputs.swap]]
[[inputs.system]]
# disk
[[inputs.disk]]
    ignore_fs = ["tmpfs", "devtmpfs", "devfs", "cifs"]
    mount_points = ["/", "/home", "/boot"]

```

`pkg: uwl.nrpe`
-----------------------------------------------------------------
 * state: pkg.installed
 * name: `uwl.nrpe`



```
install: ['nagios-nrpe-server']
```

`service: uwl.nrpe`
-----------------------------------------------------------------
 * state: service.running
 * name: `nagios-nrpe-server`

run or update after changes in:
 * [pkg: uwl.nrpe](#pkg-uwlnrpe)
 * [file: math._state.nrpe](#file-math_statenrpe)


```
enable: true
```

`pkg: math._state.nrpe extra_checks_git`
-----------------------------------------------------------------
 * state: pkg.installed
 * name: `git`



```
install: git
```

`git: math._state.nrpe extra_checks_git`
-----------------------------------------------------------------
 * state: git.latest
 * name: `https://git.uwaterloo.ca/mfcf/nagios_checks.git`

require:
 * [pkg: math._state.nrpe extra_checks_git](#pkg-math_statenrpe-extra_checks_git)

required in:
 * [file: math._state.nrpe](#file-math_statenrpe)


```
target: /usr/local/nagios_checks
user: root
```

`pkg: math._state.nrpe`
-----------------------------------------------------------------
 * state: pkg.installed
 * name: `math._state.nrpe`



```
install: ['nagios-plugins']
```

`file: math._state.nrpe`
-----------------------------------------------------------------
 * state: file.managed
 * name: `/etc/nagios/nrpe.d/uw_share_nrpe.cfg`

after changes, run or update:
 * [service: uwl.nrpe](#service-uwlnrpe)

require:
 * [git: math._state.nrpe extra_checks_git](#git-math_statenrpe-extra_checks_git)


file stat /etc/nagios/nrpe.d/uw_share_nrpe.cfg
```
gid: 0
group: root
mode: '0644'
size: 32
uid: 0
user: root
```
file data /etc/nagios/nrpe.d/uw_share_nrpe.cfg
```
allowed_hosts=129.97.111.118/32

```

`pkg: uwl.postfix`
-----------------------------------------------------------------
 * state: pkg.installed
 * name: `uwl.postfix`



```
install: ['postfix']
```

`service: uwl.postfix`
-----------------------------------------------------------------
 * state: service.running
 * name: `postfix`

run or update after changes in:
 * [file: math._state.postfix](#file-math_statepostfix)

require:
 * [pkg: uwl.postfix](#pkg-uwlpostfix)


```
enable: true
```

`file: math._state.postfix virtual_aliases`
-----------------------------------------------------------------
 * state: file.managed
 * name: `/etc/postfix/virtual_aliases`


file stat /etc/postfix/virtual_aliases
```
gid: 0
group: root
mode: '0640'
size: 564
uid: 0
user: root
```
file data /etc/postfix/virtual_aliases
```
# disallow sender-specified routing. This is a must if you relay mail
/[%!@].*[%!@]/   550 Sender-specified routing rejected
#
# direct root mail
/^root@localhost/                 steverweber@gmail.com,s8weber@uwaterloo.ca
/^root@opencs-fe-p01-01.uwaterloo.ca/   steverweber@gmail.com,s8weber@uwaterloo.ca
#
# truncate user name, then send email to campus server
#/^([^@]{3,8})[^@]*$/                          $(1)@uwaterloo.ca
/^([^@]{3,8})[^@]*@localhost$/                 $(1)@uwaterloo.ca
/^([^@]{3,8})[^@]*@opencs-fe-p01-01.uwaterloo.ca$/   $(1)@uwaterloo.ca

```

`file: math._state.postfix`
-----------------------------------------------------------------
 * state: file.managed
 * name: `/etc/postfix/main.cf`

after changes, run or update:
 * [service: uwl.postfix](#service-uwlpostfix)


file stat /etc/postfix/main.cf
```
gid: 0
group: root
mode: '0644'
size: 1826
uid: 0
user: root
```
file data /etc/postfix/main.cf
```
# See /usr/share/postfix/main.cf.dist for a commented, more complete version
# Debian specific:  Specifying a file name will cause the first
# line of that file to be used as the name.  The Debian default
# is /etc/mailname.
#myorigin = /etc/mailname
smtpd_banner = $myhostname ESMTP $mail_name (Ubuntu)
biff = no
# appending .domain is the MUA's job.
append_dot_mydomain = no
# Uncomment the next line to generate "delayed mail" warnings
#delay_warning_time = 4h
readme_directory = no
# TLS parameters
smtpd_tls_cert_file = /etc/ssl/certs/ssl-cert-snakeoil.pem
smtpd_tls_key_file = /etc/ssl/private/ssl-cert-snakeoil.key
smtpd_use_tls = yes
smtpd_tls_session_cache_database = btree:${data_directory}/smtpd_scache
smtp_tls_session_cache_database = btree:${data_directory}/smtp_scache
# See /usr/share/doc/postfix/TLS_README.gz in the postfix-doc package for
# information on enabling SSL in the smtp client.
smtpd_relay_restrictions = permit_mynetworks permit_sasl_authenticated defer_unauth_destination
myhostname = opencs-fe-p01-01.uwaterloo.ca
alias_maps = hash:/etc/aliases
alias_database = hash:/etc/aliases
myorigin = /etc/mailname
## commented out because custom settings cause:
## warning: /etc/postfix/main.cf, line XX: overriding earlier entry: mydestination=
#mydestination = $myhostname, opencs-fe-p01-01.uwaterloo.ca, localhost
#mydestination = $myhostname, localhost
relayhost =
mynetworks = 127.0.0.0/8 [::ffff:127.0.0.0]/104 [::1]/128
mailbox_size_limit = 0
recipient_delimiter = +
inet_interfaces = all
inet_protocols = all
##### custom
mydestination = 
#forward_path = $home/.forward${recipient_delimiter}${extension}, $home/.forward
forward_path = $home/.forward_override
virtual_alias_domains = $myhostname, opencs-fe-p01-01.uwaterloo.ca, localhost
virtual_alias_maps = regexp:/etc/postfix/virtual_aliases

```

`cmd: uwl.sysctl`
-----------------------------------------------------------------
 * state: cmd.wait
 * name: `sysctl --system`

run or update after changes in:
 * [file: uwl.sysctl.config](#file-uwlsysctlconfig)



`file: uwl.sysctl.config`
-----------------------------------------------------------------
 * state: file.managed
 * name: `/etc/sysctl.conf`

after changes, run or update:
 * [cmd: uwl.sysctl](#cmd-uwlsysctl)


file stat /etc/sysctl.conf
```
gid: 0
group: root
mode: '0644'
size: 568
uid: 0
user: root
```
file data /etc/sysctl.conf
```
net.ipv4.tcp_keepalive_time = 2
net.netfilter.nf_conntrack_generic_timeout = 120
fs.file-max = 4000000
net.ipv4.tcp_syncookies = 1
net.ipv4.tcp_max_syn_backlog = 3240000
net.ipv4.conf.all.rp_filter = 0
net.ipv4.tcp_max_tw_buckets = 1440000
net.ipv4.conf.default.rp_filter = 0
net.ipv4.tcp_window_scaling = 0
net.ipv4.netfilter.ip_conntrack_max = 10555360
net.netfilter.nf_conntrack_max = 10555360
net.ipv4.tcp_fin_timeout = 1
net.ipv4.netfilter.ip_conntrack_tcp_timeout_established = 54000
net.ipv4.ip_local_port_range = 10240    65535
net.nf_conntrack_max = 10655360

```

`service: uwl.iptables disabled ufw`
-----------------------------------------------------------------
 * state: service.dead
 * name: `ufw`


```
enable: false
```

`pkg: uwl.iptables`
-----------------------------------------------------------------
 * state: pkg.installed
 * name: `uwl.iptables`



```
install: ['iptables-persistent', 'netfilter-persistent']
```

`service: uwl.iptables`
-----------------------------------------------------------------
 * state: service.running
 * name: `netfilter-persistent`

require:
 * [pkg: uwl.iptables](#pkg-uwliptables)


```
enable: true
```

`cmd: uwl.iptables.v4.config enforce iptables to be full match`
-----------------------------------------------------------------
 * state: cmd.run
 * name: `iptables-save | \grep -v -F '#' | sed '/^:/s@\[[0-9]\{1,\}:[0-9]\{1,\}\]@[0:0]@g' > /etc/iptables/rules.v4`


```
unless: iptables-save | \grep -v -F '#' | sed '/^:/s@\[[0-9]\{1,\}:[0-9]\{1,\}\]@[0:0]@g'
  | diff /etc/iptables/rules.v4 -
```

`file: uwl.iptables.v4.config`
-----------------------------------------------------------------
 * state: file.managed
 * name: `/etc/iptables/rules.v4`


file stat /etc/iptables/rules.v4
```
gid: 0
group: root
mode: '0640'
size: 579
uid: 0
user: root
```
file data /etc/iptables/rules.v4
```
*filter
:INPUT ACCEPT [0:0]
:FORWARD DROP [0:0]
:OUTPUT ACCEPT [0:0]
-A INPUT -i lo -j ACCEPT
-A INPUT -m state --state RELATED,ESTABLISHED -j ACCEPT
-A INPUT -p icmp -j ACCEPT
-A INPUT -s 129.97.111.118/32 -p tcp -m state --state NEW -m tcp --dport 5666 -j ACCEPT
-A INPUT -s 129.97.111.0/24 -p tcp -m state --state NEW -m tcp --dport 22 -j ACCEPT
-A INPUT -s 209.171.37.153/32 -j DROP
-A INPUT -p tcp -m state --state NEW -m tcp --dport 80 -j ACCEPT
-A INPUT -p tcp -m state --state NEW -m tcp --dport 443 -j ACCEPT
-A INPUT -j REJECT --reject-with icmp-host-prohibited
COMMIT

```

`cmd: uwl.iptables.v4.config`
-----------------------------------------------------------------
 * state: cmd.wait_script
 * name: `salt://uwl/iptables/v4/script.sh`

run or update after changes in:
 * [file: uwl.iptables.v4.config](#file-uwliptablesv4config)

require:
 * [file: uwl.iptables.v4.config](#file-uwliptablesv4config)


```
stateful: true
template: jinja
```

`file: uwl.iptables.v6.config`
-----------------------------------------------------------------
 * state: file.managed
 * name: `/etc/iptables/rules.v6`

after changes, run or update:
 * [cmd: uwl.iptables.v6.config](#cmd-uwliptablesv6config)


file stat /etc/iptables/rules.v6
```
gid: 0
group: root
mode: '0600'
size: 369
uid: 0
user: root
```
file data /etc/iptables/rules.v6
```
*filter
:INPUT ACCEPT [0:0]
:FORWARD DROP [0:0]
:OUTPUT ACCEPT [0:0]
-A INPUT -i lo -j ACCEPT
-A INPUT -m state --state RELATED,ESTABLISHED -j ACCEPT
-A INPUT -p icmp -j ACCEPT
-A INPUT -p tcp -m state --state NEW -m tcp --dport 80 -j ACCEPT
-A INPUT -p tcp -m state --state NEW -m tcp --dport 443 -j ACCEPT
-A INPUT -j REJECT --reject-with icmp6-adm-prohibited
COMMIT

```

`cmd: uwl.iptables.v6.config`
-----------------------------------------------------------------
 * state: cmd.wait_script
 * name: `salt://uwl/iptables/v6/script.sh`

run or update after changes in:
 * [file: uwl.iptables.v6.config](#file-uwliptablesv6config)
 * [file: uwl.iptables.v6.config](#file-uwliptablesv6config)

require:
 * [file: uwl.iptables.v6.config](#file-uwliptablesv6config)


```
stateful: true
template: jinja
```

`file: uwl.motd remove legal notice`
-----------------------------------------------------------------
 * state: file.absent
 * name: `/etc/legal`



`file: uwl.motd /etc/update-motd.d/10-help-text`
-----------------------------------------------------------------
 * state: file.managed
 * name: `/etc/update-motd.d/10-help-text`


file stat /etc/update-motd.d/10-help-text
```
gid: 0
group: root
mode: '0755'
size: 10
uid: 0
user: root
```
file data /etc/update-motd.d/10-help-text
```
#!/bin/sh

```

`file: uwl.motd /etc/update-motd.d/00-header`
-----------------------------------------------------------------
 * state: file.managed
 * name: `/etc/update-motd.d/00-header`


file stat /etc/update-motd.d/00-header
```
gid: 0
group: root
mode: '0755'
size: 10
uid: 0
user: root
```
file data /etc/update-motd.d/00-header
```
#!/bin/sh

```

`file: uwl.motd static message`
-----------------------------------------------------------------
 * state: file.managed
 * name: `/etc/motd`


file stat /etc/motd
```
gid: 0
group: root
mode: '0644'
size: 593
uid: 0
user: root
```
file data /etc/motd
```
----------------------------------------------------------
OS     : Ubuntu-16.04
CORES  : 2
MEMORY : 1GiB available to users
FQDN   : opencs-fe-p01-01.uwaterloo.ca


More help, please see:
w3m https://uwaterloo.ca/math-faculty-computing-facility

Policy and computer ethics:
w3m https://uwaterloo.ca/ist/about/policies-standards-and-guidelines

If you have any problems using your UNIX, NEXUS, or Mac
accounts, ask the consultants for assistance.
Phone 888-4567 x36323, visit MC3017, or email mfcfhelp@uwaterloo.ca

Happy computing!

----------------------------------------------------------

```

`pkg: uwl.lldpd`
-----------------------------------------------------------------
 * state: pkg.installed
 * name: `uwl.lldpd`



```
install: ['lldpd']
```

`service: uwl.lldpd`
-----------------------------------------------------------------
 * state: service.running
 * name: `lldpd`

run or update after changes in:
 * [pkg: uwl.lldpd](#pkg-uwllldpd)


```
enable: true
```

`file: uwl._state.user managed root file /root/.ssh/authorized_keys`
-----------------------------------------------------------------
 * state: file.managed
 * name: `/root/.ssh/authorized_keys`


file stat /root/.ssh/authorized_keys
```
gid: 0
group: root
mode: '0600'
size: 2650
uid: 0
user: root
```
file data /root/.ssh/authorized_keys
```
# __global
ssh-rsa XXXXXXX root@mfcf.math

ssh-rsa XXXXXXX s8weber@mbp

# _cscf
ssh-dss XXXXXXX root@cscf-admin.host
# _cscf_ft_staff_admin
# todo
# _mfcf
ssh-rsa XXXXXXX root@mfcf.math

ssh-rsa XXXXXXX s8weber@mbp
# local
ssh-rsa XXXXXXX s8weber@mbp


```

`pkg: _common.pkgs`
-----------------------------------------------------------------
 * state: pkg.installed
 * name: `_common.pkgs`



```
install: ['lsof', 'dnsutils', 'wget', 'curl', 'git', 'man', 'vim', 'ed', 'nano', 'screen', 'tmux', 'sshfs']
```

`pkg: _common.cpu_microcode`
-----------------------------------------------------------------
 * state: pkg.installed
 * name: `_common.cpu_microcode`



```
install: ['intel-microcode', 'amd64-microcode']
```

`test: _common.cpu_microcode`
-----------------------------------------------------------------
 * state: test.fail_without_changes
 * name: `REQUIRE REBOOT AFTER MICROCODE CHANGES!!!`


```
onchanges:
- pkg: _common.cpu_microcode
```

`pkg: uwl.sshd`
-----------------------------------------------------------------
 * state: pkg.installed
 * name: `uwl.sshd`



```
install: ['openssh-server']
```

`service: uwl.sshd`
-----------------------------------------------------------------
 * state: service.running
 * name: `ssh`

require:
 * [pkg: uwl.sshd](#pkg-uwlsshd)


```
enable: true
```

`pkg: uwl.autofs`
-----------------------------------------------------------------
 * state: pkg.installed
 * name: `uwl.autofs`



```
install: ['autofs']
```

`service: uwl.autofs`
-----------------------------------------------------------------
 * state: service.running
 * name: `autofs`

run or update after changes in:
 * [file: uwl.autofs.config - /etc/auto.opencs_vol](#file-uwlautofsconfig---etcautoopencs_vol)
 * [file: uwl.autofs.config](#file-uwlautofsconfig)

require:
 * [pkg: uwl.autofs](#pkg-uwlautofs)


```
enable: true
```

`file: uwl.autofs.config`
-----------------------------------------------------------------
 * state: file.blockreplace
 * name: `/etc/auto.master`

after changes, run or update:
 * [service: uwl.autofs](#service-uwlautofs)

require:
 * [pkg: uwl.autofs](#pkg-uwlautofs)


ensure block of content is in file
```
#uwl.autofs.config
/-   /etc/auto.opencs_vol

```

`file: uwl.autofs.config - /etc/auto.opencs_vol`
-----------------------------------------------------------------
 * state: file.managed
 * name: `/etc/auto.opencs_vol`

after changes, run or update:
 * [service: uwl.autofs](#service-uwlautofs)


file stat /etc/auto.opencs_vol
```
gid: 0
group: root
mode: '0600'
size: 127
uid: 0
user: root
```
file data /etc/auto.opencs_vol
```
/mnt/opencs_vol  -fstype=nfs,vers=3,ro,sec=sys,soft,timeo=5,retrans=1,nosuid  fs107b.math.private.uwaterloo.ca:/opencs_vol/p01

```

`pkg: uwl.nginx`
-----------------------------------------------------------------
 * state: pkg.installed
 * name: `uwl.nginx`



```
install: ['nginx-extras']
```

`service: uwl.nginx`
-----------------------------------------------------------------
 * state: service.running
 * name: `nginx`

run or update after changes in:
 * [pkg: uwl.nginx](#pkg-uwlnginx)
 * [pkg: cscf.service.opencs.01.fe.nginx_site geoip packages](#pkg-cscfserviceopencs01fenginx_site-geoip-packages)
 * [file: cscf.service.opencs.01.fe.nginx_site - favicon](#file-cscfserviceopencs01fenginx_site---favicon)
 * [file: cscf.service.opencs.01.fe.nginx_site](#file-cscfserviceopencs01fenginx_site)
 * [file: cscf.service.opencs.01.fe.nginx_site - error_page](#file-cscfserviceopencs01fenginx_site---error_page)


```
enable: true
```

`file: cscf.service.opencs.01.fe.nginx_site`
-----------------------------------------------------------------
 * state: file.managed
 * name: `/etc/nginx/sites-enabled/default`

after changes, run or update:
 * [service: uwl.nginx](#service-uwlnginx)

require:
 * [file: uwl.tls.file private /etc/nginx/ssl_open.cs.uwaterloo.ca.pem](#file-uwltlsfile-private-etcnginxssl_opencsuwaterloocapem)
 * [file: uwl.tls.file private /etc/nginx/ssl_opencs.uwaterloo.ca.pem](#file-uwltlsfile-private-etcnginxssl_opencsuwaterloocapem)
 * [pkg: cscf.service.opencs.01.fe.nginx_site geoip packages](#pkg-cscfserviceopencs01fenginx_site-geoip-packages)
 * [file: uwl.tls.file cert /etc/nginx/ssl_dhparam4096.pem](#file-uwltlsfile-cert-etcnginxssl_dhparam4096pem)


file stat /etc/nginx/sites-enabled/default
```
gid: 0
group: root
mode: '0644'
size: 6297
uid: 0
user: root
```
file data /etc/nginx/sites-enabled/default
```
[[skipped binary data]]
```

`pkg: cscf.service.opencs.01.fe.nginx_site geoip packages`
-----------------------------------------------------------------
 * state: pkg.installed
 * name: `cscf.service.opencs.01.fe.nginx_site geoip packages`

after changes, run or update:
 * [service: uwl.nginx](#service-uwlnginx)

required in:
 * [file: cscf.service.opencs.01.fe.nginx_site](#file-cscfserviceopencs01fenginx_site)



```
install: ['geoip-database-contrib', 'libgeoip1']
```

`file: cscf.service.opencs.01.fe.nginx_site - error_page`
-----------------------------------------------------------------
 * state: file.managed
 * name: `/usr/share/nginx/html/opencs_error.html`

after changes, run or update:
 * [service: uwl.nginx](#service-uwlnginx)


file stat /usr/share/nginx/html/opencs_error.html
```
gid: 0
group: root
mode: '0644'
size: 867
uid: 0
user: root
```
file data /usr/share/nginx/html/opencs_error.html
```
<!DOCTYPE html>
<html>
    <head>
    <title>Issue</title>
    </head>
    <body>
        <h1>Opps</h1>
        <p>Sorry, because of maintainance or an issue this service is temporarily offline.</p>
        <p>We are likely aware of the issue and actively working on the solution.</p>
        <p>Please reload this page or try again in 3 minutes.<p>
    </body>
    <pre>
                                     ,
              ,-.       _,---._ __  / \
             /  )    .-'       `./ /   \
            (  (   ,'            `/    /|
             \  `-"             \'\   / |
              `.              ,  \ \ /  |
               /`.          ,'-`----Y   |
              (            ;        |   '
              |  ,-.    ,-'         |  /
              |  | (   |            | /
              )  |  \  `.___________|/
              `--'   `--'
    </pre>
</html>

```

`file: cscf.service.opencs.01.fe.nginx_site - favicon`
-----------------------------------------------------------------
 * state: file.managed
 * name: `/usr/share/nginx/html/opencs_favicon.ico`

after changes, run or update:
 * [service: uwl.nginx](#service-uwlnginx)


file stat /usr/share/nginx/html/opencs_favicon.ico
```
gid: 0
group: root
mode: '0644'
size: 6518
uid: 0
user: root
```
file data /usr/share/nginx/html/opencs_favicon.ico
```
[[skipped binary data]]
```

`cmd: uwl.systemd reload units`
-----------------------------------------------------------------
 * state: cmd.watch
 * name: `systemctl daemon-reload`

run or update after changes in:
 * [file: cscf.service.opencs.01.fe.nginx-parse-log service](#file-cscfserviceopencs01fenginx-parse-log-service)



`pkg: cscf.service.opencs.01.fe.nginx-parse-log package`
-----------------------------------------------------------------
 * state: pkg.installed
 * name: `cscf.service.opencs.01.fe.nginx-parse-log package`



```
install: ['python3', 'python3-requests']
```

`file: cscf.service.opencs.01.fe.nginx-parse-log package`
-----------------------------------------------------------------
 * state: file.managed
 * name: `/srv/nginx-parse-log.py`

require:
 * [pkg: cscf.service.opencs.01.fe.nginx-parse-log package](#pkg-cscfserviceopencs01fenginx-parse-log-package)


file stat /srv/nginx-parse-log.py
```
gid: 0
group: root
mode: '0770'
size: 6881
uid: 0
user: root
```
file data /srv/nginx-parse-log.py
```
#!/usr/bin/python3
import sys
import signal
import json
import requests
import re

host = 'NONE.math.private.uwaterloo.ca'
url = None
if len(sys.argv) == 2:
    host = sys.argv[1]
elif len(sys.argv) == 3:
    host = sys.argv[1]
    url = sys.argv[2]
else:
    print('todo: help - view source code', sys.argv)
    sys.exit(3)


import io
import threading
from threading import RLock

class Buffer_StringIO:
    '''
    buffer that auto flushes after max_delay or when buffer hits max_writes.
    '''
    data = io.StringIO()
    write_count = 0
    flush_timer = None
    onflush = None
    def __init__(self, flush_callback, flush_array=False, max_writes=200, max_delay=5.0 ):
        self.flush_callback = flush_callback
        self.flush_array = flush_array
        self.max_writes = max_writes
        self.max_delay = max_delay
        self.lock = RLock()
    def write(self, data):
        with self.lock:
            self.data.write(data)
            self.write_count += 1
            if self.write_count > self.max_writes:
                self.flush()
            if self.flush_timer == None or self.flush_timer.isAlive() == False:
                self.flush_timer = threading.Timer(self.max_delay, self.flush)
                self.flush_timer.start()
    def flush(self):
        with self.lock:
            if self.flush_timer != None:
                self.flush_timer.cancel()
            if self.write_count > 0:
                out = ''
                if self.flush_array:
                    self.data.seek(0)
                    out = self.data.readlines()
                else:
                    out = self.data.getvalue()
                if len(out) > 0:
                    self.flush_callback(out)
                self.data.truncate(0)
                self.data.seek(0)
                self.write_count = 0


#  Note: the alphabet in geohash differs from the common base32
#  alphabet described in IETF's RFC 4648
#  (http://tools.ietf.org/html/rfc4648)
__base32 = '0123456789bcdefghjkmnpqrstuvwxyz'
def geohash_encode(latitude, longitude, precision=12):
    """
    Encode a position given in float arguments latitude, longitude to
    a geohash which will have the character count precision.
    """
    # TODO: CACHE
    lat_interval, lon_interval = (-90.0, 90.0), (-180.0, 180.0)
    geohash = []
    bits = [ 16, 8, 4, 2, 1 ]
    bit = 0
    ch = 0
    even = True
    while len(geohash) < precision:
        if even:
            mid = (lon_interval[0] + lon_interval[1]) / 2
            if longitude > mid:
                ch |= bits[bit]
                lon_interval = (mid, lon_interval[1])
            else:
                lon_interval = (lon_interval[0], mid)
        else:
            mid = (lat_interval[0] + lat_interval[1]) / 2
            if latitude > mid:
                ch |= bits[bit]
                lat_interval = (mid, lat_interval[1])
            else:
                lat_interval = (lat_interval[0], mid)
        even = not even
        if bit < 4:
            bit += 1
        else:
            geohash += __base32[ch]
            bit = 0
            ch = 0
    return ''.join(geohash)


class Parse:
    buff = None
    url = ''
    host = ''
    #EXAMPLE:  "oat-fe-p01-01 nginx: 99.253.152.9 - - [10/Sep/2016:10:10:13 -0400] \"GET /api/v2/queue/1/status?dateString=2016-09-09 HTTP/1.1\" 200 222 \"https://oat.uwaterloo.ca/q/cs\" \"Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/52.0.2743.116 Safari/537.36\" \"CA\" \"43.4961\" \"-80.4936\""
    #NGINX_LOG_FORMAT '$remote_addr - $remote_user [$time_local] '
    '"$request" $status $body_bytes_sent '
    '"$http_referer" "$http_user_agent" '
    '"$geoip_country_code" "$geoip_latitude" "$geoip_longitude"';
    re_nginxlog = re.compile(r''
        '(.*)\snginx:\s'
        '(\d+.\d+.\d+.\d+)\s' #IP address
        '-\s' # NA?
        '-\s' #$remote_user (empty)
        '\[(.+)\]\s' #$time_local
        '"(\S+)\s' #$request_type
        '(\S+)\s\w+/\S+"\s' #$request
        '(\d+)\s' #$status
        '(\d+)\s' #$body_bytes_sent
        '"(\S+)"\s' #$http_referer
        '"([^"]+)"\s' #$http_user_agent
        '"(\S+)"\s' #$geoip_country_code
        '"([^"]+)"\s' #$geoip_city
        '"(\S+)"\s' #$geoip_latitude
        '"(\S+)"' #$geoip_longitude
    )

    def __init__(self, host, url):
        self.buff = Buffer_StringIO(flush_callback=self.post_flush)
        self.url = url
        self.host = host

    def readline(self, data):
        if len(data) < 5:
            return None
        if not data[0] == '{':
            return None
        d = json.loads(data)

        postm = d['MESSAGE'].replace('\"','\'')
        self.post('nginx_log,host={0} msg=\"{1}\" {2}'.format(self.host, postm, d['__REALTIME_TIMESTAMP']))

        md = self.re_nginxlog.match(d['MESSAGE'])
        if md:
            ip = md.groups()[1]
            request_type = md.groups()[3].lower()
            request_path = md.groups()[4]
            request_status = md.groups()[5]
            geoip_country_code = md.groups()[9]
            geoip_city = md.groups()[10].replace(' ', '_')
            geoip_latitude = md.groups()[11]
            geoip_longitude = md.groups()[12]

            if ip.startswith('127.') or ip.startswith('10.') or ip.startswith('192.168.') or ip.startswith('129.97.') or ( ip.startswith('172.') and geoip_country_code == '-' ):
                geoip_country_code = 'CA'
                geoip_city = 'UWaterloo'
                geoip_latitude = '43.4715'
                geoip_longitude = '-80.5431'

            geoip_hash = '-'
            if not geoip_latitude == '-':
                geoip_hash = geohash_encode(float(geoip_latitude), float(geoip_longitude), 6)

            self.post('nginx_log_parse,host={0},country={1},city={6},geohash={3},request_type={7},request_status={8} ip=\"{2}\",request=\"{4}\" {5}'.format(self.host, geoip_country_code, ip, geoip_hash, request_path, d['__REALTIME_TIMESTAMP'], geoip_city, request_type, request_status))

        return None

    def post(self, data):
        #print(data)
        self.buff.write(data + '\n')

    def post_flush(self, data):
        #print('_flush', data)
        if self.url:
            try:
                # headers={'Content-Type': 'application/octet-stream'}
                r = requests.post(self.url, data)
                if (len(r.text)):
                    print(r.text)
            except Exception as ex:
                print(ex)
        else:
            print(data)

    def read_stdin(self):
        for line in sys.stdin:
            self.readline(line)
            sys.stdout.flush()
        self.buff.flush()
        sys.stdout.flush()


app = Parse(host, url)
def signal_handler(signal, frame):
    print('sending remaining buffer')
    app.buff.flush()
    sys.exit(0)

signal.signal(signal.SIGINT, signal_handler)
app.read_stdin()

```

`file: cscf.service.opencs.01.fe.nginx-parse-log service`
-----------------------------------------------------------------
 * state: file.managed
 * name: `/etc/systemd/system/nginx-parse-log.service`

after changes, run or update:
 * [cmd: uwl.systemd reload units](#cmd-uwlsystemd-reload-units)

require:
 * [file: cscf.service.opencs.01.fe.nginx-parse-log package](#file-cscfserviceopencs01fenginx-parse-log-package)


file stat /etc/systemd/system/nginx-parse-log.service
```
gid: 0
group: root
mode: '0644'
size: 361
uid: 0
user: root
```
file data /etc/systemd/system/nginx-parse-log.service
```
[Unit]
Description=nginx-parse-log

[Service]
Type=simple
ExecStart=/bin/bash -c "journalctl -n 100 -o json -f -u nginx | /srv/nginx-parse-log.py 'opencs-fe-p01-01.uwaterloo.ca' 'https://influxdb.math.uwaterloo.ca:8086/write/?XXXX
Nice=10
Restart=on-failure
RestartSec=10s

[Install]
WantedBy=multi-user.target

```

`service: cscf.service.opencs.01.fe.nginx-parse-log service`
-----------------------------------------------------------------
 * state: service.running
 * name: `nginx-parse-log`

run or update after changes in:
 * [file: cscf.service.opencs.01.fe.nginx-parse-log service](#file-cscfserviceopencs01fenginx-parse-log-service)
 * [file: cscf.service.opencs.01.fe.nginx-parse-log package](#file-cscfserviceopencs01fenginx-parse-log-package)


```
enable: true
```




Other information
=====================================================================================

```

salt grain: ip_interfaces
-----------------------------------------------------------------
[('ens160', ['129.97.206.15', 'fe80::250:56ff:fe84:658c']), ('lo', ['127.0.0.1', '::1'])]


salt grain: hwaddr_interfaces
-----------------------------------------------------------------
[('ens160', '00:50:56:84:65:8c'), ('lo', '00:00:00:00:00:00')]


# ip address show
-----------------------------------------------------------------
1: lo: <LOOPBACK,UP,LOWER_UP> mtu 65536 qdisc noqueue state UNKNOWN group default qlen 1
    link/loopback 00:00:00:00:00:00 brd 00:00:00:00:00:00
    inet 127.0.0.1/8 scope host lo
    inet6 ::1/128 scope host 
2: ens160: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc mq state UP group default qlen 1000
    link/ether 00:50:56:84:65:8c brd ff:ff:ff:ff:ff:ff
    inet 129.97.206.15/24 brd 129.97.206.255 scope global ens160
    inet6 fe80::250:56ff:fe84:658c/64 scope link


# ip route list table all
-----------------------------------------------------------------
default via 129.97.206.1 dev ens160 
129.97.206.0/24 dev ens160  proto kernel  scope link  src 129.97.206.15 
broadcast 127.0.0.0 dev lo  table local  proto kernel  scope link  src 127.0.0.1 
local 127.0.0.0/8 dev lo  table local  proto kernel  scope host  src 127.0.0.1 
local 127.0.0.1 dev lo  table local  proto kernel  scope host  src 127.0.0.1 
broadcast 127.255.255.255 dev lo  table local  proto kernel  scope link  src 127.0.0.1 
broadcast 129.97.206.0 dev ens160  table local  proto kernel  scope link  src 129.97.206.15 
local 129.97.206.15 dev ens160  table local  proto kernel  scope host  src 129.97.206.15 
broadcast 129.97.206.255 dev ens160  table local  proto kernel  scope link  src 129.97.206.15 
fe80::/64 dev ens160  proto kernel  metric 256  pref medium
unreachable default dev lo  table unspec  proto kernel  metric 4294967295  error -101 pref medium
local ::1 dev lo  table local  proto none  metric 0  pref medium
local fe80::250:56ff:fe84:658c dev lo  table local  proto none  metric 0  pref medium
ff00::/8 dev ens160  table local  metric 256  pref medium
unreachable default dev lo  table unspec  proto kernel  metric 4294967295  error -101 pref medium


# iptables-save
-----------------------------------------------------------------
*filter
:INPUT ACCEPT [0:0]
:FORWARD DROP [0:0]
:OUTPUT ACCEPT [0:0]
-A INPUT -i lo -j ACCEPT
-A INPUT -m state --state RELATED,ESTABLISHED -j ACCEPT
-A INPUT -p icmp -j ACCEPT
-A INPUT -s 129.97.111.118/32 -p tcp -m state --state NEW -m tcp --dport 5666 -j ACCEPT
-A INPUT -s 129.97.111.0/24 -p tcp -m state --state NEW -m tcp --dport 22 -j ACCEPT
-A INPUT -s 209.171.37.153/32 -j DROP
-A INPUT -p tcp -m state --state NEW -m tcp --dport 80 -j ACCEPT
-A INPUT -p tcp -m state --state NEW -m tcp --dport 443 -j ACCEPT
-A INPUT -j REJECT --reject-with icmp-host-prohibited
COMMIT


# ip6tables-save
-----------------------------------------------------------------
*filter
:INPUT ACCEPT [0:0]
:FORWARD DROP [0:0]
:OUTPUT ACCEPT [0:0]
-A INPUT -i lo -j ACCEPT
-A INPUT -m state --state RELATED,ESTABLISHED -j ACCEPT
-A INPUT -p icmp -j ACCEPT
-A INPUT -p tcp -m state --state NEW -m tcp --dport 80 -j ACCEPT
-A INPUT -p tcp -m state --state NEW -m tcp --dport 443 -j ACCEPT
-A INPUT -j REJECT --reject-with icmp6-adm-prohibited
COMMIT




# mount
-----------------------------------------------------------------
sysfs on /sys type sysfs (rw,nosuid,nodev,noexec,relatime)
proc on /proc type proc (rw,nosuid,nodev,noexec,relatime)
udev on /dev type devtmpfs (rw,nosuid,relatime,size=1006284k,nr_inodes=251571,mode=755)
devpts on /dev/pts type devpts (rw,nosuid,noexec,relatime,gid=5,mode=620,ptmxmode=000)
tmpfs on /run type tmpfs (rw,nosuid,noexec,relatime,size=204816k,mode=755)
/dev/sda1 on / type ext4 (rw,relatime,errors=remount-ro,data=ordered)
securityfs on /sys/kernel/security type securityfs (rw,nosuid,nodev,noexec,relatime)
tmpfs on /dev/shm type tmpfs (rw,nosuid,nodev)
tmpfs on /run/lock type tmpfs (rw,nosuid,nodev,noexec,relatime,size=5120k)
tmpfs on /sys/fs/cgroup type tmpfs (ro,nosuid,nodev,noexec,mode=755)
cgroup on /sys/fs/cgroup/systemd type cgroup (rw,nosuid,nodev,noexec,relatime,xattr,release_agent=/lib/systemd/systemd-cgroups-agent,name=systemd)
pstore on /sys/fs/pstore type pstore (rw,nosuid,nodev,noexec,relatime)
cgroup on /sys/fs/cgroup/freezer type cgroup (rw,nosuid,nodev,noexec,relatime,freezer)
cgroup on /sys/fs/cgroup/hugetlb type cgroup (rw,nosuid,nodev,noexec,relatime,hugetlb)
cgroup on /sys/fs/cgroup/net_cls,net_prio type cgroup (rw,nosuid,nodev,noexec,relatime,net_cls,net_prio)
cgroup on /sys/fs/cgroup/devices type cgroup (rw,nosuid,nodev,noexec,relatime,devices)
cgroup on /sys/fs/cgroup/pids type cgroup (rw,nosuid,nodev,noexec,relatime,pids)
cgroup on /sys/fs/cgroup/memory type cgroup (rw,nosuid,nodev,noexec,relatime,memory)
cgroup on /sys/fs/cgroup/blkio type cgroup (rw,nosuid,nodev,noexec,relatime,blkio)
cgroup on /sys/fs/cgroup/cpu,cpuacct type cgroup (rw,nosuid,nodev,noexec,relatime,cpu,cpuacct)
cgroup on /sys/fs/cgroup/cpuset type cgroup (rw,nosuid,nodev,noexec,relatime,cpuset)
cgroup on /sys/fs/cgroup/perf_event type cgroup (rw,nosuid,nodev,noexec,relatime,perf_event)
systemd-1 on /proc/sys/fs/binfmt_misc type autofs (rw,relatime,fd=26,pgrp=1,timeout=0,minproto=5,maxproto=5,direct)
debugfs on /sys/kernel/debug type debugfs (rw,relatime)
mqueue on /dev/mqueue type mqueue (rw,relatime)
hugetlbfs on /dev/hugepages type hugetlbfs (rw,relatime)
fusectl on /sys/fs/fuse/connections type fusectl (rw,relatime)
sunrpc on /run/rpc_pipefs type rpc_pipefs (rw,relatime)
/etc/auto.opencs_vol on /mnt/opencs_vol type autofs (rw,relatime,fd=6,pgrp=699,timeout=300,minproto=5,maxproto=5,direct)


```
