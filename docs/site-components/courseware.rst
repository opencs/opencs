.. Courseware django-app description


Courseware
==========

.. automodule:: courseware



Models
------

.. automodule:: courseware.models
    :members:
