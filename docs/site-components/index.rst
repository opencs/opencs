.. The django apps created for the project

Site Components
===============

This secition describes the individual components (Django apps) that make up the site. Briefly, the ``courseware`` app handles all of the organization and structure of courses, and thus most of the site is rendered through it. The REST API and help pages are handled by `Tastypie`_  and ``django-flatpages`` respectively. The rest of the content (what happens in each module) is handled by the individual apps.

Because most of the site is rendered through the courseware app, the associated client-side code for displaying the course content is included in ``courseware/static``, not in those individual apps (with a couple exceptions).

.. note:: Because of the high interdependence of the different module types with ``courseware``, they all essentially depend on the courseware app.


Contents:

.. toctree::

    courseware
    exploration
    quiz
    slideshow
    glossary

.. _Tastypie: http://tastypieapi.org/
