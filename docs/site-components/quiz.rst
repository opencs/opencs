.. quiz django-app description


Quiz
====

.. automodule:: quiz



Models
------

.. automodule:: quiz.models
    :members:
