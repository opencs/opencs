.. slideshow django-app description


Slide Show (and video lessons)
==============================

.. automodule:: slides



Models
------

.. automodule:: slides.models
    :members:
