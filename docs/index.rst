.. OpenCS documentation master file, created by
   sphinx-quickstart on Fri Apr 24 12:23:31 2015.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.


.. This is just a dumb comment

Welcome to OpenCS's documentation!
==================================

.. todo:: Add a description of what OpenCS actually is

.. todo:: We probably should have a note to check the README.md file (which may be more up to date).

.. The following was pulled from the README.md file

This is the main website for the OpenCS project. The main component is a Django-based website. There is the additional `python socket`_ which is a websocket that handles Python execution requests.

Course materials, and various specific components are all held in the `OpenCS group`_ at the University of Waterloo git host.

Contents:

.. toctree::
   :maxdepth: 3

   install
   design/index
   implementation
   site-components/index
   usage/index
   glossary


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

.. _python socket: https://git.uwaterloo.ca/opencs/python-socket
.. _OpenCS group: https://git.uwaterloo.ca/groups/opencs
