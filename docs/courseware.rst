``courseware`` --- The main app, which organizes the course, and displays the content.
======================================================================================

.. automodule:: courseware
   :members:

