.. Exploration design spec

Exploration
===========

An exploration is a collection of guided coding exercises. The purpose is to give students a chance to practice concepts introduced in the lessons.

Generally, an exploration will have a progress bar/indicator (at the top), so that users can see how far they are in the module. Excercises where all tests pass are coloured green in the progress indicator, but failed attempts *must not be coloured red*. Users are able to navigate to any exercise without successfully completing the previous one(s).

For each exercise, there may be some instruction, hints, and/or an explanation (to be displayed after the code is tested/executed). The instructions and post-run explanation may contain images. There will also be an input area for code, and an output area to show the results. Code is executed by clicking the 'run' button, and code can be reset by clicking the 'restore' button. If the code is run successfully, a green checkmark will appear.

Some of the exercises may build upon the previous exercise. Therefore, it must be possible to keep a user's code for the next exercise, or to append or prepend to it.

The following sections describe behaviour specific to each language.

Python
------

Users are provided with a code input for Python scripts, and an output console, which displays all stdout and stderr. Running the user's code should behave nearly identically to executing a script from the command line. In particular, it must support:

.. hlist::

    * ``print()``
    * ``input()``
    * module import
    * tracebacks

Of note: the ``input()`` function must accept user's input during execution.

The following are specifically not required (for security reasons):

.. hlist::

    * file creation

.. todo:: fill out the above lists

In addition to interactively executing the code, it will also be tested to make sure it is "correct". The following tests must be supported:

    Code Contents
        Verify that the users code contains or does not contain a substring.

    STDIO
        Spoof and monitor standard input and output. The tests pass if the correct output is received for each input.

        Special case: prompts from the ``input()`` function are ignored.

    Function Black Box
        Test a function which the user defines by checking its result against various arguments.

        These tests may contain a preamble to set up the state, followed by a series of assert statements.

    Numerical output
        This is a special case of STDIO. Some exercises ask to print a number, but floating point issues can interfere with comparing strings. This test checks that the outputted number is within a tolerance.

    No Exceptions
        By default any exceptions from user's code will fail this test. It can be turned off for some exercises.

Web Development
---------------

These explorations come in two basic varieties: demos and exercises. Both function the same way, in that users can change and re-render the material. However, the demos my require a slightly different presentation.

For the front-end web development course, users must be able to edit separate html, css, and javascript files (at most one each), and view the results in a panel on the page. The user-defined website needs to be fully functional and interactive. For some exercises, the output of ``console.log()`` must be displayed in another panel.

Because the exercises may contain any subset of the languages, and only a subset of those will need to be seen, the course author needs to be able to specify which code input panels are displayed for each exercise. Additionally, the preferred sizes may be different, so the author also needs to be able to specify a default initial size.
