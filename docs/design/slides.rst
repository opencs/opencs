.. Slide/text-based instruction design spec

Text Instruction
================

Some courses will require text-based instruction. The author's vision is that it would behave like a book, going from page to page. The connections between instructional slides and demos and explorations should be relatively seamless.

The text slides need to support:

* two columns (text flow is optional, but preferred)
* images within the columns. Most should be full-width according to the author.
* possibly single-column for larger images

