.. Documentation of the design specs

Design Specification
====================

OpenCS is a basic courseware framework to provide interactive educational content about computer programming. The majority of content is split into individual courses that focus on a language or framework. 


Site-wide Features
------------------

.. note:: 

    The boundaries between general site, and course-specific components has not
    been well-defined, particularly for how the main landing page looks.

The only real site-wide feature is the landing page, which is simply a list of courses. In the visual design, there is also a 'Help' button in the top-right. It is unclear whether this is general-site help, or course-specific help.

It is this author'ss opinion that it should not be tied to a specific course.


Course Design
-------------

The courses are where the bulk of the material is stored. Associated with each course is a Glossary, and some reference material, as well as a curriculum.

.. note:: **(2015-04-28)** As of this date, there is no clear spec for that reference material
.. note:: **(2015-04-28)** As of this date, there is no way to attach a glossary to a course.

The curriculum is organized in two levels: The course can have multiple *units*, and each unit has multiple *modules*.

.. caution::

    Between start of implementation, and creation/insertion of course content,
    some names for components were changed. In order to keep the code base
    consistent, the original names are in code, and in this documentation.
    
    The non-technical users of the OpenCS framework are more likely to use the
    new names. These names have changed from time to time. As of writing, here
    is the translation table (``original name -> new name``)::

        unit -> module
        module -> step
        quiz -> quickclick

A module can be one of

.. toctree::
    :maxdepth: 1

    videolesson
    exploration
    quiz
    glossary
    slides

The glossary was originally designed as a module in a unit (as a summary of the unit's concepts), but is now primarily a couse-wide object. It is left as an option in case it is desired in future courses.


.. todo:: read over this document
