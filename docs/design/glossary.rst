.. glossary design spec

Glossary
========

For each course and unit, a glossary is to be provided for users to review the contents. In addition to the glossary page, individual terms in other site components (namely explorations and quick-clicks) can have their definitions displayed. 

Each course will have a glossary, which can be accessed from a *resources* menu at the top of the page. This glossary will show all the terms that are defined in the course.

.. note:: **(2015-05-11)** Initial design had the glossary/summary as a final step of a unit. This is no longer the design, but it is supported in the software.

In explorations and quizzes, words or phrases can be highlighted, and when the user interacts with the term, its definition will display as a popover. 

.. note:: The actual decoration and type of user interaction will have to be checked with an accessibility/UI specialist.
