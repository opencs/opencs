.. Quiz design spec

Quiz (Quick Click)
==================

A quiz is a collection of questions to help the students perform self-evaluation. Most questions will be multiple choice, but some will also be short-form answer (with a text-input).

For consistency, the appearance of a quiz should be the same as an exploration. That is, a progress indicator at the top, with question on the right, and interactive elements on the left. 

When the user answers the question correctly, the progress indicator should display it as green, a checkmark should appear, and an explanation for why that answer is correct should be displayed.

When the user's answer is incorrect, a hint should be given (or an explanation of why it is incorrect), and nothing should turn red or display any language like "fail" or "wrong".

Some questions allow multiple answers. In this case, all correct selections will show an explanation, and all incorrect selection will show a hint.

The user can re-check their answers as many times as they'd like, even if they got the correct answer. This is to allow them to explore and see why any choices are incorrect (by looking at all the hints).

.. note:: These quizzes are specifically not for outside evaluation (no mark is given). This avoids the need for storing user's information, and giving other users (teachers) access to a user's (student's) information.
