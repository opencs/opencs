.. videolesson design spec

Video Lesson
============

The majority of content for the Python course is delivered through video lectures. The lessons are split into chunks, each about 5 minutes, and contain either language-specific or language-independent content.

The videos that are language-independent may be included or linked to from any course, but language-specific videos are not expected to be used outside of their own unit. These videos are to be distinguished in the unit index with different icons.

The page for the videos should look consistent with the rest of the site, including any navigation buttons or displays. The video player must reasonably well-supported across devices (iPad, Chrome, Firefox, IE), an ideally would fall back to older methods (i.e. flash). Users must be able to seek to inside videos, and pause and play.

.. note:: **(2015-05-04)** Pending review: adjustible video speeds

.. seealso:: modules :py:mod:`slides`
