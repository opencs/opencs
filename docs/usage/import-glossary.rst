.. exploration import script documentation

``import-glossary.sh``
=========================

This script imports glossary files into the database. Usage::

    import-glossary glossary-file.tsv

.. note::

    The glossary is not accessible until it has been attached to a course.
    Because there is no indication of which course it is for in the spreadsheet, this change must be done manually through the admin interface.

    1. Go to /admin/
    2. Click 'course'
    3. Click the relevant course to edit
    4. Select the new glossary from the combobox beside "glossary:"

.. automodule:: OpenCS.management.commands.import_glossary
