.. glossary linking script documentation

``link-glossary.sh``
=========================

This script is used to link glossaries. It calls the ``link_glossary`` django command with all of its arguments, meaning the syntax is::

    link-glossary.sh [<course-slug> [<module-number> [<step-number>]]]

.. note::
    This command is now only a fix for glossary spans without the ``glossary`` attribute set. It will likely never need to be run.

.. automodule:: OpenCS.management.commands.link_glossary
