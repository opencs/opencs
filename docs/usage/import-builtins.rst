.. exploration import script documentation

``import-builtins.sh``
======================

This script imports built-ins collection files into the database. Usage::

    import-glossary glossary-file.tsv course-slug

.. note::

    The built-ins collection is not accessible until it has been attached to a course.
    Because we may want to retain the existing collection (to make sure the new one is correct), this change must be done manually through the admin interface.

    1. Go to /admin/
    2. Click 'course'
    3. Click the relevant course to edit
    4. Select the new glossary from the combobox beside "glossary:"

.. automodule:: OpenCS.management.commands.import_builtins
