.. flat pages import script documentation

``update-pages.sh``
=========================

This script is used to update the flat-pages (static pages used mainly for resources and help). It calls the ``update_pages`` django command for each argument, which means file globbing is accepted. For example,::

    update-pages.sh *.md

will import all the pages contained in the directory's markdown files.

.. todo:: Include information about the flat-pages format, and some template files.

.. automodule:: OpenCS.management.commands.update_pages
