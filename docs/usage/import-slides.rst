.. slides import script documentation

``import-slides.sh``
=========================

This script is used to import a slides file. It calls the ``import_slides`` django command for each argument, which means file globbing is accepted. For example,::

    import-slides.sh slides*.md

will import each slideshow in the current directory.

.. todo:: Make this information specific to slides format

An exploration file has the following structure::

    header-line

    preamble

    <!-- BEGIN - left-pane-size -->

    slides

The value ``left-pane-size`` is an integer that specifies the ratio between the left and right pane. The ratio is left-pane-size:6. This way, the ratios 1:6, 1:3, 1:2, 2:3, 1:1, 3:2, etc. can be obtained with the values 1, 2, 3, 4, 6, 9, etc. Note that ``left-pane-size`` is optional and defaults to the value 6.

The pattern for ``header-line`` is::

    <!-- Module module-number, Step step-number, step-type -->

The values of ``module-number`` and ``step-number`` are numbers, and ``step-type`` can be one of 'exploration' or 'demo'.

The preamble section holds additional metadata, specifically the title and description::

    Title: title-text
    description-text

This metadata is only read if the title is present.

The ``slides`` section has the form::

    text
    code-blocks
    post-text
    <!-- Continue - left-pane-size -->

The value of ``left-pane-size`` has identical meaning and behaviour as described above (in the ``BEGIN`` line).

The ``text`` and ``post-text`` sections are both parsed the same way: converted from Markdown to HTML with the additional syntax,

.. automodule:: OpenCS.management.commands.import_slides
