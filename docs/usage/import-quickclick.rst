.. exploration import script documentation

``import-quickclick.sh``
=========================

This script is used to import a quickclick file. It calls the ``import_quickclick`` django command for each argument, which means file globbing is accepted. For example,::

    import-quickclick.sh quickclick*.tex

will import each quickclick in the current directory.

Quickclick Format (markdown)
----------------------------

A quickclick file has the following structure::

    header-line
    
    preamble

    <!-- BEGIN -->

    questions

where ``header-line`` is of the form::

    <!-- Module module-number, Step step-number, step-type -->

The values of ``module-number`` and ``step-number`` are numbers, and ``step-type`` is ``quickclick``. The regex for this pattern is::

    <!--+\s*[Mm]odule\s*(\d+)[,\s]+[Ss]tep\s*(\d+)[,\s]+([a-zA-Z])+\s* --+>

Which means the commas are optional, and the first letter of each word is case-insensitive.

.. note:: The first letter of each word in an html comment is case-insensitive. That is, ``<!-- Question -->`` is accepted as is ``<!-- question -->``.

The preamble section holds additional metadata, specifically the title and description::

    Title: title-text
    description-text

This metadata is only read if the title is present.

The ``question`` section has the form::

    <!-- Question -->
    text
    choices

The ``text`` section is parsed the same way: converted from Markdown to HTML with the additional syntax,

+-------------------+-------------------------------+--------------------------------------------+
| purpose           | markdown                      | substitution                               |
+===================+===============================+============================================+
| first use of word | ``!d[word]``                  | ``<span class="first-use">word</span>``    |
+-------------------+-------------------------------+--------------------------------------------+
| built-in          | ``!b[foo]``                   | ``<span class="built-in">foo</span>``      |
+-------------------+-------------------------------+--------------------------------------------+
| glossary          | ``!g[term]``                  | ``<span glossary="term">term</span>``      |
+-------------------+-------------------------------+--------------------------------------------+
| glossary          | ``!g[display](term)``         | ``<span glossary="term">display</span>``   |
+-------------------+-------------------------------+--------------------------------------------+


``Choices`` can be one of two forms depending on the type of question (``choices -> short | multiple``).

A short-form question has a choice block::

    <!-- correct answer -->
    <!-- hint -->
    hint-text
    <!-- explanation -->
    explanation-text

where

* ``answer`` is answer to the question
* ``hint-text`` is the hint displayed if the user is incorrect
* ``explanation-text`` is the explanation for the correct answer
* ``explanation`` can be either the text 'Explanation' or 'Feedback'

The hint and explanation texts are parsed the same was as the question text. Both are optional, and can appear in either order. Multiple correct answers can also be specified by repeating this block once for each.

A multiple choice question has the choice block::

    <!-- Choices -->
    * choice-text
    ...

    <!-- hints -->
    * hint-text
    ...

    <!-- explanations -->
    * explanation-text
    ...

Here, ``...`` indicates that the preceding line can be repeated an arbitrary number of times. All the `*-text` objects can span multiple lines, and are passed through the same Markdown -> html parser as for the question text. Each list must have the same number of entries. The 'explanations' block can be omitted, in which case, the hint for a correct answer is used as the explanation.

To indicate a correct answer, the string "<!-- correct -->" or "<!-- multicorrect -->" must appear inside ``choice-text``. If all correct answers must be selected, then at least one of the choice texts must contain the string "multicorrect" (typically replacing "correct" in the previously mentioned string.

only one complete choice block is permitted per multiple-choice question.

.. todo:: Include some template files.

.. automodule:: OpenCS.management.commands.import_quickclick
