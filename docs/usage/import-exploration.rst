.. exploration import script documentation

``import-exploration.sh``
=========================

This script is used to import an exploration file. It calls the ``import-exploration`` django command for each argument, which means file globbing is accepted. For example,::

    import-exploration.sh explore*.tex

will import each exploration in the current directory.

Exploration Format (markdown)
-----------------------------

An exploration file has the following structure::

    header-line

    preamble

    <!-- BEGIN - left-pane-size -->

    slides

The value ``left-pane-size`` is an integer that specifies the ratio between the left and right pane. The ratio is left-pane-size:6. This way, the ratios 1:6, 1:3, 1:2, 2:3, 1:1, 3:2, etc. can be obtained with the values 1, 2, 3, 4, 6, 9, etc. Note that ``left-pane-size`` is optional and defaults to the value 6.

The pattern for ``header-line`` is::

    <!-- Module module-number, Step step-number, step-type -->

The values of ``module-number`` and ``step-number`` are numbers, and ``step-type`` can be one of 'exploration' or 'demo'.

The preamble section holds additional metadata, specifically the title and description::

    Title: title-text
    description-text

This metadata is only read if the title is present.

The ``slides`` section has the form::

    text
    code-blocks
    post-text
    <!-- Continue - left-pane-size -->

The value of ``left-pane-size`` has identical meaning and behaviour as described above (in the ``BEGIN`` line).

The ``text`` and ``post-text`` sections are both parsed the same way: converted from Markdown to HTML with the additional syntax,

+-------------------+-------------------------------+--------------------------------------------+
|purpose            |markdown                       |substitution                                |
+===================+===============================+============================================+
|first use of word  |``!f[word]``                   |<span class="first-use">word</span>         |
+-------------------+-------------------------------+--------------------------------------------+
|built-in           |``!b[foo]``                    |<span class="built-in">foo</span>           |
+-------------------+-------------------------------+--------------------------------------------+
|glossary           |``!g[term]``                   |<span glossary="term">term</span>           |
+-------------------+-------------------------------+--------------------------------------------+
|glossary           |``!g[display](term)``          |<span glossary="term">display</span>        |
+-------------------+-------------------------------+--------------------------------------------+
|hint               |``<!-- Hint - hint_text -->``  |None (passes hint_text through the markdown |
|                   |                               |parser, and adds it to the list of hints)   |
+-------------------+-------------------------------+--------------------------------------------+

The ``code-blocks`` section is a collection of lines::

    <!-- filetype - action - filename - relative_size -->

where

* ``filetype`` is one of 'html', 'css', 'js'
* ``action`` is one of 'keep', 'replace', 'prepend', 'append'
* ``filename`` (optional) is the name of the source file to be included (relative to the exploration file)
* ``size`` (optional) is the relative size of the codeBlock when displayed (6 is default, 0 means hidden)

.. note:: Files other than html/css/js that are used in the demo cannot be uploaded through the web container service. Any images or other media used in demo sources must be uploeaded separately, and referred to with an absolute path.

    For example, ``<img src="/media/demos/images/example.png" />`` works, but ``<img src="images/example.png" />`` does not work.

.. note:: Within a slide (i.e. until the next ``<!-- continue -->`` statement), any text between code-block lines is ignored.

.. todo:: include a template file as an example

.. automodule:: OpenCS.management.commands.import_exploration
