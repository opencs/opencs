.. exploration import script documentation

``update-snippets.sh``
=========================

This script is used to import an snippet file. It calls the ``update_snippets`` django command for each argument, which means file globbing is accepted. For example,::

    update_snippets.sh *.py

will import every snippet in the python files in the current directory

.. todo:: Include information about the exploration format, and some template files.

``update_snippets`` command
------------------------------

.. automodule:: OpenCS.management.commands.update_snippets
