.. exploration import script documentation

``import-video.sh``
=========================

This script is used to import a video file. It calls the ``import_video`` django command for each argument, which means file globbing is accepted. For example,::

    import-video videos/*.mp4

will import each video in the ``videos/`` directory.

.. automodule:: OpenCS.management.commands.import_video
