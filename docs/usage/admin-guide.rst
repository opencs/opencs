.. _admin-guide:

Admin Guide
===========

The main admin site is split into different components/sections (corresponding to the django apps that control the content). The courseware app is for course organization, '*Flat Pages*' is for the help pages, and the rest correspond to the various content-types.

Clicking on a database model displays a table of all the content of that type. Many tables also have editable values for the most-changed properties. These include:

- whether the course should show on the main page
- order of modules (or units or course) inside a unit (or course or index)
- which unit (or course) a module (or unit) is in

Further fields are accessible by clicking on a particular instance of the model. Most content can be edited this way. It is preferable that any changes to videos, glossary terms, or titles are made through the admin interface.

.. note:: Full control over explorations and quickclicks is lacking, because the content is quite complicated. The best way to make changes for these items is to re-import them with an admin script.

About Course Organization
-------------------------

There are 3 levels to a course:

1. Course
2. Unit
3. Module

A unit can only be assigned to one course, and a module can only be assigned to one unit. However, the module is really just a pointer to a piece of content (quickclick, exploration, video, etc.). This allows for re-use of a piece of content in different modules.

For example, a video lesson on sequences can be assigned to a module in unit 5 of the "Python from scratch" course, and also to a module in unit 2 of the "Language-independent" course

An entire unit, or just a module can be reviewed in a staging course first, then moved into place later using the table views. Because the modules are tied only to the unit (not the course), if the unit is moved, all modules within it are moved as well.
