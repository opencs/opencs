.. How to use the site as an administrator

Administrative usage
====================

There are two ways to add or change course content in the website. The `administrative interface`_, or through the command line on the servers.

See :ref:`admin-guide` for details of how to use the administrative interface.

The scripts are used as the primary method of data entry, because the workflow is such that the author creates material in text files (of various formats) then sends it to the producer to enter into the site. The scripts help automate the entry process for bulk data entry.

As such, the admin site is only really intended for post-import editing and fixes. Additionally, the data structures are rather complex, so the automatically generated admin site is a bit lacking in functionality. Tasks that must be done through the admin site include,

* Setting exploration exercises to permit raised exceptions in Python.
* Moving modules and units out of the staging area, into a public course.

The following scripts are used to enter and/or update course data:

.. toctree::
    :maxdepth: 1
    
    import-video
    import-quickclick
    import-exploration
    import-slides
    import-glossary
    import-builtins
    link-glossary
    update-pages
    update-snippets

.. _administrative interface: https://open.cs.uwaterloo.ca/admin/
