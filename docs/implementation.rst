.. Documentation on how to install the project for development and deployment

Implementation of Design
========================

The main stack for the website is `Django`_ on the server, and `AngularJS`_ for interactive components on the website. An additional service is needed for the interactive Python environment, which is provided through a `Tornado`_ web server with a websocket. The websocket interface is needed, because live streaming and interactive ``input()`` commands are in the design specifications.

The actual server infrastructure uses `Nginx`_ as the front-end proxy, which performs load-balancing to the Django and Tornado services. The Django database is managed by `Postgresql`_. Each service is hosted on its own virtual machine for extensibility and load-balancing.

Django Host
-----------

Django is used to provide the (essentially) static content and the database interface. Responsibilities include:

* URL managment, and content provision
* A REST interface to all course content (currently provided by the `Tastypie`_ plugin)
* Flat pages for resources
* Templates for a consistent theme

AngularJS
---------

AngularJS is only used to make the individual pages interactive. It is not used for routing requests, or loading new content (apart from glossary popups and code snippets). The primary parts of the site that use AngularJS are:

* Explorations (for going from one exercise to the next, and displaying results)
* Quickclicks (as for explorations)
* Glossary popups
* Content injection, like snippets.


Python Socket
-------------

The interactive python service is handled by a websocket server. The source code is in the `opencs/python-socket <https://git.uwaterloo.ca/opencs/python-socket/>`_ repository in the university's git host. That repo has both the server, and a javascript client.

The javascript client is a ``PythonSocket`` object, which provides an event handling interface for test results and exec results, commands to send execution and test requests to the server, and management of the output panel (with interactive input).

.. todo:: document some of the usage

For details on the javascript client, as well as the message-passing specification, see that project's documentation.


HTML/css/javascript Service
---------------------------

The service to store users' code in the web development course is provided by a django app |webapp| which provides a very simple REST api.

The user's code is uploaded in a JSON formatted POST request, not as individual files. This is important, because it prevents loading arbitrary files. Therefore, no images or other media can be directly included in a demo/exploration. To get around this, the ``<img>`` tag's source needs to use an *absolute* path to the image, which is stored elsewhere.

.. http:post:: /web-container/(str: id)/

    Upload a website.

    **Example request**

    .. sourcecode:: http

        POST /webservice/ HTTP/1.1
        Host: open.cs.uwaterloo.ca
        Accept: application/json

        {
            "id": "optional id",
            "files":
                [{
                    "name": "index.html",
                    "content": "<html><body><p>Hello World!</p></body></html>"
                }, {
                    "name": "main.css",
                    "content": "/* this isn't actually linked to from the html */ p{color:red;}"
                }]
        }

    **Example response**

    .. sourcecode:: http

        HTTP/1.1 200 OK
        Vary: Accept
        Content-Type: application/json

        { 
          "id": "abc123",
          "url": "/location/of/page.html"
        }

    :query string id: (optional) The ID of the web content to be uploaded. If the id is already in use, a unique id is created.

    :<json string id: The requested id for the web content.
    :<json list files:  A list of file objects, each with ``name`` and ``content`` fields.

    :>json string id: The id for the content provided by the server.
    :>json string url: The url for the posted website. This is where the preview-frame should point.

.. http:DELETE:: /web-container/(str: id)/

    Remove the website with corresponding ``id``. (Be a good citizen, throw out your trash)

    **Example request**

    .. sourcecode:: http

        DELETE /webservice/abc123/ HTTP/1.1
        Host: open.cs.uwaterloo.ca

    **Example response**

    .. sourcecode:: http

        HTTP/1.1 200 OK

    :query string id: The ID of the web content to be removed

    :status 200: The web content has now been deleted.
    :status 410: There was no web content matching the ID provided.

.. http:GET:: /web-container/(str: id)/

    Redirect to the website with the corresponding ``id``. (Added as a convenience, but not actually used).

    **Example request**

    .. sourcecode:: http

        GET /webservice/abc123/ HTTP/1.1
        Host: open.cs.uwaterloo.ca

    **Example response**

    .. sourcecode:: http

        HTTP/1.1 301 0

    :query string id: The ID of the web content to be viewed

    :status 301: A redirection to the directory that the web content is stored in.
    :status 410: There was no web content matching the ID provided.

.. _Django: http://djangoproject.com/
.. _AngularJS: https://angularjs.org/
.. _Tornado: http://www.tornadoweb.org/
.. _Nginx: http://nginx.org/
.. _Postgresql: http://www.postgresql.org/
.. _Tastypie: http://tastypieapi.org/


.. |webapp| replace:: *webhost*
