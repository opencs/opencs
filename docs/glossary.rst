.. A glossary for design terms and lingo specific to this project

Project-Specific Terms
======================

.. glossary::

    Course
        A full curriculum on a large subject, like Python or Javascript, that encompasses a set of smaller subjects

    Unit
        A set of :term:`module`s that covers a small subject. Similar to a week of a university course.

        .. warning:: Inside the course material, this is refered to as a *module*

    Module
        A component of a :term:`unit`. It can be a :term:`video lesson`, :term:`exploration`, or :term:`quiz`
        
        .. warning:: Inside the course material, this is refered to as a *step*

    Video lesson
        .. todo:: write this

    Exploration
        .. todo:: write this

    Quiz
        .. todo:: write this
