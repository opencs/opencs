# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('snippets', '0001_initial'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='codesnippet',
            name='description',
        ),
        migrations.AlterField(
            model_name='codesnippet',
            name='name',
            field=models.CharField(max_length=80),
        ),
    ]
