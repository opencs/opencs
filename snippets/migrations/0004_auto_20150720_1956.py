# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('snippets', '0003_auto_20150527_1821'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='codesnippet',
            options={'ordering': ['module__unit__position', 'module__position', 'position']},
        ),
    ]
