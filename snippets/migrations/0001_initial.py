# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('courseware', '0006_auto_20150326_2104'),
    ]

    operations = [
        migrations.CreateModel(
            name='CodeSnippet',
            fields=[
                ('id', models.AutoField(serialize=False, auto_created=True, primary_key=True, verbose_name='ID')),
                ('code', models.TextField()),
                ('language', models.CharField(max_length=6, default='python', choices=[('python', 'Python')])),
                ('name', models.CharField(max_length=50)),
                ('description', models.CharField(max_length=80, blank=True)),
                ('module', models.ForeignKey(null=True, on_delete=django.db.models.deletion.SET_NULL, to='courseware.Module', blank=True)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
    ]
