# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('snippets', '0002_auto_20150513_1629'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='codesnippet',
            options={'ordering': ['position']},
        ),
        migrations.AddField(
            model_name='codesnippet',
            name='position',
            field=models.PositiveSmallIntegerField(default=0),
            preserve_default=True,
        ),
    ]
