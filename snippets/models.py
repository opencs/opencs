from django.db import models
from courseware.models import Module

class CodeSnippet(models.Model):
    """
    A code snippet for use in the python panel

    Attributes:
        code (TextField): The actual code snippet
        language (CharField): The language of the snippet (defaults to python)
        name (Charfield): Snippet Name
        module (ForeignKey(Module)): Optional link to a specific module
    """

    code = models.TextField()
    PYTHON = 'python'
    LANGUAGE_CHOICES = (
            (PYTHON, 'Python'),
            )
    language = models.CharField(max_length=6, choices=LANGUAGE_CHOICES, default=PYTHON)

    name = models.CharField(max_length=80)

    # Optionally connect the snippet with a module
    module = models.ForeignKey(Module, blank=True, null=True, on_delete=models.SET_NULL)
    position = models.PositiveSmallIntegerField(default=0);

    def __str__(self):
        return self.name

    class Meta:
        ordering = ['module__unit__position', 'module__position', 'position']
