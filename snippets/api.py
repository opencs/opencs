from tastypie.resources import ModelResource
from tastypie import fields
from tastypie.constants import ALL, ALL_WITH_RELATIONS
from OpenCS.api import CamelCaseJSONSerializer

from .models import CodeSnippet
from courseware.models import Course, Unit, Module

class SimpleCourseResource(ModelResource):
    glossary = fields.ToOneField('glossary.api.SimpleGlossaryResource', attribute='glossary', full=False)

    class Meta:
        resource_name = 'course'
        queryset = Course.objects.all()
        serializer = CamelCaseJSONSerializer(2)
        filtering = {
                'id': ALL,
                'slug': ALL,
                }

    def prepend_urls(self):
        return [
            url(r"^(?P<resource_name>%s)/(?P<slug>[\w\d-]+)/$" % self._meta.resource_name, self.wrap_view('dispatch_detail'), name="api_dispatch_detail"),
        ]

class SimpleUnitResource(ModelResource):
    course = fields.ToOneField(SimpleCourseResource, attribute='course', full=True)

    class Meta:
        resource_name = 'unit'
        queryset = Unit.objects.all()
        serializer = CamelCaseJSONSerializer(2)
        excludes = ['height', 'width']
        filtering = {
                'id': ALL,
                'position': ALL,
                'course': ALL_WITH_RELATIONS,
                }

class SimpleModuleResource(ModelResource):
    unit = fields.ToOneField(SimpleUnitResource, attribute='unit', full=True)

    class Meta:
        resource_name = 'module'
        queryset = Module.objects.all()
        serializer = CamelCaseJSONSerializer(2)
        filtering = {
                'id': ALL,
                'position': ALL,
                'unit': ALL_WITH_RELATIONS,
                }

class SnippetResource(ModelResource):
    module = fields.ToOneField(SimpleModuleResource, attribute='module', null=True, full=True)

    class Meta:
        resource_name = 'snippet'
        queryset = CodeSnippet.objects.all()
        serializer = CamelCaseJSONSerializer()
        filtering = {
                'id': ALL,
                'module': ALL_WITH_RELATIONS,
                }

