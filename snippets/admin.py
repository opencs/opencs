from django.contrib import admin

from .models import CodeSnippet

# Register your models here.
@admin.register(CodeSnippet)
class SnippetAdmin(admin.ModelAdmin):
    list_display = ('str', 'position', 'module')
    list_editable = ('position', )

    def course(self, obj):
        c = obj.unit.course
        return c.title
    def str(self, obj):
        return obj.name or 'untitled'
