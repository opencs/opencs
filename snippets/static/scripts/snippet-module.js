(function () {
    'use strict';

    var snippetModule = angular.module('snippetModule', []);
    
    function snippetService($http) {
        var service = {};

        var unitListPromise;

        /** Get a paritcular snippet
         *
         * Returns a promise (with the additional $http `success` and `error` methods).
         */
        service.getSnippet = function (snippetId) {
            return $http.get('/api/v1/snippet/' + snippetId + '/');
        };

        /** Get a list of snippets (with filters for unit/module
         */
        service.getSnippetList = function (courseId, unitId, moduleId) {
            var url = '/api/v1/snippet/';
            if (moduleId)
                url = url + '?module__id=' + moduleId;
            else if (unitId)
                url = url + '?module__unit__id=' + unitId;
            else if (courseId)
                url = url + '?module__unit__course__id=' + courseId;
            else
                url = url + '?limit=0';

            console.log(url, moduleId, unitId, courseId);

            var snippetPromise =  $http.get(url).then( function (response) {
                    return response.data.objects;
                }, function (response) {
                    console.warn('Recieved ' + response.status + ' response.', response);
                });
            return snippetPromise;

        };

        /** get the list of units in a course
         *
         * Arguments:
         *  course: either a number (courseId) or string (courseSlug)
         */
        service.getUnitList = function (course) {
            var url = '/api/v1/unit/';
            if (typeof course == 'string') 
                url += '?course__slug=' + course;
            else if (typeof course == 'number') 
                url += '?course__id=' + course;

            unitListPromise = $http.get(url).then(function (response) {
                return response.data.objects;
            }, function (response) {
                console.warn('Recieved ' + response.status + ' response.', response.data);
            });
            return unitListPromise;
        };

        /** get the list of modules in a unit
         *
         * Arguments:
         *  unitId: the actual id of the unit
         */
        service.getModuleList = function (unitId) {
            var url = '/api/v1/module/';
            if (unitId) 
                url = url + '?unit__id=' + unitId;

            var moduleListPromise = $http.get(url).then(function (response) {
                return response.data.objects;
            }, function (response) {
                console.warn('Recieved ' + response.status + ' response.', response.data);
            });
            return moduleListPromise;
        };
        
        /** Get the course corresponding to the slug
         *
         * Arguments:
         *  unitId: the actual id of the unit
         */
        service.getCourse = function (slug) {
            var url = '/api/v1/course/?slug=' + slug;

            var coursePromise = $http.get(url).then(function (response) {
                return response.data.objects[0];
            }, function (response) {
                console.warn('Recieved ' + response.status + ' response.', response.data);
            });
            return coursePromise;
        };

        return service;
    }
    snippetService.$inject = ['$http'];

    function snippetCtrl($scope, snippets) {
        $scope.mv = this;

        $scope.course = {title: 'Python from scratch'};
        $scope.unit = {};
        $scope.module = {};

        // XXX We default to Python From Scratch in this python panel
        var c = snippets.getCourse('python-from-scratch')
        c.then(function (course) {
            $scope.course = course;
            snippets.getUnitList(course.id).then( function (units) {
                $scope.unitList = [{title: 'All modules'}];
                $scope.unitList = $scope.unitList.concat(units);
            });
        });

        // Set up watches for changes in our 'filters'
        $scope.$watch('unit', function (newValue, oldValue) {
            snippets.getModuleList($scope.unit.id).then( function (modules) {
                $scope.moduleList = modules;
            });
            // XXX reset the module, because it is just for filtering.
            // setting $scope.module to a new object forces the snippetList update.
            $scope.module = {};
        });
        $scope.$watch('module', function (newValue, oldValue) {
                // XXX the commented version would exclude snippets unassociated with a module
                // snippets.getSnippetList($scope.course.id, $scope.unit.id, $scope.module.id)
                snippets.getSnippetList(undefined, $scope.unit.id, $scope.module.id)
                .then( function (snippets) {
                    $scope.snippetList = snippets;
                });
        });
    }
    snippetCtrl.$inject = ['$scope', 'snippetService'];

    function snippetDirective() {
        return {
            restrict: 'A',
            scope: {
                snippet: '='
            },
            controller: snippetCtrl,
            template: 'Filter by module <select ng-options="(item.position ? item.position +\'. \' : \'\' )+ item.title for item in unitList" ng-model="unit"></select>. Select code example:<select ng-options="item.module.unit.position + \'.\' + item.module.position +\' -  \'+ item.name for item in snippetList track by item.id" ng-model="snippet"></select>'
        };
    }


    snippetModule.factory('snippetService', snippetService);
    snippetModule.controller('snippetCtrl', snippetCtrl);
    snippetModule.directive('snippet', snippetDirective);

})();
