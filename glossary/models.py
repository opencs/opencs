from django.db import models

# Create your models here.
class Word(models.Model):
    """ An individual word/term

    The associated step and module numbers are saved, because they're available
    and might be needed in the future. These refer to the first appearance of the term.
    
    Attributes: 
        word (CharField):  The word or term for the glossary item
        definition (CharField): The definition of the term
        unit_number (PositiveSmallIntegerField): The unit number the word is first used in
        module_number (PositiveSmallIntegerField): The module number inside that unit
    """

    # Attributes
    word = models.CharField(max_length=200)  # TODO: more reasonable lengths?
    definition = models.CharField(max_length=200);

    # "Relations"
    unit_number = models.PositiveSmallIntegerField(default=0, verbose_name="Module Number")
    module_number = models.PositiveSmallIntegerField(default=0, verbose_name="Step Number")

    # Functions
    def copyFrom(self, w):
        """ Creates a copy of the word.
        """
        self.word = w.word
        self.definition = w.definition

    def is_same_as(self,w):
        """ Checks if a Word has the same term and definition as this one
        """
        return w.word == self.word and w.definition == self.definition

    def __str__(self):
        return self.word + " - " + self.definition

    class Meta:
        ordering = ['word']

class Glossary(models.Model):
    """ A collection of terms and definitions

    Attributes:
        word_set (ManyToManyField): The set of words in the dictionary
        unit_number (PositiveSmallIntegerField): The unit number this glossary is connected with
        title (CharField): The title of the glossary (displayed at the top of the page)
    """

    # Relations
    word_set = models.ManyToManyField(Word)

    # "Relations"
    unit_number = models.PositiveSmallIntegerField(default=0, verbose_name="Module Number")

    # Attributes
    title = models.CharField(max_length=200)

    # Functions
    def words(self):
        return self.word_set.order_by('word')

    def __str__(self):
        courses = self.course_set.all()
        if courses:
            return self.title + ' (' + ', '.join([ str(c) for c in courses ]) + ')'
        else:
            return self.title + ' (unassigned)'

    class Meta:
        ordering = ['unit_number']
        verbose_name_plural = "Glossaries"


# Now for basically the same thing, but for Built-Ins
class BuiltIn(models.Model):
    """ A description of a built-in function or constant

    The associated step and module numbers are saved, because they're available
    and might be needed in the future. These refer to the first appearance of the term.
    
    Attributes: 
        name (CharField):  The name/identifier of the builtin
        description (Text): The simple description
        explanation (Text): An explanation of the function
        language (Charfield): The language it is built into
        b_type (Charfield): The type of the built-in (e.g. element, function, property)

        module (Module): The module it first appears in
        example_set (BuiltInExamples): The set of examples for this builtin

    """

    # Attributes
    name = models.CharField(max_length=50)
    description = models.TextField(blank=True)
    explanation = models.TextField(blank=True)

    PYTHON = 'Python'
    HTML = 'HTML'
    CSS = 'CSS'
    JS = 'JS'
    LANGUAGE_CHOICES = (
            (PYTHON, 'Python'),
            (HTML, 'HTML'),
            (CSS, 'css'),
            (JS, 'javascript'),
            )
    language = models.CharField(max_length=6, choices=LANGUAGE_CHOICES, default=PYTHON)
    b_type = models.CharField(max_length=50, blank=True, verbose_name='Type')

    # Relations
    module = models.ForeignKey('courseware.Module', blank=True, null=True, on_delete=models.SET_NULL)

    # Functions
    def course(self):
        if self.module:
            return self.module.unit.course
        else:
            return None

    def unit_number(self):
        if self.module:
            return self.module.unit.position
        else:
            return None

    def module_number(self):
        if self.module:
            return self.module.position
        else:
            return None

    def example(self):
        exs = self.example_set.all()
        if exs:
            return exs[0].text
        else:
            return None

    def __str__(self):
        return self.name + ' ' + self.language

    class Meta:
        ordering = ['language', 'name']

class BuiltInExample(models.Model):
    """ An example for a built-in function

    Attributes:
        text (TextField): The actual example (in html)
        builtin (BuiltIn): The builtin function it is an example of
    """

    # Attributes
    text = models.TextField()

    # Relations
    builtin = models.ForeignKey(BuiltIn, related_name='example_set')

    # Functions
    def __str__(self):
        return self.builtin.name + '.example'

class BuiltInCollection(models.Model):
    """ A collection of builtin functions

    Attributes:
        builtin_set (ManyToManyField): The set of builtins in the collection
        title (CharField): The title of the collection (displayed at the top of the page)
    """

    # Relations
    builtin_set = models.ManyToManyField(BuiltIn, through='BuiltInRelation', through_fields=('collection', 'builtin'))

    # Attributes
    title = models.CharField(max_length=200)

    # Functions
    def builtins(self):
        """Retrieves the builtin_set in the correct order
        """
        return self.builtin_set.order_by('relation_set__order')

    def __str__(self):
        courses = self.course_set.all()
        if courses:
            return self.title + ' (' + ', '.join([ str(c) for c in courses ]) + ')'
        else:
            return self.title + ' (unassigned)'


    class Meta:
        verbose_name = "Built-in Collection"

class BuiltInRelation(models.Model):
    collection = models.ForeignKey(BuiltInCollection, related_name='relation_set')
    builtin = models.ForeignKey(BuiltIn, related_name='relation_set')
    order = models.PositiveSmallIntegerField(default=0)

