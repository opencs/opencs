from django.contrib import admin
from glossary.models import Word, Glossary, BuiltIn, BuiltInExample, BuiltInCollection

class WordAdmin(admin.ModelAdmin):
    list_display = (
            'word',
            'glossaries',
            'definition',
            'unit_number',
            'module_number',
            )
    list_filter = (
            'glossary__course__title',
            )

    def glossaries(self, obj):
        return ', '.join([ str(g) for g in obj.glossary_set.all()]) or 'unassigned'
    glossaries.admin_order_field = 'glossary__course__title'

class WordInline(admin.TabularInline):
    model = Glossary.word_set.through
    extra = 1;
    
class GlossaryAdmin(admin.ModelAdmin):
    list_display = ('title', 'unit_number','word_count')
    fieldsets = [
        ('Glossary Information', {'fields': ['title','unit_number']}),       
        ]
    inlines = [
        WordInline,
        ]
    exclude = ('word_set',)

    def word_count(self, obj):
        return obj.word_set.count()

class BuiltinInline(admin.TabularInline):
    model = BuiltInCollection.builtin_set.through

class ExampleInline(admin.TabularInline):
    model = BuiltInExample
    
class BuiltinAdmin(admin.ModelAdmin):
    list_display = ('name', 'description', 'module', 'collections')
    list_filter = (
            'builtincollection__course__title',
            )
    inlines = [
        ExampleInline,
        ]
    exclude = ('example_set', )
    ordering = ['builtincollection__course__title','relation_set__order']

    def collections(self, obj):
        return ', '.join([ str(c) for c in obj.builtincollection_set.all()]) or 'unassigned'
    collections.admin_order_field = 'builtincollection__course__title'

class BuiltinCollectionAdmin(admin.ModelAdmin):
    list_display = ('title', 'builtin_count')
    readonly_fields = ('courses',)
    fieldsets = [
        ('Collection Information', {'fields': ['title', 'courses']}),
        ]
    inlines = [
            BuiltinInline
        ]

    def courses(self, obj):
        return '; '.join([ str(x) for x in obj.course_set.all() ])

    def builtin_count(self, obj):
        return obj.builtin_set.count()



admin.site.register(Word, WordAdmin)
admin.site.register(Glossary, GlossaryAdmin)
admin.site.register(BuiltInCollection, BuiltinCollectionAdmin)
admin.site.register(BuiltIn, BuiltinAdmin)
