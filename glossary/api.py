# REST-api file for glossary

from tastypie.resources import ModelResource
from tastypie import fields
from tastypie.constants import ALL, ALL_WITH_RELATIONS
from django.conf.urls import url

from OpenCS.api import CamelCaseJSONSerializer
from glossary.models import Glossary, Word

from snippets.api import SimpleCourseResource

def register_to_api(api):
    """ Helper function to register these resources with an Api instance"""
    resources = [GlossaryResource, 
                 WordResource, 
                 ]
    for resource in resources:
        api.register(resource())

class SimpleGlossaryResource(ModelResource):
    course = fields.ToManyField(SimpleCourseResource, attribute='course_set', full=False)
    words = fields.ToManyField('glossary.api.WordResource', 'word_set', full=False)
    class Meta:
        queryset = Glossary.objects.all()
        resource_name = 'sg'
        serializer = CamelCaseJSONSerializer()
        filtering = {
                'id': ALL,
                'course': ALL_WITH_RELATIONS,
                }

class WordResource(ModelResource):
    glossary = fields.ToManyField('glossary.api.GlossaryResource', 'glossary', full=False, null=True)

    def build_filters(self, filters=None):
        if filters is None:
            filters = {}

        orm_filters = super(WordResource, self).build_filters(filters)
        print(orm_filters)

        if "course" in filters:
            print(filters['course'])
            orm_filters['glossary__course__slug'] = filters['course']

        return orm_filters


    class Meta:
        queryset = Word.objects.order_by('-unit_number')
        detail_uri_name = 'word'
        serializer = CamelCaseJSONSerializer(2)
        filtering = {
                'id': ALL,
                'word': ALL,
                'glossary': ALL_WITH_RELATIONS,
                'unit_number': ALL,
                }


class GlossaryResource(ModelResource):
    # Setting full=True and full_list=False gives all the word detail in a detail request,
    # but only resource URI's for the list request
    # TODO: Maybe set use_in=detail if there's a performance problem with the list request
    course = fields.ToManyField(SimpleCourseResource, attribute='course_set', full=False)
    words = fields.ToManyField(WordResource, 'word_set', full=True, full_list=False)

    class Meta:
        queryset = Glossary.objects.all()
        resource_name = 'glossary'
        serializer = CamelCaseJSONSerializer()
        filtering = {
                'id': ALL,
                'course': ALL_WITH_RELATIONS,
                }
