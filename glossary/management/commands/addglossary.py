from django.core.management.base import BaseCommand, CommandError
from django.db import transaction
from glossary.models import Glossary, Word

import re

class Command(BaseCommand):
    args = '<glossary-file> <title>'
    help = 'Adds a glossary from the LaTeX file into the database'

    def handle(self, *args, **options):
        try:
            filename = args[0]
            title = args[1]
        except IndexError:
            raise CommandError('Wrong number of arguments')

        with transaction.atomic():
            if Glossary.objects.filter(title=title):
                choice = input('A glossary with title "%s" already exists. [R]eplace, [C]ombine, [N]ew, or [A]bort? ' % title)
                if choice.lower() == 'n':
                    g = Glossary(title = title)
                    g.save()
                    g.word_set = make_word_set(filename)
                elif choice.lower() == 'r':
                    g = Glossary.objects.get(title=title)
                    g.word_set = make_word_set(filename)
                elif choice.lower() == 'c':
                    g = Glossary.objects.get(title=title)
                    g.word_set.add(*make_word_set(filename))
                else:
                    raise CommandError('Glossary creation aborted')
            else:
                g = Glossary(title = title)
                g.save()
                g.word_set = make_word_set(filename)

        print_glossary(g)

def make_word_set(filename):
    prog = re.compile(r'\\item\s+(?P<word>.*?)\s+-+\s+(?P<definition>.*)$')
    try:
        f = open(filename, 'r')
    except IOError:
        raise CommandError('File "%s" does not exist' % filename)

    word_set = []
    created_words = []
    for line in f:
        m = prog.search(line)
        if m:
            w = Word(**m.groupdict())
            same_words = Word.objects.filter(word=m.groupdict()['word'])
            if same_words:
                print('A word already exists\n\nNew Word/Definition: %s\nExisting Words:' %w)
                for counter, old in enumerate(same_words,start=1):
                    print('{:>3} {!s}'.format(counter, old))
                choice = input('[u]se existing, [r]eplace one, [a]dd the new definition? ')
                if choice.lower() == 'a':
                    w.save()
                else:
                    if len(same_words) == 1:
                        n=1
                    else:
                        n = int(input('Which one?'))
                    if choice.lower() == 'u':
                        w = same_words[n-1]
                    elif choice.lower() == 'r':
                        ow = same_words[n-1]
                        ow.copyFrom(w)
                        ow.save()
                        w = ow
                    else:
                        raise CommandError('Invalid Operation')
            else:
                w.save()

            word_set.append(w)

    return word_set

def print_glossary(g):
        try:
            loaded = Glossary.objects.get(pk=g.pk)
        except Glossary.DoesNotExist:
            raise CommandError("Glossary did not save properly")
        loaded_words = g.word_set.all()
        print('Successfully created the glossary:\n %s\n %s' % (loaded, loaded_words))
