# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('glossary', '0004_auto_20150818_2158'),
    ]

    operations = [
        migrations.AddField(
            model_name='builtin',
            name='b_type',
            field=models.CharField(blank=True, max_length=50),
        ),
        migrations.AlterField(
            model_name='builtin',
            name='language',
            field=models.CharField(choices=[('Python', 'Python'), ('HTML', 'HTML'), ('CSS', 'css'), ('JS', 'javascript')], default='Python', max_length=6),
        ),
    ]
