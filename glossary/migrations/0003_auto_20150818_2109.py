# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('courseware', '0013_auto_20150817_1743'),
        ('glossary', '0002_auto_20150410_1448'),
    ]

    operations = [
        migrations.CreateModel(
            name='BuiltIn',
            fields=[
                ('id', models.AutoField(serialize=False, primary_key=True, verbose_name='ID', auto_created=True)),
                ('name', models.CharField(max_length=50)),
                ('description', models.TextField(blank=True)),
                ('explanation', models.TextField(blank=True)),
                ('language', models.CharField(max_length=6, default='python', choices=[('python', 'Python'), ('html', 'HTML'), ('css', 'css'), ('js', 'javascript')])),
                ('module', models.ForeignKey(on_delete=django.db.models.deletion.SET_NULL, null=True, blank=True, to='courseware.Module')),
            ],
            options={
                'ordering': ['language', 'name'],
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='BuiltInCollection',
            fields=[
                ('id', models.AutoField(serialize=False, primary_key=True, verbose_name='ID', auto_created=True)),
                ('title', models.CharField(max_length=200)),
            ],
            options={
                'verbose_name': 'Built-in Collection',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='BuiltInExample',
            fields=[
                ('id', models.AutoField(serialize=False, primary_key=True, verbose_name='ID', auto_created=True)),
                ('text', models.TextField()),
                ('builtin', models.ForeignKey(to='glossary.BuiltIn', related_name='example_set')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='BuiltInRelation',
            fields=[
                ('id', models.AutoField(serialize=False, primary_key=True, verbose_name='ID', auto_created=True)),
                ('order', models.PositiveSmallIntegerField(default=0)),
                ('builtin', models.ForeignKey(to='glossary.BuiltIn')),
                ('collection', models.ForeignKey(to='glossary.BuiltInCollection')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.AddField(
            model_name='builtincollection',
            name='builtin_set',
            field=models.ManyToManyField(through='glossary.BuiltInRelation', to='glossary.BuiltIn'),
            preserve_default=True,
        ),
    ]
