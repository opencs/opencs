# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('glossary', '0003_auto_20150818_2109'),
    ]

    operations = [
        migrations.AlterField(
            model_name='builtinrelation',
            name='builtin',
            field=models.ForeignKey(to='glossary.BuiltIn', related_name='relation_set'),
        ),
        migrations.AlterField(
            model_name='builtinrelation',
            name='collection',
            field=models.ForeignKey(to='glossary.BuiltInCollection', related_name='relation_set'),
        ),
    ]
