# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('glossary', '0005_auto_20150821_1615'),
    ]

    operations = [
        migrations.AlterField(
            model_name='builtin',
            name='b_type',
            field=models.CharField(blank=True, verbose_name='Type', max_length=50),
        ),
    ]
