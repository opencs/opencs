# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('glossary', '0001_initial'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='glossary',
            options={'ordering': ['unit_number'], 'verbose_name_plural': 'Glossaries'},
        ),
        migrations.AlterModelOptions(
            name='word',
            options={'ordering': ['word']},
        ),
        migrations.AddField(
            model_name='glossary',
            name='unit_number',
            field=models.PositiveSmallIntegerField(verbose_name='Module Number', default=0),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='word',
            name='module_number',
            field=models.PositiveSmallIntegerField(verbose_name='Step Number', default=0),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='word',
            name='unit_number',
            field=models.PositiveSmallIntegerField(verbose_name='Module Number', default=0),
            preserve_default=True,
        ),
    ]
