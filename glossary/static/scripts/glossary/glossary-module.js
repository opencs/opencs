(function () {

    var glossaryModule = angular.module('glossaryModule', ['ngResource']);

    glossaryModule.directive('glossary', glossaryDirective);
    glossaryModule.factory('glossaryTermService', termFactory);
    glossaryModule.factory('glossaryService', glossaryFactory);
    glossaryModule.directive('bindHtmlCompile', bindCompile);


    /** Term Service
     *
     * an angular service which provides access to glossary terms
     */
    function termFactory($resource) {
        return $resource('/api/v1/word/?word=:term', 
                         {term: '@term' }, 
                         {
                             id_get: {
                                 method: 'GET', 
                                 cache: true, 
                                 url: '/api/v1/word/?id=:id',
                                 transformResponse: function (data) {
                                     var result = JSON.parse(data).objects[0];
                                     return result;
                                 },
                             },
                             get: {
                                 method: 'GET', 
                                 cache: true, 
                                 url: '/api/v1/word/?word__iexact=:term&unit_number__lte=:unit',
                                 params: { unit: 100 },
                                 transformResponse: function (data) {
                                     var result = JSON.parse(data).objects[0];
                                     return result;
                                 },
                             },
                             old_get: {
                                 method: 'GET', 
                                 cache: true, 
                                 transformResponse: function (data) {
                                     var result = JSON.parse(data).objects[0];
                                     return result;
                                 },
                             },
                         });
    }
    termFactory.$inject = ['$resource'];

    /** Glossary Service
     *
     * an angular service which provides access to an entire glossary
     */
    function glossaryFactory($resource) {
        return $resource('/api/v1/glossary/:id', {id: '@id' });
    }
    glossaryFactory.$inject = ['$resource'];


    /** Glossary directive
     *
     * Usage:
     *      <span glossary="idList">display text</span>
     *
     *  where `idList` is a list of id's for terms in the site's glossary database.
     */
    function glossaryDirective($compile, $parse, terms) {
        return {
            restrict: 'A',
            scope: {},
            controller: ['$scope', 'glossaryTermService', function($scope, terms) {
            }],
            link: function (scope, iElement, iattrs) {
                // XXX This first clause is only for backwards compatibility with the old glossary lookup method
                if (iattrs.glossary[0] === '[') {
                    scope.term = terms.id_get({id: $parse(iattrs.glossary)()[0] || 0});
                }
                else if (iattrs.glossary)
                    var params = {term: iattrs.glossary,
                                  };
                    if (typeof courseSlug !== 'undefined')
                        params.course = courseSlug;
                    if (typeof unitPosition !== 'undefined')
                        params.unit = unitPosition;
                    scope.term = terms.get({term:iattrs.glossary, course:courseSlug } );
                iElement.append($compile('<span class="popup" ng-hide="!term.definition"><span class="inner">{{term.definition}}</span></span>')(scope));
            },
        };
    }
    glossaryDirective.$inject = ['$compile', '$parse', 'glossaryTermService'];

    /** Bind and compile html directive
     *
     * This directive is needed when we attach some html to an element (typically a slide).
     * Without it, the glossary spans won't function.
     *
     * XXX It may make sense to only compile the glossary children.
     */
    function bindCompile ($compile) {
        return {
            restrict: 'A',
            link: function (scope, element, attrs) {
                scope.$watch(function () {
                    return scope.$eval(attrs.bindHtmlCompile);
                }, function (value) {
                    element.html(value);
                    $compile(element.contents())(scope);
                });
            }
        };
    }
    bindCompile.$inject = ['$compile']

})();
