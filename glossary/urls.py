from django.conf.urls import patterns, url

from glossary import views

urlpatterns = patterns('', 
        url(r'^$', views.index, name='global'),
        url(r'^(?P<unit_number>\d+)/$', views.glossary, name='glossary'),
        url(r'^word/(?P<word_id>\d+)/$', views.word, name='word_detail'),
        )
