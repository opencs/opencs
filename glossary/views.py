from django.http import HttpResponse
from django.shortcuts import render, get_object_or_404, get_list_or_404

from glossary.models import Word, Glossary

def index(request):
    word_list = Word.objects.order_by('word')
    context = {'glossary': {'title': 'Glossary'}, 'word_list': word_list}
    return render(request, 'glossary/global.html', context)

def glossary(request, unit_number):
    glossaries = get_list_or_404(Glossary, unit_number=unit_number)
    return render(request, 'glossary/glossary.html', {'glossary_list': glossaries, 'unit': glossaries[0].title })

def word(request, word_id):
    w = get_object_or_404(Word, id=word_id)
    return render(request, 'glossary/word_details.html', {'word': w})
