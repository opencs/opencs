from django.conf import settings
from django.core.urlresolvers import resolve
from django.contrib.flatpages.models import FlatPage
from django.http import Http404

from courseware.models import Course

def pysock_context(request):
    pysock = request.get_host().split(':')[0] + settings.PYSOCK_SUFFIX
    if request.scheme == 'https':
        pysock = "wss://" + pysock
    else:
        pysock = "ws://" + pysock

    return {'python_socket':pysock}
    
def analytics_context(request):
    if settings.ANALYTICS:
        return {'analytics': settings.ANALYTICS}
    else:
        return {'analytics': False}

def url_context(request):
    url = request.path
    split_url = [ part for part in url.split('/') if part ]
    links = []
    acc = '/'
    for i in range(0, len(split_url)):
        acc = acc + split_url[i] + '/'
        # Flatpages need to be checked without 'resolve'
        fp = FlatPage.objects.filter(url=acc)
        if fp:
            split_url[i] = fp[0].title
            links.append(acc)
        else:
            # check for another url handler
            try:
                view, args, kwargs = resolve(acc)
                # XXX The next two lines would cause infinite loops
                # kwargs['request'] = request
                # view(*args, **kwargs)
                links.append(acc)
            except Http404:
                links.append("#")
    course = None;
    if split_url:
        course = Course.objects.filter(slug=split_url[0])
        if course:
            course = course[0]
            split_url[0] = course.title
        else:
            course = None
    return {
            'url': url,
            'crumbs': zip(split_url, links),
            'course': course or None,
            }
