"""
Common tools for defining the format of TastyPie requests.
"""

import json
import re
from django.core.serializers.json import DjangoJSONEncoder
from tastypie.serializers import Serializer

class CamelCaseJSONSerializer(Serializer):
    """
    A Serializer that converts underscores to camelCase
    """
    json_indent = None
    formats = ['json']
    content_types = {
            'json': 'application/json',
            }
    
    def __init__(self, indent=None, *args, **kwargs):
        """
        Attributes:
            indent (int): Number of spaces to indent output (default ``None``)
        """
        super().__init__(*args, **kwargs)
        self.json_indent = indent

    def to_json(self, data, options=None):
        data = self.to_simple(data, options)

        def underscoreToCamel(match):
            return match.group()[0] + match.group()[2].upper()

        def camelize(data):
            if isinstance(data, dict):
                new_dict = {}
                for key, value in data.items():
                    new_key = re.sub(r"[a-z]_[a-z]", underscoreToCamel, key)
                    new_dict[new_key] = camelize(value)
                return new_dict
            if isinstance(data, (list, tuple)):
                for i in range(len(data)):
                    data[i] = camelize(data[i])
                return data
            return data

        camelized_data = camelize(data)

        return json.dumps(camelized_data, sort_keys=True, indent=self.json_indent)
