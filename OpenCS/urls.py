from django.conf.urls import patterns, include, url
from django.contrib import admin

from OpenCS import views

from tastypie.api import Api
from glossary.api import register_to_api as register_glossary_api
from quiz.api import register_to_api as register_quiz_api
from exploration.api import register_to_api as register_exp_api
from slides.api import register_to_api as register_slides_api
from courseware.api import register_to_api as register_courseware_api
from snippets.api import SnippetResource

# Create the REST API
v1_api = Api(api_name="v1")
register_glossary_api(v1_api)
register_quiz_api(v1_api)
register_exp_api(v1_api)
register_slides_api(v1_api)
register_courseware_api(v1_api)
v1_api.register(SnippetResource())

# Change the admin site header and title
# TODO: decide on making a custom admin.site for this
admin.site.site_header = "OpenCS Administration"
admin.site.site_header = "OpenCS Site Admin"


urlpatterns = patterns('',
    # For the Final Site
    url(r'^api/', include(v1_api.urls)),
    url(r'^web-container/', include('webhost.urls'), name='webcontainer'),
    url(r'^admin/', include(admin.site.urls)),
    url(r'^python-from-scratch/python-panel/', views.pypanel, name='pypanel'),
    url(r'^glossary/', include('glossary.urls', namespace='glossary')),
)

# XXX The flatpages have priority over courseware
urlpatterns += patterns('django.contrib.flatpages.views',
    url(r'^/', include('django.contrib.flatpages.urls')),
)

urlpatterns += patterns('courseware.views',
    url(r'', include('courseware.urls', namespace="courseware")),
)

# For the Dev-mode media serving
from django.conf import settings
from django.conf.urls.static import static
if settings.DEVMODE:
    urlpatterns = urlpatterns + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
