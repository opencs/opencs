"""
``import_builtins`` command
------------------------------

This Django management command to add a builtin collection to the database.

usage
^^^^^
::

    python manage.py import_builtins <glossary-file> [<course-slug>]

The builtins file can be located anywhere, because it is not referenced after import. The file must be a ``.tsv`` (tab-separated value), with columns in the order:

* ID
* Module number
* step number
* Name
* Description
* Explanation
* Example
* Language (one of 'Python', 'HTML', 'CSS', 'JS')
* Type (the type of built-in, e.g. element, function, property)

All of name, description, explanation, and example can be markdown-formatted.

Behaviour
^^^^^^^^^

A new built-ins collection with the title "Built-ins" is created. Then a new builtin object is created for each line, and added to the collection.

If the course-slug is included, then each built-in will be linked to the module in that course. Without the course-slug, module and step number information will be lost.

To assign the collection to a particular course, the admin interface can be used. Go to the admin page for the course, and select the collection from the selector.

.. note:: no existing builtins or collections are removed or altered in this process.

"""
from django.core.management.base import BaseCommand, CommandError
from django.db import transaction

from glossary.models import BuiltIn, BuiltInCollection, BuiltInRelation
from courseware.models import Module

import OpenCS.markdown as md
import re

from os import path

p_paragraph = re.compile(r'</?p>')

class Command(BaseCommand):

    # TODO Update to add_arguments style 
    # https://docs.djangoproject.com/en/1.8/howto/custom-management-commands/#accepting-optional-arguments
    args = '<glossary file> [<course slug>]'
    help = 'Import built-ins from a tab-separated value file\n  This does not replace any existing content'

    def handle(self, *args, **kwargs):
        if len(args) == 1:
            filename = args[0]
            course_slug = None
        elif len(args) == 2:
            filename = args[0]
            course_slug = args[1]
        else:
            raise CommandError('Wrong number of arguments')

        print('\nReading Collection' )
        with open(filename) as f:
            contents = f.read()
        
        lines = contents.splitlines()[1:]

        with transaction.atomic():
            collection = BuiltInCollection()
            collection.title = "Built-ins"
            collection.save()
            for line in lines:
                parts = line.split('\t')
                # Create the builtin
                builtin = BuiltIn()
                order = parts[0]
                un = parts[1]
                mn = parts[2]
                if course_slug:
                    try:
                        builtin.module = Module.objects.get(unit__course__slug=course_slug,
                                                        unit__position=un, 
                                                        position=mn)
                    except:
                        print('Warning, unit %s, module %s does not exist' % (un, mn))
                builtin.name = parts[3]
                builtin.description = md.markdown(parts[4])
                builtin.explanation = md.markdown(parts[5])
                builtin.language = parts[7]
                builtin.b_type = parts[8]
                builtin.save()

                # Attach the example to the builtin
                builtin.example_set.create(text = md.markdown(parts[6]))

                # Assign the built-in to the collection
                BuiltInRelation.objects.create(collection=collection,
                                               builtin=builtin,
                                               order=parts[0])
