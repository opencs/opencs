"""
``update_pages`` command
------------------------

This Django management command updates the flatpages in the database, replacing the contents at the corresponding url.

usage
^^^^^
::

    python manage.py update_pages <pages-file>

The pages file can be located anywhere, because it is not referenced after import. The locations (urls) of the pages are determined by the files contents.

Behaviour
^^^^^^^^^

The pages file is first split into its individual page texts. 

.. note:: This split is done by matching on the regex ``<!--+\s*(.+)[\s]+-+\s*(/.*/)\s*--+>``

Then for each page text, the following is performed:

1. The metadata is pulled from the first line (e.g. "<!-- title - url -->"). Only one ``'-'`` character can be used as a separator.
2. The flatpage object is looked up by url, and created if not found.
3. The title and contents are set. Contents are passed through a markdown -> html conversion.
"""
from django.core.management.base import BaseCommand, CommandError
from django.db import transaction
from django.contrib.flatpages.models import FlatPage
from django.contrib.sites.models import Site

import re

# For markdown support
import OpenCS.markdown as md

class Command(BaseCommand):
    # TODO Update to add_arguments style 
    # https://docs.djangoproject.com/en/1.8/howto/custom-management-commands/#accepting-optional-arguments
    args = '<flatpages-file>'
    help = 'Adds/replaces all the flatpages defined in the file'

    def handle(self, *args, **kwargs):
        try:
            filename = args[0]
        except IndexError:
            raise CommandError('Wrong number of arguments')

        p_header = re.compile(r'<!--+\s*(.+)[\s]+-+\s*(/.*/)\s*--+>')

        # Pages will be a list in the form [text, title, url, ...]
        with open(filename, 'r') as f:
            pages = p_header.split( f.read() )

        sites = Site.objects.all()   # needed because the flatpage must be assigned to a site
        for i in range(1, len(pages),3):
            title = pages[i].strip()
            url = pages[i+1].strip()
            page = pages[i+2]
            print("importing %s, %s" %(title, url))

            # If the url is already there, replace it with the new values
            fp = FlatPage.objects.filter(url=url)
            if fp:
                fp = fp[0]
            else:
                fp = FlatPage()

            fp.url = url
            fp.title = title
            fp.content = md.markdown(page)

            fp.save()
            fp.sites = sites
            fp.save()
