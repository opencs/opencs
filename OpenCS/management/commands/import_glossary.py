"""
``import_glossary`` command
------------------------------

This Django management command add glossary terms to the database.

usage
^^^^^
::

    python manage.py import_glossary <glossary-file>

The glossary file can be located anywhere, because it is not referenced after import. The file must be a ``.tsv`` (tab-separated value), with columns in the order:

* Module number
* step number
* *unused*
* *unused*
* term
* defnintion (markdown supported)

Behaviour
^^^^^^^^^

A new glossary with the title "Glossary" is created, as well as one for each module. Then a new term is created for each line, and added to the "Glossary" and module glossaries.

To assign the glossary to a particular course, the admin interface can be used. Go to the admin page for the course, and select the glossary from the selector. *The glossary title should also be changed at this point.*

.. note:: no terms, definitions, or old glossaries are removed or overwritten

"""
from django.core.management.base import BaseCommand, CommandError
from django.db import transaction

from glossary.models import Word, Glossary
from courseware.models import Course, Unit

import OpenCS.markdown as md
import re

from os import path

p_paragraph = re.compile(r'</?p>')

class Command(BaseCommand):
    # TODO Update to add_arguments style 
    # https://docs.djangoproject.com/en/1.8/howto/custom-management-commands/#accepting-optional-arguments
    args = '<glossary file>'
    help = 'Import glossary terms from a tab-separated value file\n  This does not replace any existing content'

    def handle(self, *args, **kwargs):
        try:
            filename = args[0]
        except IndexError:
            raise CommandError('Wrong number of arguments')
        
        # Determine module and unit numbers from filename
        print('\nReading Glossary' )

        with open(filename) as f:
            contents = f.read()
        
        lines = contents.splitlines()[1:]

        # All new content goes into "staging"
        course = Course.objects.get(title='Staging')

        global_gloss = Glossary()
        global_gloss.title = "Glossary"
        global_gloss.un = 0
        global_gloss.save()
        old_un = 0
        with transaction.atomic():
            for line in lines:
                parts = line.split('\t')
                un = int(parts[0])
                if old_un != un:
                    old_un=un
                    # And start a new glossary
                    glossary = Glossary()
                    unit = course.unit_set.filter(position=un)
                    if unit:
                        glossary.title = unit[0].title
                    else:
                        glossary.title = 'module %d' % un
                    glossary.unit_number = un
                    glossary.save()
                word = Word()
                word.unit_number = parts[0]
                word.module_number = parts[1]
                word.word = parts[4]
                word.definition = p_paragraph.sub("", md.markdown(parts[5]))
                word.save()
                glossary.word_set.add(word)
                global_gloss.word_set.add(word)
