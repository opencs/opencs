"""
``import_slides`` command
------------------------------

This Django management command adds a slideshow to the database, assigning it to the proper module and step.

usage
^^^^^
::

    python manage.py import_slides <slides-file>

The slides file can be located anywhere, because it is not referenced after import.

Metadata (module and step number) for Markdown-based slideshows is determined by the preamble of the file, so there is no restriction on the filename.

Behaviour
^^^^^^^^^

First, the module and step are looked up in the *staging-web* course. If the pre-existing step is not an slideshow, an error is raised. If either is missing, it is created, and the new module's title will be "untitled". 

The ``title`` and ``description`` fields of the module are set to the values in the pre-amble with default: "Step N".

Next, a new ``slideshow`` object is created, and the contents of the file are used to populate it with new slides.

Any images in the slideshow are copied into the ``MEDIA_ROOT`` under a unique directory for the slide. This means that if an image is used in more than one slide, it will be stored once for each slide. However, we also avoid name collisions, and make it possible to delete all files associated with an object without affecting others.

After the new slideshow is created, it is assigned to the step, and the pre-existing slideshow is deleted (if there is one).

.. note:: The images associated with the old slideshow and slides are not removed.

.. note:: The entire process is performed in an atomic transaction. Therefore, if any part of the process fails by raising an exception, then no changes will be commited to the database.
"""
from django.core.management.base import BaseCommand, CommandError
from django.db import transaction

import OpenCS.latex2markdown as l2m
import OpenCS.markdown as md
import re

from django.contrib.contenttypes.models import ContentType
from slides.models import SlideShow, Slide, SlideImage
from courseware.models import Course, Unit, Module

from django.conf import settings
import shutil
from os import path
import os

class Command(BaseCommand):
    # TODO Update to add_arguments style 
    # https://docs.djangoproject.com/en/1.8/howto/custom-management-commands/#accepting-optional-arguments
    args = '<slides file>'
    help = 'Import the slides from the file'


    p_title = re.compile(r'^[Tt]itle[:\-\s]+(.*)', re.MULTILINE)

    def handle(self, *args, **kwargs):
        try:
            filename = args[0]
            if not(filename.startswith('/')):
                filename = path.join(os.getcwd(), args[0])
            self.filepath = path.dirname(filename)
        except IndexError:
            raise CommandError('Wrong number of arguments')
        
        # Determine module and unit numbers from filename
        pattern = re.compile(r'mod(\d\d)step(\d\d)')
        m = pattern.match(path.split(filename)[1])
        if m:
            un_tmp = int(m.groups()[0])
            mn_tmp = int(m.groups()[1])
        else:
            un_tmp = 0;
            mn_tmp = 0;
            print("Warning: non-standard slideshow name")

        with open(filename) as f:
            contents = f.read()

        header = contents.strip().splitlines(1)[0]

        p_header = re.compile(r'<!-*\s*[Mm]odule\s*(\d+)[,\-\s]+[Ss]tep\s*(\d+)[,\-\s]+([a-zA-Z]*)[\s\-]+>')
        m = p_header.match(header)
        if m:
            un = int(m.groups()[0])
            mn = int(m.groups()[1])
            mod_type = m.groups()[2]
        else:
            raise CommandError('Invalid step header: %s' % header)

        if un_tmp!=un or mn_tmp != mn:
            print('Warning: Module and step numbers in header do not match filename')
        if mod_type not in ['text', 'slides']:
            print('Warning: Expected "text" or "slides" module type. Found "%s"' % mod_type)
        print('\nSlideshow, unit %d, module %d' %(un, mn))

        try: 
            preamble, main_text = contents.split('<!-- BEGIN -->');
            slide_texts = main_text.split('<!-- Continue -->')
        except:
            raise CommandError('Malformed module, likely missing "<!-- Begin -->"')

        # Look for the title clause in preamble
        ms = self.p_title.split(preamble)
        if len(ms)>1:
            title = ms[1].strip()
            print('title: %s' % title)
            description = ms[2].strip()
        else:
            title = 'Step %d' % mn
            description = mod_type

        # All new content goes into "staging-web"
        course = Course.objects.get(slug='staging-web')
        slideshow_type = ContentType.objects.get_for_model(SlideShow)

        # The rest is atomic, because we start creating objects if they don't exist
        with transaction.atomic():
            # Find the unit, and create it if it doesn't exist
            unit = course.unit_set.filter(position=un)
            if unit:
                unit = unit[0]
            else:
                print("creating unit")
                unit = Unit()
                unit.position = un
                unit.title = 'untitled'
                unit.course = course
                unit.save()
            
            # Find the module, and create it if it doesn't exist
            module = unit.module_set.filter(position=mn)

            if module:
                module = module[0]
                module.title = title
                module.description = description
                if not(module.content_type == slideshow_type):
                    raise CommandError("Pre-existing module is not a slideshow. Unit: %d, Module %d" % (un, mn) )
            else:
                print("creating module")
                module = Module()
                module.content_type = slideshow_type
                module.unit = unit
                module.position = mn
                module.title = title
                module.description = description

            # Now create the slideshow, and attach it to the module
            ss = SlideShow()
            ss.title = title
            ss.save()

            index = 1
            print('slide: ', end='')
            for st in slide_texts:
                if st:
                    print(index, end=' ')
                    slide = self.parse_slide(st)
                    slide.slideshow = ss
                    slide.index = index
                    slide.save()
                    index = index + 1

            old_ss = module.content_object
            module.content_object = ss
            module.save()
            print('saved')
            if old_ss:
                old_ss.delete()
            
    p_image = re.compile(r'<img\s+alt="([^"]*)"\s+src="([^"]*)')

    def parse_slide(self, text):
        s = Slide()

        # Save the slide so that images can be linked to it
        html = md.markdown(text)
        s.text = html
        s.save()

        # This will be a list in the form [text, image_title, image_file, ...]
        image_components = self.p_image.split(html)
        for i in range(1, len(image_components), 3):
            img = SlideImage()
            img.slide = s
            img.title = image_components[i]
            image_file = image_components[i+1]
            orig_file = path.join(self.filepath, image_file)
            image_file = path.join('images/slides/%s/' % (s.id), path.basename(image_file))
            stored_file = path.join(settings.MEDIA_ROOT,  image_file)
            os.makedirs(path.dirname(stored_file), mode=0o775, exist_ok=True)
            try:
                shutil.copyfile(orig_file, stored_file)
                img.image = image_file
                img.save()
            except FileNotFoundError:
                print('\n\tWarning: image file %s is missing\n' % image_components[i+1])

        # The slide's save function finds all <img> tags corresponding to its image_set and updates them
        s.save()

        return s
