"""
``link_glossary`` command
------------------------------

This Django management command fixes glossary popups which are missing a value (by using the display term)

usage
^^^^^
::

    python manage.py link_glossary [<course-slug> [<module-number> [<step-number>]]]

The arguments are used to refine the search for explorations/quickclicks. If all are omitted, then every single exploration/quickclick in the database is checked, which can be rather time consuming.

Behaviour
^^^^^^^^^

First, all the steps corresponding to the arguments are collected. Then, the html contents of the explorations and quizzes are passed through the glossary linker. The contents are specifically the 

* ``text``,
* ``post_text``,
* ``hints``, and
* ``explanations``

fields.

These are checked for glossary popups. If the popup does not have a defined lookup term, the display term is used, and patched.
"""
from django.core.management.base import BaseCommand, CommandError
from django.db import transaction

import re

from exploration.models import Slide
from quiz.models import Question

from courseware.models import Course

from glossary.models import Word

from os import path

class Command(BaseCommand):
    # TODO Update to add_arguments style 
    # https://docs.djangoproject.com/en/1.8/howto/custom-management-commands/#accepting-optional-arguments
    args = '[<course-slug> [<unit-number> [<module-number>]]]'
    help = 'Go through *all* site quizes and explorations, and link the glossary attributes in html to the glossary terms'

    def handle(self, *args, **kwargs):
        # TODO handle course unit and module arguments
        if len(args) > 3:
            raise CommandError('Too many arguments')
        course = None;
        unit = None;
        module = None;
        if len(args) > 0:
            try:
                course = Course.objects.get(slug=args[0])
            except:
                raise CommandError('Course may not exist. Please use all lowercase, and dashes instead of spaces')
        try:
            if len(args) > 1:
                unit = course.unit_set.get(position=args[1])
            if len(args) > 2:
                module = unit.module_set.get(position=args[2])
        except:
            raise CommandError('Unit or Module cannot be found')

        if module:
            if match_module(module):
                raise CommandError('Module is not a quiz or exploration')
        elif unit:
            ms = unit.module_set.all()
            for m in ms:
                match_module(m)
        elif course:
            us = course.unit_set.all()
            for u in us:
                ms = u.module_set.all()
                for m in ms:
                    match_module(m)
        else:
            print('Going through ALL explorations and quickclicks')
        
            slide_set = Slide.objects.all()
            glossary_match_exploration(slide_set)

            question_set = Question.objects.all()
            glossary_match_quiz(question_set)


def match_module(module):
    if module.content_type.model == 'exploration':
        slide_set = module.content_object.slide_set.all()
        glossary_match_exploration(slide_set)
    elif module.content_type.model == 'quiz':
        question_set = module.content_object.question_set.all()
        glossary_match_quiz(question_set)
    else:
        return 'Not a valid module type'

def glossary_match_exploration(slide_set):
    for slide in slide_set:
        log_slide(slide)
        glossary_match_slide(slide, False)
        
def glossary_match_quiz(question_set):
    for question in question_set:
        log_question(question)
        glossary_match_question(question, False)



def log_slide(slide):
    """
    print the slide id, and where it can be found in the course
    """
    print("Checking slide: %d" % slide.id)
    exploration = slide.exploration
    module = exploration.modules.all()
    if module:
        module = module[0]
        unit = module.unit
        print("\tLocated at Unit %d, Module %d, Slide %d" % (unit.position, module.position, slide.index))

def log_question(slide):
    """
    print the question id, and where it can be found in the course
    """
    print("Checking question: %d" % slide.id)
    quiz = slide.quiz
    module = quiz.modules.all()
    if module:
        module = module[0]
        unit = module.unit
        print("\tLocated at Unit %d, Module %d, Question %d" % (unit.position, module.position, slide.index))


def glossary_match_slide(slide, dry_run=True):
    """
    Scan through a slide's html contents, and update any glossary spans found

    If ``dry_run`` is True (default), then don't change the contents.
    """
    with transaction.atomic():
        slide.text = gloss_match(slide.text)
        print("notes")
        slide.post_text = gloss_match(slide.post_text)
        print("hints")
        slide.hints = [ gloss_match(h) for h in slide.hints ]
        if not dry_run:
            slide.save()

def glossary_match_question(question, dry_run=True):
    """
    Scan through a question's html contents, and update any glossary spans found

    If ``dry_run`` is True (default), then don't change the contents.
    """
    with transaction.atomic():
        question.text = gloss_match(question.text)
        for c in question.choice_set.all():
            c.text = gloss_match(c.text)
            c.hint = gloss_match(c.hint)
            c.explanation = gloss_match(c.explanation)
            if not dry_run:
                c.save()
        if not dry_run:
            question.save()

p_old_glossary = re.compile(r'<span glossary="([^"]*?)">(.*?)</span>')
p_glossary = re.compile(r'<span glossary="([^"]*?)" term="([^"]*?)">(.*?)</span>')


def gloss_match(text):

    # parts will be a list of the form [text] + [existing_id, displayed term, text] for each match.
    parts = p_old_glossary.split(text)
    new_text = [parts[0]]
    for i in range(1, len(parts), 3):
        display = parts[i+1]
        term = parts[i]
        if not term:
            term = display
        gloss_element = ['<span glossary="', term, '">', display, '</span>']
        new_text.extend(gloss_element)
        new_text.append(parts[i+2])
    return ''.join(new_text)
    

def lookup_term(term):
    matches = Word.objects.filter(word=term)
    print("Found the matches...")
    for w in matches:
        print("\t%d\t%s\t - %s" % (w.pk, w.word, w.definition[0:50] + '...')) 
    return ','.join([str(w.pk) for w in matches])
