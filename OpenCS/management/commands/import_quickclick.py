"""
``import_quickclick`` command
------------------------------

This Django management command adds a quickclick (quiz) to the database, assigning it to the proper module and step.

usage
^^^^^
::

    python manage.py import_quickclick <quickclick-file>

The quickclick file can be located anywhere, because it is not referenced after import. 

.. note:: 
    For LaTeX-based quickclicks, the module and step numbers are determined by the file name, thus they need to contain the pattern::

        quickclick<module-number><step-letter>

    where 
        * ``<module-number>`` is a two-digit number, 
        * ``<step-letter>`` is the number of the step encoded as a letter (a -> 1, b ->2, ...)

    The actual regex pattern for this is::

        ^([a-zA-Z]*)(\d\d)([a-z])

    there is no check that the string preceding the module number is "quickclick". However, an error will be raised if it contains any non-letters.

    **Markdown formatted quickclick files use the header-line for metadata, not filename**

Behaviour
^^^^^^^^^

First, the module and step are looked up in the *Staging* course. If the pre-existing step is not a quiz/quickclick, an error is raised. If either is missing, it is created, and the new module's title will be "untitled". 

Next, a new ``Quiz`` object is created, and the contents of the file are used to populate it with new ``questions`` and ``choices``. If the title is given in the preamble, then the module and quiz are given that title. The default value for ``title`` is "Step N" for markdown-based and "Step N" for latex-based

After the quiz is created, it is assigned to the step, and the pre-existing quiz is deleted (if there is one).

.. note:: The entire process is performed in an atomic transaction. Therefore, if any part of the process fails by raising an exception, then no changes will be commited to the database.
"""
from django.core.management.base import BaseCommand, CommandError
from django.db import transaction

import OpenCS.latex2markdown as l2m
import OpenCS.markdown as md
import re

from django.contrib.contenttypes.models import ContentType
from quiz.models import Quiz, Question, Choice, QuizImage
from courseware.models import Course, Unit, Module

from django.conf import settings
import shutil
import os
from os import path

class Command(BaseCommand):
    # TODO Update to add_arguments style 
    # https://docs.djangoproject.com/en/1.8/howto/custom-management-commands/#accepting-optional-arguments
    args = '<quickclick file>'
    help = 'Import the quickclick from the file'

    def handle(self, *args, **kwargs):
        try:
            filename = args[0]
            if not(filename.startswith('/')):
                filename = path.join(os.getcwd(), args[0])
            self.filepath = path.dirname(filename)
        except IndexError:
            raise CommandError('Wrong number of arguments')
        
        extension = path.splitext(filename)[1]
        if  extension == ".tex":
            parser = LatexImport()
            parser.filepath = path.dirname(filename)
            parser.import_quickclick(filename)
        elif extension == ".md":
            parser = MarkdownImport()
            parser.filepath = path.dirname(filename)
            parser.import_quickclick(filename)
        else:
            raise CommandError('unsupported file format (%s). Please use either .tex or .md' % extension)
        
## The following functions/classes do all the actual work of parsing the files
class MarkdownImport():
    """
    Markdown-based import for the new webcourses

    This will need to be updated to support python courses as well.
    """

    p_header = re.compile(r'<!--+\s*[Mm]odule\s*(\d+)[,\s]+[Ss]tep\s*(\d+)[,\s]+([a-zA-Z])+\s* --+>')
    p_begin = re.compile(r'<!---*\s*BEGIN\s*---*>')
    p_title = re.compile(r'^[Tt]itle[:\-\s]+(.*)', re.MULTILINE)
    p_question = re.compile(r'<!---*\s*[Qq]uestion\s*---*>')

    def import_quickclick(self, filename):
        with open(filename) as f:
            contents = f.read()
            preamble, q_texts = self.p_begin.split(contents)
            q_texts = self.p_question.split(q_texts)

        # Determine module and unit numbers from header
        m = self.p_header.search(preamble)
        if m:
            un = int(m.group(1))
            mn = int(m.group(2))
        else:
            raise CommandError('Non-standard header: %s' % preamble.splitlines(1)[0])
        print('\nQuickclick, unit %d, module %d' %(un, mn))

        # XXX It is assumed that this is a web course
        # TODO allow the user to specify the staging area
        # All new content goes into "staging"
        course = Course.objects.get(slug='staging-web')
        quiz_type = ContentType.objects.get_for_model(Quiz)

        # Look for the title clause in preamble
        ms = self.p_title.split(preamble)
        if len(ms)>1:
            title = ms[1].strip()
            print('title: %s' % title)
            description = ms[2].strip()
        else:
            title = 'Step %d' % mn
            description = 'Quick Click'

        # The rest is atomic, because we start creating objects if they don't exist
        with transaction.atomic():
            # Find the unit, and create it if it doesn't exist
            unit = course.unit_set.filter(position=un)
            if unit:
                unit = unit[0]
            else:
                print("creating unit")
                unit = Unit()
                unit.position = un
                unit.title = 'untitled'
                unit.course = course
                unit.save()
            
            # Find the module, and create it if it doesn't exist
            module = unit.module_set.filter(position=mn)
            if module:
                module = module[0]
                module.title = title
                module.description = description
                if not(module.content_type == quiz_type):
                    raise CommandError("Pre-existing module is not an exploration. Unit: %d, Module %d" % (un, mn) )
            else:
                print("creating module")
                module = Module()
                module.content_type = quiz_type
                module.unit = unit
                module.position = mn
                module.title = title
                module.description = description

            # Now create the quiz, and attach it to the model
            self.quiz = Quiz()
            self.quiz.title = title
            self.quiz.save()

            index = 1
            print('question: ', end='')
            for qt in q_texts[1:]:
                if qt:
                    print(index, end=' ')
                    question = self.parse_question(index, qt)
                    question.save()
                    index = index + 1

            old_quiz = module.content_object
            module.content_object = self.quiz
            module.save()
            print('saved')
            if old_quiz:
                old_quiz.delete()


    p_paragraph = re.compile(r'</?p>')
    p_choice = re.compile(r'<!--+\s*[Cc]hoices\s*--+>')
    p_hint = re.compile(r'<!--+\s*[Hh]ints?\s*--+>')
    p_explanation = re.compile(r'<!--+\s*(?:[Ee]xplanations?|[Ff]eedback)\s*--+>')
    p_multi = re.compile(r'[mM]ulti[cC]orrect')
    p_correct = re.compile(r'<!--+\s*(?:[mM]ulti)?[cC]orrect\s*--+>')
    p_correct_short = re.compile(r'<!--+\s*(?:[mM]ulti)?[cC]orrect:?[\s\-]*(.*?)\s+--+>')

    def parse_question(self, index, text):
        q = Question()
        q.quiz = self.quiz
        q.index = index

        parts = self.p_choice.split(text)
        # We can default to short-form question
        (text, choices) = self.parse_short(parts[0])
        html = md.markdown(text)
        q.text = html
        q.answer_type = q.SHORT

        if len(parts) > 1:           # The question is actually multiplechoice
            choices = self.parse_multi(parts[1])
            if self.p_multi.search(parts[1]):
                q.answer_type = q.MULTI
            else:
                q.answer_type = q.SINGLE

        q.save()
        for c in choices:
            c.question = q
            c.save()

        self.save_images(q, html)

        return q

    def parse_short(self, text):
        """ Parse a shortform question 

        In the case of a multi-part, the text will still be right, but the choices will be wrong.
        """
        parts = self.p_correct_short.split(text)
        # parts will be in the form [text, correct_answer, text, ...]
        qtext = parts[0]

        choices = []
        index = 1
        for i in range(1, len(parts), 2):
            c = Choice()
            c.text = parts[i].strip()
            c.index = index
            index = index+1
            (c.hint, c.explanation) = self.parse_explanation(parts[i+1])
            choices.append(c)
        return (qtext, choices)

    def parse_multi(self, text):
        """ Parse the multiple choice part of a question """
        (choice_list, remaining) = self.p_hint.split(text, 1)
        # TODO fix up list extraction
        c_texts = self.split_list(choice_list)
        more = self.p_explanation.split(remaining, 1)
        if len(more) == 1:
            c_hints = self.split_list(more[0])
            c_explanations = ["" for h in c_hints]
        else:
            c_hints = self.split_list(more[0])
            c_explanations = self.split_list(more[1])

        choices = []
        index = 1;
        for t, h, e in zip(c_texts, c_hints, c_explanations):
            c = Choice()
            if self.p_correct.search(t):
                c.is_correct = True
            else:
                c.is_correct = False
            # Remove the 'correct' markup from all texts
            t = self.p_correct.sub('', t)
            h = self.p_correct.sub('', h)
            e = self.p_correct.sub('', e)
            # If there is no explanation block, hints for correct questions are explanations
            if c.is_correct and len(more)==1:
                e = h
                h = ""
            text = md.markdown(t)
            hint = md.markdown(h)
            explanation = md.markdown(e)
            # XXX The way hints and explanations work, paragraphs are not allowed
            c.text = self.p_paragraph.sub('', text)
            c.hint = self.p_paragraph.sub('', hint)
            c.explanation = self.p_paragraph.sub('', explanation)
            c.index = index
            index = index +1
            choices.append(c)

        return choices

    def parse_explanation(self, text):
        """ Finds the hint and explanation for short-form answers """
        h = self.p_hint.search(text)
        e = self.p_explanation.search(text)
        if h and e:
            if h.end() < e.end():
                # Hint *should* come first
                hint = md.markdown(text[h.end():e.start()-1])
                explanation = md.markdown(text[e.end():])
                return (hint, explanation)
            elif e.end() < h.end():
                # explanation *should* come first
                explanation = md.markdown(text[e.end():h.start()-1])
                hint = md.markdown(text[h.end():])
                return (hint, explanation)
        elif h:
            # only hint, no explanation
            hint = md.markdown(text[h.end():])
            return (hint, "")
        elif e:
            # only explanation, no hint
            explanation = self.markdown(text[e.end():])
            return ("", explanation)
        else:
            # no hint or explanation
            return ("", "")

    p_md_list = re.compile(r'\n\s*[*+\-](?=\s)')
    def split_list(self, text):
        """ Splits up the different components of a markdown list
        """
        return [ t.strip() for t in self.p_md_list.split(text)[1:] ]

    p_image = re.compile(r'<img\s+alt="([^"]*)"\s+src="([^"]*)')
    def save_images(self, q, text):
        # XXX The slide must be saved first!
        # This will be a list in the form [text, image_title, image_file, ...]
        image_components = self.p_image.split(text)
        for i in range(1, len(image_components), 3):
            img = QuizImage()
            img.question = q
            img.title = image_components[i]
            image_file = image_components[i+1]
            orig_file = path.join(self.filepath, image_file)
            image_file = path.join('images/quiz/%d/' % (q.quiz.id), path.basename(image_file))
            stored_file = path.join(settings.MEDIA_ROOT,  image_file)
            os.makedirs(path.dirname(stored_file), mode=0o775, exist_ok=True)
            try:
                shutil.copyfile(orig_file, stored_file)
                img.image = image_file
                img.save()
            except FileNotFoundError:
                print('\n\tWarning: image file %s is missing\n' % image_components[i+1])


class LatexImport():
    """
    Latex-based import for legacy.

    This is left in place to support the import of old-format python lessons.
    """

    def import_quickclick(self, filename):
        # Determine module and unit numbers from filename
        pattern = re.compile(r'([a-zA-Z]*)(\d\d)([a-z])')
        m = pattern.match(path.split(filename)[1])
        if m:
            un = int(m.groups()[1])
            mn = ord(m.groups()[2])-96
        else:
            raise CommandError('Non-standard quickclick name: %s' % path.split(filename)[1])
        print('\nQuickClick, unit %d, module %d' %(un, mn))

        with open(filename) as f:
            contents = f.read()
            q_texts = contents.split(r'% Question')


        # All new content goes into "staging"
        # XXX For latex-based, it is assumed to go into "Staging"
        course = Course.objects.get(title='Staging')
        quiz_type = ContentType.objects.get_for_model(Quiz)

        title = 'Step %d' % mn
        description = 'Quick Click'

        # The rest is atomic, because we start creating objects if they don't exist
        with transaction.atomic():
            # Find the unit, and create it if it doesn't exist
            unit = course.unit_set.filter(position=un)
            if unit:
                unit = unit[0]
            else:
                print("creating unit")
                unit = Unit()
                unit.position = un
                unit.title = 'untitled'
                unit.course = course
                unit.save()
            
            # Find the module, and create it if it doesn't exist
            module = unit.module_set.filter(position=mn)

            if module:
                module = module[0]
                if not(module.content_type == quiz_type):
                    raise CommandError("Pre-existing module is not a quiz. Unit: %d, Module %d" % (un, mn) )
            else:
                print("creating module")
                module = Module()
                module.content_type = quiz_type
                module.unit = unit
                module.position = mn
                module.title = title
                module.description = description

            # Now create the quiz, and attach it to the model
            quiz = Quiz()
            quiz.title = title
            quiz.save()

            index = 1
            print('question: ', end='')
            for qt in q_texts[1:]:
                if qt:
                    print(index, end=' ')
                    (q, choices) = self.parse_question(qt)
                    q.quiz = quiz
                    q.index = index
                    q.save()
                    index = index + 1

                    for c in choices:
                        c.question = q
                        c.save();

            old_quiz = module.content_object
            module.content_object = quiz
            module.save()
            print('saved')
            if old_quiz:
                old_quiz.delete()
            

    ## The following functions do all the actual work of parsing the files

    p_correct = re.compile(r'%\s*(?:[mM]ulti)?[cC]orrect:?\s*(.*)')
    p_multi = re.compile(r'[mM]ulti[cC]orrect')
    p_paragraph = re.compile(r'</?p>')
    p_hint = re.compile(r'%\s*[hH]int')
    p_explanation = re.compile(r'%\s*[eE]xplanation')

    def parse_question(self, text):
        q = Question()

        parts = text.split(r'% Choices')
        # We can default to short-form question
        (text, choices) = self.parse_short(parts[0])
        q.answer_type = q.SHORT
        if len(parts) > 1:           # The question is actually multiplechoice
            choices = self.parse_multi(parts[1])
            if self.p_multi.search(parts[1]):
                q.answer_type = q.MULTI
            else:
                q.answer_type = q.SINGLE

        q.text = md.markdown(text)

        return (q, choices);

    def get_enumerate(self, text):
        parts = text.split(r'\end{enumerate}', 1)
        verb = parts[0].split(r'\begin{enumerate}')[1].strip()
        return verb

    def parse_multi(self, text):
        """ Parse a question that has multiple choices """
        parts = text.split(r'% Hints')
        c_texts = [ t.strip() for t in self.get_enumerate(parts[0]).split(r'\item{}')[1:] ]
        more = parts[1].split(r'% Explanations')
        if len(more) == 1:
            c_hints = [ h.strip() for h in self.get_enumerate(more[0]).split(r'\item{}')[1:] ]
            c_explanations = ["" for h in c_hints]
        else:
            c_hints = [ h.strip() for h in self.get_enumerate(more[0]).split(r'\item{}')[1:] ]
            c_explanations = [ h.strip() for h in self.get_enumerate(more[1]).split(r'\item{}')[1:] ]
        choices = []
        index = 1;
        for t, h, e in zip(c_texts, c_hints, c_explanations):
            c = Choice()
            if self.p_correct.search(t):
                c.is_correct = True
            else:
                c.is_correct = False
            t = self.p_correct.sub('', t)
            h = self.p_correct.sub('', h)
            e = self.p_correct.sub('', e)
            text_lines = [ l2m.process_line(l) for l in t.splitlines() ]
            if c.is_correct and len(more)==1:
                e = h
                h = ""
            hint_lines = [ l2m.process_line(l) for l in h.splitlines() ]
            explanation_lines = [ l2m.process_line(l) for l in e.splitlines() ]
            text = md.markdown( '\n'.join(text_lines) )
            hint = md.markdown( '\n'.join(hint_lines) )
            explanation = md.markdown( '\n'.join(explanation_lines) )
            c.text = self.p_paragraph.sub('', text)
            c.hint = self.p_paragraph.sub('', hint)
            c.explanation = self.p_paragraph.sub('', explanation)
            c.index = index
            index = index +1
            choices.append(c)

        return choices


    def parse_short(self, text):
        """ Parse  a shortform question """
        l2m.clear_state()
        parts = self.p_correct.split(text)
        qtext = self.convert_text_md(parts[0])

        choices = []
        index = 1
        for i in range(1, len(parts), 2):
            c = Choice()
            c.text = parts[i].strip()
            c.index = index
            index = index+1
            (c.hint, c.explanation) = self.parse_explanation(parts[i+1])
            choices.append(c)
        return (qtext, choices)


    def convert_text_md(self, text):
        """ Given a chunk of LaTeX, returns the markdown equivalent
        """
        lines = text.splitlines()
        md_lines = []
        for l in lines:
            md_lines.append(l2m.process_line(l))
        return '\n'.join(md_lines)

    def convert_text(self, md_text):
        """ Given a chunk of LaTeX, returns the html equivalent
        """
        return md.markdown(self.convert_text_md(md_text))

    def parse_explanation(self, text):
        h = self.p_hint.search(text)
        e = self.p_explanation.search(text)
        if h and e:
            if h.end() < e.end():
                # Hint *should* come first
                hint = self.convert_text(text[h.end():e.start()-1])
                explanation = self.convert_text(text[e.end():])
                return (hint, explanation)
            elif e.end() < h.end():
                # explanation *should* come first
                explanation = self.convert_text(text[e.end():h.start()-1])
                hint = self.convert_text(text[h.end():])
                return (hint, explanation)
        elif h:
            # only hint, no explanation
            hint = self.convert_text(text[h.end():])
            return (hint, "")
        elif e:
            # only explanation, no hint
            explanation = self.convert_text(text[e.end():])
            return ("", explanation)
        else:
            # no hint or explanation
            return ("", "")

