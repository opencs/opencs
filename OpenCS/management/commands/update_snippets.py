"""
``update_snippets`` command
---------------------------

This Django management command updates the snippets in the database, replacing the contents in the order they come within each step.

usage
^^^^^
::

    python manage.py update_snippets <snippets-file>

The snippets file can be located anywhere, because it is not referenced after import. The index of the snippet is based on the order that it comes in the file.

Behaviour
^^^^^^^^^

The snippets file is first split into its individual steps.

.. note:: This split is done by matching on the substring ``"#####"``. Do not use that many hashmarks anywhere else in the snippet file.

Then for each step text, the following is performed:

1. The metadata is pulled from the first line (e.g. "#### Module 2, step 3").
2. The step is split into its individual snippets
3. The snippets are created by ``step`` and ``position`` within that step. Existing snippets are replaced.
"""
from django.core.management.base import BaseCommand, CommandError
from django.db import transaction

from courseware.models import Module
from snippets.models import CodeSnippet

import re
p_step_header = re.compile(r'[Mm]odule[\s,]+(\d+)[\s,]+[Ss]tep[\s,]+(\d+)')

class Command(BaseCommand):
    # TODO Update to add_arguments style 
    # https://docs.djangoproject.com/en/1.8/howto/custom-management-commands/#accepting-optional-arguments
    args = '<snippets-file>'
    help = 'Adds/replaces all the snippets defined in the file. (snippets defined in order)'

    def handle(self, *args, **kwargs):
        try:
            filename = args[0]
        except IndexError:
            raise CommandError('Wrong number of arguments')

        f = open(filename, 'r')
        steps = [ p for p in f.read().split('#####') if p]   # a 'pythonic' way to filter out empy strings
        for step in steps:
            step_header = step.split('\n', 1)[0]
            m = p_step_header.search(step_header)
            if m:
                mn = m.group(1)
                sn = m.group(2)
            else:
                raise CommandError('Malformed step header: %s' % ('#####'+step_header))

            # XXX We're assuming the module/step already exists
            # XXX Only python-from-scratch is supported for now
            try:
                module = Module.objects.get(position=sn, unit__position=mn, unit__course__slug='python-from-scratch')
            except:
                module = None

            print(step_header)
            if not module:
                print('step does not exist')
            snippets = step.split('####')
            position = 1
            for snip in snippets[1:]:
                # Replace the one in this position if it exists
                s = CodeSnippet.objects.filter(module=module, position=position)
                if s:
                    s = s[0]
                else:
                    s = CodeSnippet()
                s.code = ('####' + snip).strip();
                s.language = CodeSnippet.PYTHON
                s.name = snip.split('\n', 1)[0].strip()
                s.module = module
                s.position = position
                s.save()
                position = position + 1
