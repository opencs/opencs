"""
``import_exploration`` command
------------------------------

This Django management command adds an exploration to the database, assigning it to the proper module and step.

usage
^^^^^
::

    python manage.py import_exploration <exploration-file>

The exploration file can be located anywhere, because it is not referenced after import. For LaTeX-based explorations, the module and step numbers are determined by the file name, thus they need to contain the pattern::

    explore<module-number><step-letter>

where 
    * ``<module-number>`` is a two-digit number, 
    * ``<step-letter>`` is the number of the step encoded as a letter (a -> 1, b ->2, ...)

The actual regex pattern for this is::

    ^([a-zA-Z]*)(\d\d)([a-z])

.. note:: there is no check that the string preceding the module number is "explore". However, an error will be raised if it contains any non-letters.

However, metadata for Markdown-based explorations is determined by the preamble of the file, so there is no restriction on the filename apart from extension.

Behaviour
^^^^^^^^^

First, the unit and module are looked up in the *Staging* course. If the pre-existing module is not an exploration, an error is raised. If either is missing, it is created. A new unit's title will be "untitled". 

Next, a new ``Exploration`` object is created, and the contents of the file are used to populate it with new slides/exercises. If the type of the module is ``demo``, then the icon is set to the demo icon, not the default. Similarly, if the type is ``solution``, then the icon is set to the solution icon.

The ``title`` and ``description`` fields of the module are set to the values in the pre-amble with default: "Step N" for markdown or "Step N" for LaTeX.

.. note:: it was supposed to be changed to 'page' for web material, but that would also require changing certain strings all over the site (like next step buttons)

The code files (for web explorations) will only be stored with their basename (that is just the file, no path/directory), which means all html/css/js files will be created in the same directory in the exploration.

.. note:: References between files should assume that they are in the same directory, 
   so ``relative/path/to/style.css`` should be included as::
        <link rel=stylesheet href="style.css" />

   However, any files that are not html/css/js cannot be included as part of the demo source. These files need to be uploaded separately, and  included with an **absolute** path.

If the slide does not contain an ``html`` code block, then the ``show_rendered`` flag is set to false. Likewise, if the slide does not contain a ``js`` code block, the ``show_console`` flag is set to false.

Any images in the exploration are copied into the ``MEDIA_ROOT`` under a unique directory for the slide. This means that if an image is used in more than one slide, it will be stored once for each slide. However, we also avoid name collisions, and make it possible to delete all files associated with an object without affecting others.

After the new exploration is created, it is assigned to the step, and the pre-existing exploration is deleted (if there is one).

.. note:: The images associated with the old exploration and slides are not removed.

.. note:: The entire process is performed in an atomic transaction. Therefore, if any part of the process fails by raising an exception, then no changes will be commited to the database.
"""
from django.core.management.base import BaseCommand, CommandError
from django.db import transaction

import OpenCS.latex2markdown as l2m
import OpenCS.markdown as md
import re

from django.contrib.contenttypes.models import ContentType
from exploration.models import Exploration, Slide, Test, ExplorationImage, CodeBlock
from courseware.models import Course, Unit, Module

from django.conf import settings
import shutil
from os import path
import os

class Command(BaseCommand):
    # TODO Update to add_arguments style 
    # https://docs.djangoproject.com/en/1.8/howto/custom-management-commands/#accepting-optional-arguments
    args = '<exploration file>'
    help = 'Import the exploration from the file'

    def handle(self, *args, **kwargs):
        try:
            filename = args[0]
            if not(filename.startswith('/')):
                filename = path.join(os.getcwd(), args[0])
            self.filepath = path.dirname(filename)
        except IndexError:
            raise CommandError('Wrong number of arguments')
        
        extension = path.splitext(filename)[1]
        if  extension == ".tex":
            parser = LatexImport()
            parser.filepath = path.dirname(filename)
            parser.import_exploration(filename)
        elif extension == ".md":
            parser = MarkdownImport()
            parser.filepath = path.dirname(filename)
            parser.import_exploration(filename)
        else:
            raise CommandError('unsupported file format (%s). Please use either .tex or .md' % extension)


## The following functions/classes do all the actual work of parsing the files
class MarkdownImport():
    """
    Markdown-based import for the new webcourses

    This will need to be updated to support python courses as well.
    """

    p_header = re.compile(r'<!--+\s*[Mm]odule\s*(\d+)[,\s]+[Ss]tep\s*(\d+)[,\s]+([a-zA-Z]+)\s* --+>')
    p_title = re.compile(r'^[Tt]itle[:\-\s]+(.*)', re.MULTILINE)
    p_begin = re.compile(r'<!--+\s*BEGIN[\-\s]*(\d*)\s*--+>')
    p_continue = re.compile(r'<!--+\s*[Cc]ontinue[\-\s]*(\d*)\s*--+>')

    def import_exploration(self, filename):
        with open(filename) as f:
            contents = f.read()
            preamble, first_pane_size, slide_texts = self.p_begin.split(contents)
            slide_texts = self.p_continue.split(slide_texts)
            slide_texts.insert(0, first_pane_size)

        # Determine module and unit numbers from header
        m = self.p_header.search(preamble)
        if m:
            un = int(m.group(1))
            mn = int(m.group(2))
            exp_type = m.group(3)
        else:
            raise CommandError('Non-standard header: %s' % preamble.splitlines(1)[0])
        if exp_type.lower() == "demo":
            print('\nDemo, unit %d, module %d' %(un, mn))
        elif exp_type.lower() == "solution":
            print('\nSolution, unit %d, module %d' %(un, mn))
        else:
            print('\nExploration, unit %d, module %d' %(un, mn))

        # Look for the title clause in preamble
        ms = self.p_title.split(preamble)
        if len(ms)>1:
            title = ms[1].strip()
            print('title: %s' % title)
            description = ms[2].strip()
        else:
            title = 'Step %d' % mn
            description = exp_type

        # XXX It is assumed that this is a web course
        # TODO allow the user to specify the staging area
        # All new content goes into "staging"
        course = Course.objects.get(slug='staging-web')
        exploration_type = ContentType.objects.get_for_model(Exploration)

        # The rest is atomic, because we start creating objects if they don't exist
        with transaction.atomic():
            # Find the unit, and create it if it doesn't exist
            unit = course.unit_set.filter(position=un)
            if unit:
                unit = unit[0]
            else:
                print("creating unit")
                unit = Unit()
                unit.position = un
                unit.title = 'untitled'
                unit.course = course
                unit.save()
            
            # Find the module, and create it if it doesn't exist
            module = unit.module_set.filter(position=mn)
            if module:
                module = module[0]
                module.title = title
                module.description = description
                if not(module.content_type == exploration_type):
                    raise CommandError("Pre-existing module is not an exploration. Unit: %d, Module %d" % (un, mn) )
            else:
                print("creating module")
                module = Module()
                module.content_type = exploration_type
                module.unit = unit
                module.position = mn
                module.title = title
                module.description = description
                if exp_type.lower() == 'demo':
                    module.icon = module.DEMO
                elif exp_type.lower() == 'solution':
                    module.icon = module.SOLUTION

            # Now create the exploration, and attach it to the model
            self.exp = Exploration()
            self.exp.title = title
            # XXX again, it is assumed that these are all web-course content
            self.exp.language = Exploration.WEB
            self.exp.save()

            index = 1
            print('slide: ', end='')
            for i in range(0, len(slide_texts), 2):
                pane_size = slide_texts[i] or '6'
                st = slide_texts[i+1]
                if st[i+1]:
                    print(index, end=' ')
                    slide = self.parse_slide(index, st)
                    slide.left_pane_size = pane_size
                    slide.save()
                    index = index + 1

            old_exp = module.content_object
            module.content_object = self.exp
            module.save()
            print('saved')
            if old_exp:
                old_exp.delete()


    p_code_block = re.compile(
            r'<!--+\s*(html|css|js)[\-\s]*([a-zA-Z]*)[\-\s]*([a-zA-z/.\-_]*)[\-\s]*(\d*)[\-\s]*--+>')
    p_paragraph = re.compile(r'</?p>')

    def parse_slide(self, index, text):
        s = Slide()
        s.exploration = self.exp
        s.index = index

        # `split_blocks` will be in the form:
        # [text, codeType, replace, path, size, ...]
        split_blocks = self.p_code_block.split(text)

        # XXX If any tests or hints are present, this will not find it
        # TODO Separate hints from the text and post_text
        text = split_blocks[0]
        (text, s.hints) = self.split_hints(text)
        post_text = split_blocks[-1]
        (post_text, post_hints) = self.split_hints(post_text)
        s.hints.extend(post_hints)
        s.text = text
        s.post_text = post_text
        s.save()
        
        codeTypes = []
        for i in range(1, len(split_blocks), 5):
            cb = CodeBlock()
            cb.slide = s
            cb.filetype = split_blocks[i]
            codeTypes.append(cb.filetype)
            replace = split_blocks[i+1]
            if replace in ['replace', 'keep', 'clear']:
                cb.replace = cb.REPLACE
            elif replace == 'keep':
                cb.replace = cb.NONE
            elif replace == 'append':
                cb.replace = cb.APPEND
            elif replace == 'prepend':
                cb.replace = cb.PREPEND
            else:
                raise CommandError("Unrecognized codebox action: %s" % replace)

            source_file = split_blocks[i+2]
            if (source_file):
                cb.filename = path.basename(source_file)
                source_file = path.join(self.filepath, source_file)
                with open(source_file) as sf:
                    cb.code = sf.read()

            size = split_blocks[i+3]
            if size:
                size = int(size)
                if size == 0:
                    cb.show = False
                else:
                    cb.size = size
            cb.save()

        # Auto-hide the irrelevant output types
        if not ('html' in codeTypes):
            s.show_rendered = False
        if not ('js' in codeTypes):
            s.show_console = False

        self.save_images(s, text)
        self.save_images(s, post_text)

        return s

    p_image = re.compile(r'<img\s+alt="([^"]*)"\s+src="([^"]*)')
    def save_images(self, s, text):
        # XXX The slide must be saved first!
        # This will be a list in the form [text, image_title, image_file, ...]
        image_components = self.p_image.split(text)
        for i in range(1, len(image_components), 3):
            img = ExplorationImage()
            img.slide = s
            img.title = image_components[i]
            image_file = image_components[i+1]
            orig_file = path.join(self.filepath, image_file)
            image_file = path.join('images/exp-slides/%s/' % (s.id), path.basename(image_file))
            stored_file = path.join(settings.MEDIA_ROOT,  image_file)
            os.makedirs(path.dirname(stored_file), mode=0o775, exist_ok=True)
            try:
                shutil.copyfile(orig_file, stored_file)
                img.image = image_file
                img.save()
            except FileNotFoundError:
                print('\n\tWarning: image file %s is missing\n' % image_components[i+1])

    p_hint = re.compile(r'<!--+\s*[Hh]int\s*-+(.*?)\s--+>')
    def split_hints(self, text):
        """ Extracts the hints from a chunk of text
        returns a tuple of (remaining text, [hints])
        """
        split = self.p_hint.split(text)
        hints = []
        first_pass = split[0]
        for i in range(1, len(split), 2):
            hints.append( split[i].strip() )
            first_pass = first_pass + split[i+1]
        return (md.markdown(first_pass), [md.markdown(h) for h in hints])


class LatexImport():
    """
    Latex-based import for legacy.

    This is left in place to support the import of old-format python lessons.
    """

    def import_exploration(self, filename):
        # Determine module and unit numbers from filename
        pattern = re.compile(r'([a-zA-Z]*)(\d\d)([a-z])')
        m = pattern.match(path.split(filename)[1])
        if m:
            un = int(m.groups()[1])
            mn = ord(m.groups()[2])-96
        else:
            raise CommandError('Non-standard exploration name: %s' % path.split(filename)[1])
        print('\nExploration, unit %d, module %d' %(un, mn))
        title = 'Step %d' % mn

        with open(filename) as f:
            contents = self.get_document(f.read())
            slide_texts = contents.split(r'\continue')

        # XXX It is assumed that this is a python-from-scratch course
        # All new content goes into "staging"
        course = Course.objects.get(title='Staging')
        exploration_type = ContentType.objects.get_for_model(Exploration)

        # The rest is atomic, because we start creating objects if they don't exist
        with transaction.atomic():
            # Find the unit, and create it if it doesn't exist
            unit = course.unit_set.filter(position=un)
            if unit:
                unit = unit[0]
            else:
                print("creating unit")
                unit = Unit()
                unit.position = un
                unit.title = 'untitled'
                unit.course = course
                unit.save()
            
            # Find the module, and create it if it doesn't exist
            module = unit.module_set.filter(position=mn)

            if module:
                module = module[0]
                if not(module.content_type == exploration_type):
                    raise CommandError("Pre-existing module is not an exploration. Unit: %d, Module %d" % (un, mn) )
            else:
                print("creating module")
                module = Module()
                module.content_type = exploration_type
                module.unit = unit
                module.position = mn
                module.title = title
                module.description = "Guided coding exercises"

            # Now create the exploration, and attach it to the model
            exp = Exploration()
            exp.title = 'Explore'
            exp.language = Exploration.PYTHON
            exp.save()

            index = 1
            print('slide: ', end='')
            for st in slide_texts:
                if st:
                    print(index, end=' ')
                    (slide, tests) = self.parse_slide(exp, index, st)
                    # default pane ratio is 2:3 for python 
                    # (and the latex format can't support otherwise)
                    slide.left_pane_size = 4
                    slide.save()
                    index = index + 1

                    for test in tests:
                        if test.has_tests():
                            test.slide = slide
                            test.save()

            old_exp = module.content_object
            module.content_object = exp
            module.save()
            print('saved')
            if old_exp:
                old_exp.delete()
        pass

    p_inbrace = re.compile(r'\{(.*?)\}')
    p_contains = re.compile(r'\\contains\{(.*?)\}')
    p_omits = re.compile(r'\\omits\{(.*?)\}')
    p_numsol = re.compile(r'%?\\sol\{(.*?)\}')
    p_hint = re.compile(r'Hint:(.*)$')
    p_quote_pair = re.compile(r'(".*"),\s*(".*")')
    p_pair = re.compile(r'(.*),\s*(.*)')
    p_quote = re.compile(r'(".*")')

    p_paragraph = re.compile(r'</?p>')

    def parse_slide(self, exp, index, text):
        s = Slide()
        s.exploration = exp
        s.index = index

        parts = text.split(r'\codebox')
        pre = parts[0]
        if parts[1].startswith('{below}'):
            s.replace_code = s.REPLACE
            (s.code, post) = self.split_verbatim(parts[1])
        elif parts[1].startswith('{clear}'):
            s.replace_code = s.REPLACE
            s.code = ''
            post = parts[1].split('\n', 1)[1]
        elif parts[1].startswith('{keep}'):
            s.replace_code = s.NONE
            s.code = ''
            post = parts[1].split('\n', 1)[1]
        else:
            # the contents of the braces is the code
            m = self.p_inbrace.match(parts[1])
            s.code = m.groups()[0]
            s.replace_code = s.REPLACE
            post = parts[1].split('\n', 1)[1]
        (text, ts, tests) = self.parse_text(pre)
        (post_text, nts, ntests) = self.parse_text(post)
        s.text = md.markdown(text)
        s.post_text = md.markdown(post_text)
        ts.extend(nts)
        tests.extend(ntests)
        
        s.save()
        self.save_images(s, text)
        self.save_images(s, post_text)

        # The 'test_lines' still have to be processed
        c_test = Test()
        c_test.test_type = Test.CONTAINS
        n_test = Test()
        n_test.test_type = Test.NUMSOL
        for t in ts:
            m = self.p_hint.match(t)
            if m:
                hint = self.p_paragraph.sub("",md.markdown(l2m.process_line(m.groups()[0])))
                s.hints.append(hint)
            m = self.p_contains.match(t)
            if m:
                c_test.add_contains(m.groups()[0])
            m = self.p_omits.match(t)
            if m:
                c_test.add_omits(m.groups()[0])
            m = self.p_numsol.match(t)
            if m:
                n_test.set_numsol(m.groups()[0])
        # Combine all the tests now
        tests.append(c_test)
        tests.append(n_test)

        return (s, tests);

    p_image = re.compile(r'!\[([^\]]*)\]\(([^)]*)\)')
    def save_images(self, s, text):
        # XXX The slide must be saved first!
        # This will be a list in the form [text, image_title, image_file, ...]
        image_components = self.p_image.split(text)
        for i in range(1, len(image_components), 3):
            img = ExplorationImage()
            img.slide = s
            img.title = image_components[i]
            image_file = image_components[i+1]
            orig_file = path.join(self.filepath, image_file)
            image_file = path.join('images/exp-slides/%s/' % (s.id), path.basename(image_file))
            stored_file = path.join(settings.MEDIA_ROOT,  image_file)
            os.makedirs(path.dirname(stored_file), mode=0o775, exist_ok=True)
            try:
                shutil.copyfile(orig_file, stored_file)
                img.image = image_file
                img.save()
            except FileNotFoundError:
                print('\n\tWarning: image file %s is missing\n' % image_components[i+1])

    def parse_text(self, text):
        """ Parse a segment of text, mixed with block tests """
        parts = text.split(r'\tests')
        (lines, test_lines) = self.parse_clean_text(parts[0])
        tests = []
        for p in parts[1:]:
            (test_block, rest) = self.split_verbatim(p)

            test = Test()
            m = self.p_inbrace.search(p)
            if m.groups()[0] == 'io':
                test.test_type = test.STDIO
                ios = self.split_io(test_block)
                for i, o in ios:
                    test.add_io(i, o)
                tests.append(test)
            else:
                test.test_type = test.FUNCTION
                test.set_function(test_block)
                tests.append(test)

            (nl, ntl) = self.parse_clean_text(rest)
            lines.extend(nl)
            test_lines.extend(ntl)

        return ('\n'.join(lines), test_lines, tests)


    def parse_clean_text(self, text):
        """ Parse just the text with oneline tests """
        l2m.clear_state()
        lines = text.splitlines()
        md_lines = []
        test_lines = []
        for l in lines:
            # check for the tests and hints
            if l.lower().startswith("hint"):
                test_lines.append(l)
            elif l.startswith(r'\contain'):
                test_lines.append(l)
            elif l.startswith(r'\omit'):
                test_lines.append(l)
            elif l.startswith(r'%\sol'):
                test_lines.append(l)
            else:
                md_lines.append(l2m.process_line(l))
        return (md_lines, test_lines)

    def split_io(self, text):
        lines = text.splitlines()
        ios = []
        for line in lines:
            m = self.p_quote_pair.search(line)
            if m:
                # XXX using eval to import a string representation :/
                ios.append((eval(m.groups()[0]), eval(m.groups()[1])))
                continue
            m = self.p_quote.search(line)
            if m:
                ios.append(('', eval(m.groups()[0])))
                continue
            m = self.p_pair.search(line)
            if m:
                ios.append(m.groups())
                continue
            # if it's not a pair, then I guess it's just testing the output
            ios.append( ('', line.strip()) )
        return ios

    def split_verbatim(self, text):
        parts = text.split(r'\end{verbatim}', 1)
        verb = parts[0].split(r'\begin{verbatim}')[1].strip()
        rest = parts[1]
        return (verb, rest)

    def get_document(self, text):
        parts = text.split(r'\end{document}', 1)
        # There's an additiona; '\noindent{ Exploration module 10c}' kind of line inside the document
        #doc = parts[0].split(r'\begin{document}')[1].strip()
        doc = parts[0].split(r'\noindent{')[1].split(r'}', 1)[1].strip()
        return doc

