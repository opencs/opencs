"""
``import_video`` command
------------------------

This Django management command adds a video file to the database, assigning it to the proper module and step.

usage
^^^^^
::

    python manage.py import_exploration <video-file>

The video file must under in the ``MEDIA_ROOT`` directory (currently ``/srv/media/``), typically in a ``videos/`` subdirectory. The module and step numbers are determined by the file name, thus they need to contain the pattern::

    <module-number><module-title>-<step-letter>

where 
    * ``<module-number>`` is a two-digit number, 
    * ``<module-title>`` is the title (optional) containing only letters,
    * ``<step-letter>`` is the number of the step encoded as a letter (a -> 1, b ->2, ...)

The actual regex pattern for this is::

    (\d\d)([a-zA-Z]*)-([a-z])

Behaviour
^^^^^^^^^

First, the module and step are searched for in the *Staging* course. If either the module or step do not exist, it is created along with a new VideoLesson object pointing to the file. In the case that the module does not already exist, it is given the title extracted from the file name.

If the module and/or step is pre-existing, the existing database entries are used and updated. The corresponding ``VideoLesson`` object is also updated to point to the new file. An additional check ensures that the step is a video lesson, and an error is raised otherwise.

If there is a ``.vtt`` file with the same name as the video, it will be included as the caption file.

.. note:: If the module, step, and videolesson already exist, the video can be updated by simply replacing the video file with the newer version.
"""
from django.core.management.base import BaseCommand, CommandError
from django.db import transaction

from django.conf import settings

from django.contrib.contenttypes.models import ContentType
from slides.models import VideoLesson
from courseware.models import Course, Unit, Module

from os import listdir, getcwd
from os.path import isfile, join, splitext
import re

class Command(BaseCommand):
    # TODO Update to add_arguments style 
    # https://docs.djangoproject.com/en/1.8/howto/custom-management-commands/#accepting-optional-arguments
    args = '<video file>'
    help = 'Import the video file, assigning it to the proper module and step'

    def handle(self, *args, **kwargs):
        try:
            video_file = args[0]
            if not(video_file.startswith('/')):
                video_file = join(getcwd(), args[0])
        except IndexError:
            raise CommandError('Wrong number of arguments')

        if video_file.startswith(settings.MEDIA_ROOT):
            rel_file = video_file[len(settings.MEDIA_ROOT)+1:]
        else:
            raise CommandError('path provided is not in MEDIA_ROOT')

        print("Importing %s" % video_file)
        print("Path relative to MEDIA_ROOT: %s" % rel_file)
        # files = [ f for f in listdir(video_path) if isfile(join(video_path, f))]
        files = [video_file]

        # check for the vtt file
        rel_vtt = splitext(rel_file)[0] + '.vtt'
        if not isfile( join(settings.MEDIA_ROOT, rel_vtt)):
            rel_vtt = None

        pattern = re.compile(r'(\d\d)([a-zA-Z]*)-([a-z])')
        
        # only working with the first course for now
        course = Course.objects.get(title='Staging')
        video_type = ContentType.objects.get_for_model(VideoLesson)

        # the filename metadata will be unit number, unit title, module number (as a letter)
        metadata = pattern.search(rel_file).groups()

        if metadata:
            un = int(metadata[0])
            ut = metadata[1]
            mn = ord(metadata[2]) -96
            print(un, ut, mn)

            # Find the unit, and create it if it doesn't exist
            unit = course.unit_set.filter(position=un)
            if unit:
                unit = unit[0]
            else:
                unit = Unit()
                unit.position = un
                unit.title = ut
                unit.course = course
                unit.save()
            
            # Find the module, and create it if it doesn't exist
            module = unit.module_set.filter(position=mn)
            if module:
                module = module[0]
                if module.content_type == video_type:
                    vl = module.content_object
                    vl.src = rel_file
                    print("src: %s" % vl.src)
                    if rel_vtt:
                        vl.vtt = rel_vtt
                        print("vtt: %s" % vl.vtt)
                    vl.save()
                else:
                    print("Pre-existing module is not a video. Unit: %d, Module %d" % (un, mn) )
            else:
                module = Module()
                module.content_type = video_type
                module.unit = unit
                module.position = mn
                module.title = "Video"
                module.description = "Video Lecture"

                vl = VideoLesson()
                vl.title = ut
                vl.src = rel_file
                print("vtt: %s" % vl.vtt)
                if rel_vtt:
                    vl.vtt = rel_vtt
                    print("vtt: %s" % vl.vtt)
                vl.save()
                
                module.content_object = vl
                module.save()

