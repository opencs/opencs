"""
``link_glossary`` command
------------------------------

This Django management command checks the glossary popup tags in explorations and quickclicks to make sure the terms return the correct definition.

usage
^^^^^
::

    python manage.py check_glossary [<course-slug> [<module-number> [<step-number>]]]

The arguments are used to refine the search for explorations/quickclicks. If all are omitted, then every single exploration/quickclick in the database is checked, which can be rather time consuming.

Behaviour
^^^^^^^^^

First, all the steps corresponding to the arguments are collected. Then, the html contents of the explorations and quizzes are passed through the glossary linker. The contents are specifically the 

* ``text``,
* ``post_text``,
* ``hints``, and
* ``explanations``

fields.

When a glossary popup of the form ``<span glossary="term">display term</span>`` is found, the definition is looked up. If the term is missing, a warning will be printed with its location

"""
from django.core.management.base import BaseCommand, CommandError
from django.db import transaction

import re

from exploration.models import Slide
from quiz.models import Question

from courseware.models import Course

from glossary.models import Word

from os import path

class Command(BaseCommand):
    # TODO Update to add_arguments style 
    # https://docs.djangoproject.com/en/1.8/howto/custom-management-commands/#accepting-optional-arguments
    args = '[<course-slug> [<unit-number> [<module-number>]]]'
    help = 'Go through *all* site quizes and explorations, and link the glossary attributes in html to the glossary terms'

    def handle(self, *args, **kwargs):
        # TODO handle course unit and module arguments
        if len(args) > 3:
            raise CommandError('Too many arguments')
        course = None;
        unit = None;
        module = None;
        if len(args) > 0:
            try:
                course = Course.objects.get(slug=args[0])
            except:
                raise CommandError('Course may not exist. Please use all lowercase, and dashes instead of spaces')
        try:
            if len(args) > 1:
                unit = course.unit_set.get(position=args[1])
            if len(args) > 2:
                module = unit.module_set.get(position=args[2])
        except:
            raise CommandError('Unit or Module cannot be found')

        if module:
            if check_module(module):
                raise CommandError('Module is not a quiz or exploration')
        elif unit:
            ms = unit.module_set.all()
            for m in ms:
                check_module(m)
        elif course:
            us = course.unit_set.all()
            for u in us:
                ms = u.module_set.all()
                for m in ms:
                    check_module(m)
        else:
            print('Going through ALL explorations and quickclicks')
        
            slide_set = Slide.objects.all()
            glossary_check_exploration(slide_set)

            question_set = Question.objects.all()
            glossary_check_quiz(question_set)


def check_module(module):
    if module.content_type.model == 'exploration':
        slide_set = module.content_object.slide_set.all()
        glossary_check_exploration(slide_set)
    elif module.content_type.model == 'quiz':
        question_set = module.content_object.question_set.all()
        glossary_check_quiz(question_set)
    else:
        return 'Not a valid module type'

def glossary_check_exploration(slide_set):
    for slide in slide_set:
        glossary_check_slide(slide, False)
        
def glossary_check_quiz(question_set):
    for question in question_set:
        glossary_check_question(question, False)



def log_slide(slide):
    """
    print the slide id, and where it can be found in the course
    """
    text = ("In slide: %d" % slide.id)
    exploration = slide.exploration
    module = exploration.modules.all()
    if module:
        module = module[0]
        unit = module.unit
        text += ("\tLocated at Unit %d, Module %d, Slide %d" % (unit.position, module.position, slide.index))
    return text

def log_question(slide):
    """
    print the question id, and where it can be found in the course
    """
    text = "In question: %d" % slide.id
    quiz = slide.quiz
    module = quiz.modules.all()
    if module:
        module = module[0]
        unit = module.unit
        text += "\tLocated at Unit %d, Module %d, Question %d" % (unit.position, module.position, slide.index)
    return text

def glossary_check_slide(slide, dry_run=True):
    """
    Scan through a slide's html contents, and update any glossary spans found

    If ``dry_run`` is True (default), then don't change the contents.
    """
    location = log_slide(slide)
    with transaction.atomic():
        gloss_check(slide.text, location)
        gloss_check(slide.post_text, location + ' notes')
        [ gloss_check(h, location+' hints') for h in slide.hints ]

def glossary_check_question(question, dry_run=True):
    """
    Scan through a question's html contents, and update any glossary spans found

    If ``dry_run`` is True (default), then don't change the contents.
    """
    location = log_question(question)
    with transaction.atomic():
        gloss_check(question.text, location)
        for c in question.choice_set.all():
            gloss_check(c.text, location + 'choice')
            gloss_check(c.hint, location + 'choice hint')
            gloss_check(c.explanation, location + 'choice explanation')

p_glossary = re.compile(r'<span glossary="([^"]*?)">(.*?)</span>')


def gloss_check(text, location):
    # parts will be a list of the form [text] + [lookup_term, displayed term, text] for each match.
    parts = p_glossary.split(text)
    new_text = [parts[0]]
    for i in range(1, len(parts), 3):
        display = parts[i+1]
        term = parts[i]
        if lookup_term(term):
            print(location)

def lookup_term(term):
    matches = Word.objects.filter(word=term)
    if matches:
        if len(matches) > 1:
            print("Warning: more than one match:")
            for w in matches:
                print("\t%d\t%s\t - %s" % (w.pk, w.word, w.definition[0:50] + '...')) 
            return True
        return False
    else:
        print("Warning: no match for %s" % term)
        return True
