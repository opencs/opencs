"""
Django settings for OpenCS project.

For more information on this file, see
https://docs.djangoproject.com/en/1.7/topics/settings/

For the full list of settings and their values, see
https://docs.djangoproject.com/en/1.7/ref/settings/
"""

from .common import *


# What is appended onto the hostname to access the python websocket
PYSOCK_SUFFIX = "/python-socket"

# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
import os

from config import *

# Production should definitely not have debug
DEBUG = False
TEMPLATE_DEBUG = False

# Allowed hosts is necessary
ALLOWED_HOSTS = [
        'localhost',
        '.uwaterloo.ca',
        ]



# Database
# https://docs.djangoproject.com/en/1.7/ref/settings/#databases

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': DB_NAME,
        'USER': DB_USER,
        'PASSWORD': DB_PASS,
        'HOST': DB_HOST,
        'PORT': DB_PORT,
    }
}
