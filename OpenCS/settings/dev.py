"""
Django settings for OpenCS project.

For more information on this file, see
https://docs.djangoproject.com/en/1.7/topics/settings/

For the full list of settings and their values, see
https://docs.djangoproject.com/en/1.7/ref/settings/
"""

from .common import *

# What is appended onto the hostname to access the python websocket
PYSOCK_SUFFIX = ":7000/"

# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
import os

# Let all other files know we're in DEV Mode
DEVMODE = True

# Quick-start development settings - unsuitable for production
# See https://docs.djangoproject.com/en/1.7/howto/deployment/checklist/

SECRET_KEY = 'pgbu=q@%@6#wv_6hr-hbfhley#rc&g@_h3ec$o_q9&m^vjtp@%'

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = True
TEMPLATE_DEBUG = True

ALLOWED_HOSTS = []


# Database
# https://docs.djangoproject.com/en/1.7/ref/settings/#databases

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': os.path.join(BASE_DIR, 'db.sqlite3'),
    }
}


STATIC_ROOT = os.path.join(BASE_DIR, 'static')
MEDIA_ROOT = os.path.join(BASE_DIR, 'media')

# For the web container service
CONTAINER_ROOT = os.path.join(MEDIA_ROOT, "containers")
CONTAINER_URL = "/media/containers/"
