"""
Django settings for OpenCS project.

For more information on this file, see
https://docs.djangoproject.com/en/1.7/topics/settings/

For the full list of settings and their values, see
https://docs.djangoproject.com/en/1.7/ref/settings/
"""

# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
import os

# Default to DEVMODE false
DEVMODE = False
ANALYTICS = False

# Because I've moved the common config up, we need a third dirname call
BASE_DIR = os.path.dirname(os.path.dirname(os.path.dirname(__file__)))
TEMPLATE_DIRS = [os.path.join(BASE_DIR, 'OpenCS/templates')]


# Application definition

INSTALLED_APPS = (
    'OpenCS',
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'django.contrib.sites',             # required for flatpages
    'django.contrib.flatpages',         # used for 'home', 'about', etc.
    'tastypie',
    'glossary',
    'quiz',
    'exploration',
    'slides',
    'courseware',
    'snippets',
)

# There's only one site, but this is needed for flatpages
SITE_ID=1

MIDDLEWARE_CLASSES = (
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.auth.middleware.SessionAuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
    'django.contrib.flatpages.middleware.FlatpageFallbackMiddleware',       # for flatpages
)

TEMPLATE_CONTEXT_PROCESSORS = (
    "django.contrib.auth.context_processors.auth",
    "django.core.context_processors.debug",
    "django.core.context_processors.i18n",
    "django.core.context_processors.media",
    "django.core.context_processors.static",
    "django.core.context_processors.tz",
    "django.contrib.messages.context_processors.messages",
    "OpenCS.contexts.analytics_context",
    "OpenCS.contexts.url_context",
)


ROOT_URLCONF = 'OpenCS.urls'

WSGI_APPLICATION = 'OpenCS.wsgi.application'


# Internationalization
# https://docs.djangoproject.com/en/1.7/topics/i18n/

LANGUAGE_CODE = 'en-us'

TIME_ZONE = 'UTC'

USE_I18N = True

USE_L10N = True

USE_TZ = True


# Static files (CSS, JavaScript, site-Images)
# root set in .dev or .production
STATIC_URL = '/static/'
STATICFILES_DIRS = (
    os.path.join(BASE_DIR, 'OpenCS/static'),
    )

# Uploaded media files (images and video for instructional material)
# root set in .dev or .production
MEDIA_URL = '/media/'

""" Additional notes:

    DATE_INPUT_FORMATS is ommitted, because we don't have any date entries at the moment.
    Likewise for datetime
"""
