'use strict';

var PythonSocket = function(url, options) {

    // Default settings
    this.settings = {
        debug: false,
        timeoutInterval: 2000,
        maxReconnectAttempts: 1,

        automaticOpen: true,

        reconnectInterval: 1000,
        maxReconnectInterval: 30000,
        reconnectDecay: 1.5,
    };
    options = options || {};
    // Change settigns based on options
    for (var key in options) {
        this.settings[key] = options[key];
    }
    
    // Read-only properties
    this.url = url;
    this.reconnectAttempts = 0;
    /* The current state of the connection (one of CONNECTING, OPEN, CLOSING, CLOSED) */
    this.readyState = WebSocket.CONNECTING;

    // Private state variables
    var self = this;
    var ws;
    var forcedClose = false;
    var timedOut = false;
    var eventTarget = document.createElement('div');

    // "on*" properties as event handlers
    eventTarget.addEventListener('open',        function(event) { self.onopen(event); });
    eventTarget.addEventListener('close',       function(event) { self.onclose(event); });
    eventTarget.addEventListener('connecting',  function(event) { self.onconnecting(event); });
    // eventTarget.addEventListener('message',     function(event) { self.onmessage(event); });
    eventTarget.addEventListener('finished',       function(event) { self.onfinished(event); });
    eventTarget.addEventListener('error',       function(event) { self.onerror(event); });

    // Expose Event API
    this.addEventListener = eventTarget.addEventListener.bind(eventTarget);
    this.removeEventListener = eventTarget.removeEventListener.bind(eventTarget);
    this.dispatchEvent = eventTarget.dispatchEvent.bind(eventTarget);

    /**
     * This function generates an event that is compatible with standard
     * compliant browsers and IE9 - IE11
     *
     * This will prevent the error:
     * Object doesn't support this action
     *
     * http://stackoverflow.com/questions/19345392/why-arent-my-parameters-getting-passed-through-to-a-dispatched-event/19345563#19345563
     * @param s String The name that the event should use
     * @param args Object an optional object that the event will use
     */
    function generateEvent(s, args) {
        var evt = document.createEvent("CustomEvent");
        evt.initCustomEvent(s, false, false, args);
        return evt;
    };

    // Finally the bit that actually handles a socket
    this.open = function (reconnectAttempt) {
        ws = new WebSocket(self.url);
        
        eventTarget.dispatchEvent(generateEvent('connecting'));

        if (self.settings.debug)
            console.debug('PythonSocket', 'attempt-connect', self.url);

        var localws = ws;
        var timeout = setTimeout(function() {
            if (self.debug) console.debug('PythonSocket', 'connection-timeout', self.url);
            timedOut = true;
            localws.close();
            timedOut = false;
        }, self.settings.timeoutInterval);

        ws.onopen = function(event) {
            clearTimeout(timeout);
            if (self.settings.debug) console.debug('PythonSocket', 'onopen', self.url);
            self.readyState = WebSocket.OPEN;
            self.reconnectAttempts = 0;
            var e = generateEvent('open');
            e.isReconnect = reconnectAttempt;
            eventTarget.dispatchEvent(e);
        };

        // XXX Right now, we're set up to automatically reconnect to the socket
        ws.onclose = function(event) {
            clearTimeout(timeout);
            ws = null;
            if (forcedClose) {
                self.readyState = WebSocket.CLOSED;
                eventTarget.dispatchEvent(generateEvent('close'));
            } else {
                self.readyState = WebSocket.CONNECTING;
                var e = generateEvent('connecting');
                e.code = event.code;
                e.reason = event.reason;
                e.wasClean = event.wasClean;
                eventTarget.dispatchEvent(e);
                if (!reconnectAttempt && !timedOut) {
                    if (self.settings.debug) console.debug('PythonSocket', 'onclose', self.url);
                    eventTarget.dispatchEvent(generateEvent('close'));
                }

                var timeout = self.settings.reconnectInterval * 
                    Math.pow(self.settings.reconnectDecay, self.settings.reconnectAttempts);
                setTimeout(function() {
                    self.reconnectAttempts++;
                    self.open(true);
                }, timeout > self.settings.maxReconnectInterval ? self.settings.maxReconnectInterval : timeout);
            }
        };

        // Dispatch all the messages!
        ws.onmessage = function(event) { 
            var msg = JSON.parse(event.data);
            switch (msg.type) {
                case "register":
                    if (msg.registered) {
                        self.uniqueId = msg.uniqueID;
                    } else {
                        var e = generateEvent('error');
                        e.msg = 'Registrations attempt unsuccessful';
                        eventTarget.dispatchEvent(e);
                    }
                    break;
                case "eval":
                    // XXX Not implemented
                    break;
                case "out":
                    var e = generateEvent('stdout');
                    e.content = msg.content;
                    eventTarget.dispatchEvent(e);
                    break;
                case "err":
                    var e = generateEvent('stderr');
                    e.content = msg.content;
                    eventTarget.dispatchEvent(e);
                    break;
                case "input-req":
                    var e = generateEvent('inputreq');
                    eventTarget.dispatchEvent(e);
                    break;
                case "io-result":
                    var e = generateEvent('ioresult');
                    e.data = { 
                        success: msg.success,
                        results: msg.results,
                    };
                    eventTarget.dispatchEvent(e);
                    break;
                case "f-result":
                    var e = generateEvent('fresult');
                    e.data = { 
                        success: msg.success,
                        results: msg.results,
                        traceback: msg.traceback,
                    };
                    eventTarget.dispatchEvent(e);
                    break;
                case "start":
                    // XXX Not implemented
                    break;
                case "stop":
                    var e = generateEvent('finished');
                    e.data = {
                        error: msg.error,
                        msg: msg.msg
                    }
                    eventTarget.dispatchEvent(e);
                    break;
                case "error":
                    var e = generateEvent('error');
                    e.msg = msg.msg;
                    eventTarget.dispatchEvent(e);
                    break;
                default:
                    console.debug('PythonSocket', 'unknown message', msg)
            }
            var e = generateEvent('message');
            e.data = event.data;
            eventTarget.dispatchEvent(e);
        };

        ws.onerror = function(event) {
            if (self.settings.debug) console.debug('PythonSocket', 'onmessage', self.url, event);
            eventTarget.dispatchEvent(generateEvent('error'));
        };
    }

    if (this.settings.automaticOpen === true)
        this.open(false);

    this.send = function(data) {
        if (ws) {
            if (self.settings.debug)
                console.debug('PythonSocket', 'send', self.url, data);
            return ws.send(data);
        } else {
            throw 'INVALID_STATE_ERR : Pausing to reconnect python socket';
        }
    };

    this.close = function(code, reason) {
        if (typeof code == 'undefined') {
            code = 1000;
        }
        forcedClose = true;
        if (ws) ws.close(code, reason);
    };

    this.refresh = function() {
        if (ws) 
            ws.close();
    };
};


/**
 * An event listener to be called when the WebSocket connection's readyState changes to OPEN;
 * this indicates that the connection is ready to send and receive data.
 */
PythonSocket.prototype.onopen = function(event) {};
/** An event listener to be called when the WebSocket connection's readyState changes to CLOSED. */
PythonSocket.prototype.onclose = function(event) {};
/** An event listener to be called when a connection begins being attempted. */
PythonSocket.prototype.onconnecting = function(event) {};
/** An event listener to be called when a message is received from the server. */
//PythonSocket.prototype.onmessage = function(event) {};
/** An event listener to be called when an error occurs. */
PythonSocket.prototype.onerror = function(event) {};
PythonSocket.prototype.onfinished = function(event) {};


PythonSocket.prototype.initialize = function () {
    var data = { type: "init" }
    var msg = JSON.stringify(data);
    this.debugLog(msg);
    this.send(msg);
};

// TODO add handler for the response

PythonSocket.prototype.eval = function (code) {
    var data = {
            uniqueID: this.uniqueID,
            type: "eval",
            code: code
        };
    var msg = JSON.stringify(data);
    this.debugLog(msg);
    this.send(msg);
};

// TODO add handler for the response

PythonSocket.prototype.exec = function (code) {
    var data = {
            uniqueID: this.uniqueID,
            type: "exec",
            code: code
        };
    var msg = JSON.stringify(data);
    this.debugLog(msg);
    this.send(msg);
    this.output.innerHTML = ""
};

PythonSocket.prototype.sendInput = function (s) {
    var data = {
        uniqueID: this.uniqueID,
        type: 'input',
        content: s,
    };
    var msg = JSON.stringify(data);
    this.debugLog(msg);
    this.send(msg);
};

/* send a stdio test suite to the server
 *
 * Arguments:
 *  c    - code
 *  ins  - inputs
 *  outs - outputs
 */
PythonSocket.prototype.sendIotest = function (c, ins, outs) {
    var data = {
        uniqueID: this.uniqueID,
        type: 'iotest',
        code: c,
        inputs: ins,
        outputs: outs,
    };
    var msg = JSON.stringify(data);
    this.debugLog(msg);
    this.send(msg);
};

/* send a function test suite to the server
 *
 * Arguments:
 *  c    - code
 *  tc   - testCode: should be a bunch of assert statements
 */
PythonSocket.prototype.sendFtest = function (c, tc) {
    var data = {
        uniqueID: this.uniqueID,
        type: 'ftest',
        code: c,
        testCode: tc,
    };
    var msg = JSON.stringify(data);
    this.debugLog(msg);
    this.send(msg);
};

PythonSocket.prototype.sendInterrupt = function () {
    var data = {
        uniqueID: this.uniqueID,
        type: 'interrupt',
    };
    var msg = JSON.stringify(data);
    if (this.readyState == WebSocket.OPEN) {
        this.debugLog(msg);
        console.debug('pythonSocket', 'sending interrupt');
        this.send(msg);
    }
    else
        console.debug('pythonSocket', 'interrupt', 'socket closed');
};



// Emulate the setOutput method of the previous incarnation
PythonSocket.prototype.setOutput = function (element) {
    this.output = element;
    var ps = this;
    this.addEventListener('stdout' , function (event) {
        element.appendChild(document.createTextNode(event.content));
    });
    this.addEventListener('stderr' , function (event) {
        var e = document.createElement('span');
        e.className = 'error-text';
        e.appendChild(document.createTextNode(event.content));
        element.appendChild(e);
    });
    this.addEventListener('inputreq' , function (event) {
        var inputField = document.createElement('input');
        var currentFocus = document.activeElement;
        inputField.onkeypress = function (e) {
            if (e.keyCode ===13) {
                inputField.setAttribute('disabled','disabled');
                currentFocus.focus()
                ps.sendInput(inputField.value);
            }
        };
        inputField.className = 'user-input';
        element.appendChild(inputField);
        element.appendChild(document.createTextNode('\n'));
        inputField.focus();
    });
};


PythonSocket.prototype.setDebug = function (element) {
    this.debugOutput = element;
    this.addEventListener("message", function (event) {
        var msg = event.data;
        element.innerHTML += "\nRECEIVED MESSAGE:\n" + msg;
    });

    this.onopen = function (event) {
        element.innerHTML += "\nConnection established\n";
    };

    this.onclose = function (event) {
        element.innerHTML += "\nConnection closed\n";
    };
};

PythonSocket.prototype.debugLog = function (msg) {
    if (this.debugOutput) 
        this.debugOutput.innerHTML += "\nSENT MESSAGE:\n" + msg;
};
