(function () {
    angular.module('accordion', [])
    .directive('accordion', accordionGroupDirective)
    .directive('fold', foldDirective)
    ;


    /** accordion group directive.
     */
    function accordionGroupDirective () {
        return {
            restrict: 'C',
            transclude: false,
            scope: {expanded: '='},
            controller: ['$scope', '$element', function($scope, $element) {
                this.$scope = $scope;
                $scope.mv = this;

                var folds = $scope.folds = [];
                var tabs = $scope.tabs = [];

                var id = 0;
                this.addFold = function(fscope, felement, ftab) {
                    fscope.hide = !$scope.expanded;
                    fscope.tab = ftab;
                    folds.push(fscope);
                    tabs.push(ftab);
                    $element[0].appendChild(ftab[0]);
                    $element[0].appendChild(felement[0]);

                    ftab.attr('role', 'tab');
                    ftab.attr('tabindex', '0');
                    ftab.attr('aria-controls', 'fold'+id);
                    ftab.attr('aria-expanded', !fscope.hide);
                    felement.attr('role', 'tabpanel');
                    felement.attr('id', 'fold'+id);
                    
                    id++;

                    ftab[0].addEventListener('keydown', function (e) {
                        console.log(e);
                        var index = tabs.indexOf(ftab);
                        if (e.which >= 37 && e.which <= 40) {
                            if (e.which <= 38) {
                                // Left or up
                                var newIndex = Math.max(index-1, 0)
                            }
                            else {
                                // Right or down
                                var newIndex = Math.min(index+1, tabs.length-1)
                            }
                            angular.forEach(tabs, function(tab) {
                                tab.attr('aria-selected', 'false');
                            });
                            tabs[newIndex].attr('aria-selected', 'true');
                            tabs[newIndex][0].focus();
                        }
                    });
                };
            }],
            link: function (scope, ielement, iattre, ctrl, transclude) {
                ielement.attr('multiselectable','true')
                ielement.attr('role','tablist')
            }
        }
    }


    /** A directive for the actual panels in the accordion
     */
    function foldDirective () {
        return {
            restrict: 'C',
            require: '^accordion',
            transclude: true,
            scope: {
            },
            link: function (scope, ielement, attrs, accordionCtrl, transclude) {
                scope.id = "" + Math.round(performance.now()*1000);
                ielement.attr('id', scope.id);

                scope.$watch('hide', function(hide) {
                    if (hide) {
                        ielement.addClass('ng-hide');
                    }
                    else {
                        ielement.removeClass('ng-hide');
                    }
                });

                // Transclude here, so that there's no extra element created
                var transcludedContent, transclusionScope;
                transclude(function(tclone, tscope) {
                    transcludedContent = tclone;
                    transclusionScope = tscope;
                    ielement.append(tclone);

                    // move the header outside the element
                    var header = angular.element(ielement[0].getElementsByTagName('h2')[0]);
                    header.remove();
                    header[0].addEventListener('click', function () {
                        scope.hide = !scope.hide;
                        header.attr('aria-expanded', !scope.hide);
                        scope.$apply();
                    });
                    header[0].addEventListener('keypress', function (e) {
                        if (e.which == 32 || e.which == 13) {
                            scope.hide = !scope.hide;
                            header.attr('aria-expanded', !scope.hide);
                            scope.$apply();
                        }
                    });
                    accordionCtrl.addFold(scope, ielement, header);
                });

            },
        }
            
            
    }

})();
