'use strict';

var pythonPanelApp = angular.module('pythonPanelApp', ['ui.ace','flexDivider', 'snippetModule']);

/** The main controller for the python-panel
 *      - the ACE editor should be inserted with 
 *              <div ui-ace="aceOptions" ng-model="codeInput"></div>
 */
pythonPanelApp.controller('pythonPanelCtrl',['$scope', function ($scope) {
    $scope.mv = this;

    // this will point to the ace instance, so that functions can change its behaviour
    var _ace;
    $scope.codeInput = "";

    /* At some point, this should probably become a service, and
     * use ng-model instead of manipulating the output directly
     */
    var pythonSocket = ps;
    var codeOutput;

    /* listeners for code completion and on-server test results */
    pythonSocket.addEventListener('finished', function(e) {
        $scope.doneExec = true;
        $scope.$apply();        // Let angular know something has changed
    });

    /* Debug listener */
    pythonSocket.addEventListener('error', function(e) {
        console.debug('pythonSocket', e.msg);
    });
    /**/

    /* RunCode is like the workhorse:
     * run code and handle all the messages from the server
     */
    $scope.runCode = function () {
        $scope.doneExec = false;
        pythonSocket.exec($scope.codeInput);
        // make sure the focus is back on the editor
        _ace.focus();
    };

    /** stopCode sends the interrupt signal
     */
    $scope.stopCode = function () {
        pythonSocket.sendInterrupt();
    };

    $scope.loadSnippet = function () {
        if ($scope.snippet)
            $scope.codeInput = $scope.snippet.code;
    };

    /* Configuration for the code input box
     */
    // This version activates scope
    var runCode = function () {
        $scope.$apply($scope.runCode);
    }
    $scope.aceOptions = {
        mode: 'python3',
        theme: 'opencs',
        
        // This sets up the Ace component
        onLoad: function (aceInstance) {
            _ace = aceInstance;
            _ace.$blockScrolling = Infinity;
            _ace.commands.addCommand({
                    name: 'runCode',
                    bindKey: {win: 'Shift-Enter', mac: 'Shift-Enter'},
                    exec: runCode,
                    readOnly: true
                });
            // XXX For usability (keyboard navigation), tab-based commands must be removed
            _ace.commands.removeCommands(['indent', 'outdent']);
            _ace.focus();
        },
    };
    // The editor needs to be told to resize when its bounding box is resized
    $scope.$on('resize', function () {
        _ace.resize();
    });

    /* For binding an output to the python socket
     */
    $scope.addOutput = function (element) {
        pythonSocket.setOutput(element[0]);
        codeOutput = element[0];
    };
}])
.directive('codeOutput', function () {
    return {
        restrict: 'A',
        link: function(scope, element, attrs) {
            scope.addOutput(element);
        }
    };
})
;
