"""
Custom Markdown extension for OpenCS.

Included are the additional directives: 

+-------------------+-------------------------------+--------------------------------------------+
|purpose            |markdown                       |substitution                                |
+===================+===============================+============================================+
|first use of word  |``!f[word]``                   |<span class="first-use">word</span>         |
+-------------------+-------------------------------+--------------------------------------------+
|built-in           |``!b[foo]``                    |<span class="built-in">foo</span>           |
+-------------------+-------------------------------+--------------------------------------------+
|glossary           |``!g[term]``                   |<span glossary="term">term</span>           |
+-------------------+-------------------------------+--------------------------------------------+
|glossary           |``!g[display](term)``          |<span glossary="term">display</span>        |
+-------------------+-------------------------------+--------------------------------------------+
|hint               |``<!-- Hint - hint_text -->``  |None (passes hint_text through the markdown |
|                   |                               |parser, and adds it to the list of hints)   |
+-------------------+-------------------------------+--------------------------------------------+

There is also support for indicating the language of the code blocks. The html comment ``<!-- lang-[language] -->`` adds the class "lang-[language]" to the next ``<code>`` tag found.

For example::

    This is an html tag, <!-- lang-html -->`<div>`. This is a css rule,

    <!-- lang-css -->
        div {
          border: blue;
        }

Would translate to::

    <p>This is an html tag, <code class="lang-html>&lt;div&gt;</code>. This is a css rule,</p>
    
    <pre><code class="lang-css">div {
      border: blue;
    }</code></pre>

As a convenience, any occurrence of ``:(\w+):`` is substituted with ``<!-- lang-\1 -->``, thus ``:html:`<div>` `` would have the same effect.
"""

import re
import markdown as md


_p_md = [
    (re.compile(r'!g(?:loss)?\[([^\]]*?)\]\(([^\)]*?)\)'), r'<span glossary="\2">\1</span>'),
    (re.compile(r'!g(?:loss)?\[([^\]]*?)\]'), r'<span glossary="\1">\1</span>'),
    (re.compile(r'!d\[([^\]]*?)\]'), r'<span class="first-use">\1</span>'),
    (re.compile(r'!b\[([^\]]*?)\]'), r'<span class="built-in">\1</span>'),
    # This is for fancy code formatting
    (re.compile(r':(\w+):'), r'<!-- lang-\1 -->')
    ]
_p_lang = re.compile(r'<!--+\s*lang-(\w+)\s*--+>')
_p_code = re.compile(r'<code>')
def markdown(text):
    """ performs markdown -> html conversion with some additional directives
    """
    for p, r in _p_md:
        text = p.sub(r, text)

    text = _sub_figs(text)
    html = md.markdown(text, extensions=[
        'markdown.extensions.tables', 
        'markdown.extensions.admonition',
        ])
    lang_tags = _p_lang.split(html);
    final = []
    final.append(lang_tags[0])
    for i in range(1, len(lang_tags), 2):
        lang = lang_tags[i]
        html = lang_tags[i+1]
        final.append(_p_code.sub('<code class="lang-%s">' % lang, html, 1))
    return ''.join(final)

_p_fig = re.compile(r'!f([lr]?)\[([^\]]*?)\]\(([^\)]*?)\)(?:\{([^\}]*?)\})?')
def _sub_figs(text):
    m = _p_fig.search(text)
    while m:
        fig = '<figure{0}><img alt="{1}" src="{2}"/><figcaption>{3}</figcaption></figure>'
        if m.group(1) == 'l':
            lr = ' class="float-left"'
        elif m.group(1) == 'r':
            lr = ' class="float-right"'
        else:
            lr = ''
        caption = markdown(m.group(4) or '')

        text = text[:m.start()] + fig.format(lr, m.group(2), m.group(3), caption) + text[m.end():]
        m = _p_fig.search(text)
    return text
