from django.shortcuts import render
from django.template import RequestContext

from OpenCS.contexts import pysock_context

def pypanel(request):
    c = RequestContext(request, {}, [pysock_context])
    return render(request, 'pypanel.html', context_instance=c)
