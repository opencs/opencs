#!/usr/bin/env bash

# update the django DB and static files
python3 manage.py collectstatic --noinput --settings=OpenCS.settings.production
python3 manage.py migrate --noinput --settings=OpenCS.settings.production

# update the documentation
pushd docs
make html
mkdir -p /srv/opencs_vol/documentation/
cp -r _build/html/* /srv/opencs_vol/documentation/
popd
