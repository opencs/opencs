(function () {
    angular.module('panelLayout', [])
    .directive('layout', layoutDirective)
    .directive('pane', paneDirective)
    .directive('console', consoleDirective)
    ;

    var paneId = 0;


    /** A FlexDivider object.
     * it really isn't necessary to have a whole directive, linking function and all for this
     *
     * Arguments:
     *  element     - element to behave as divider
     *  prevElm     - previous pane in display
     *  nextElm     - next pane in display
     *  panes       - a list of all panes in the layout
     */
    function FlexDivider(element, prevElm, nextElm, panes) {
        var _this = this;
        // The components the divider controls
        this.prev = prevElm;
        this.next = nextElm;
        this.panes = panes;

        // The divider's DOM representation 
        // XXX private, because it should never change for sake of event handlers
        var elm = element;
        this.getElement = function() {
            return elm;
        };

        this.show = function() {
            elm.removeClass('ng-hide');
        };
        this.hide = function() {
            elm.addClass('ng-hide');
        };

        // Mouse Event variables
        var startY=0, pH=0, nH=0;

        // Set the CSS to make it look right
        // XXX .flex-divider should have flex:none; cursor:row-resize; and set height.
        // XXX prev and next need their CSS to include 'flex-basis: 0'
        elm.addClass('flex-divider');

        // Handle the mouse events
        elm.on('mousedown', function(event) {
            // Prevent default dragging of selected content
            event.preventDefault();
            
            // Make sure the numbers are all consistent
            updateSizes();
            pH = _this.prev[0].offsetHeight;
            nH = _this.next[0].offsetHeight;
            startY = event.clientY;
            document.addEventListener('mousemove', mousemove);
            document.addEventListener('mouseup', mouseup);
        });

        function mousemove(event) {
            var diff = event.clientY - startY;
            _this.prev.css({
                "-ms-flex-positive": Math.max(pH + diff,1),
                "-webkit-flex-grow": Math.max(pH + diff,1),
                "-moz-flex-grow": Math.max(pH + diff,1),
                "flex-grow": Math.max(pH + diff,1),
            });
            _this.next.css({
                "-ms-flex-positive": Math.max(nH - diff,1),
                "-webkit-flex-grow": Math.max(nH - diff,1),
                "-moz-flex-grow": Math.max(nH - diff,1),
                "flex-grow": Math.max(nH - diff,1),
            });
            // XXX We will need a different way to send the resize event message
            //scope.$emit('resize');
        }

        function mouseup(event) {
            document.removeEventListener('mousemove', mousemove);
            document.removeEventListener('mouseup', mouseup);
        }

        function updateSizes() {
            var h = [];
            for (var i in _this.panes)
                if ( !_this.panes[i].hide )
                    h[i] = _this.panes[i].element[0].offsetHeight;
            for (var i in _this.panes)
                if ( !_this.panes[i].hide )
                    _this.panes[i].element.css({
                        "-ms-flex-positive": h[i],
                        "-webkit-flex-grow": h[i],
                        "-moz-flex-grow": h[i],
                        "flex-grow": h[i],
                    });
        }
        // End of class definition
    }

    // In this version, all of the display is handled within the layout, instead of
    // using ngShow directives defined on the panes
    function layoutDirective () {
        return {
            restrict: 'A',
            require: '?^ngController',
            transclude: true,
            scope: {controls: '='},
            controller: ['$scope', '$element', function($scope, $element) {
                this.$scope = $scope;
                $scope.mv = this;

                // The layout can be 'stack' or 'tab'. 
                // Here, we default to stack
                $scope.layout = 'stack'
                

                var tmpDoc = document.createDocumentFragment();

                var panes = $scope.panes = [];
                var dividers = $scope.dividers = [];

                // Methods to add, remove, and transfer panes
                /** addPane binds the element to the pane's scope, and adds it to the end
                 *
                 * Primary use: linking panes in the initial compilation/linking step
                 */
                this.addPane = function(pscope, pelement) {
                    pscope.element = pelement;
                    pscope.id = 'pane-' + paneId++;
                    pelement.attr('id', pscope.id);
                    panes.push(pscope);
                    $element[0].getElementsByClassName('layout')[0].appendChild(pelement[0]);
                };
                /** insertPane inserts a paneScope into the layout
                 *
                 * arguments:
                 *  - pscope: the scope of the pane (with it's element bound to scope)
                 *  - index (optional): the new position for the pane (defaults to 0)
                 */
                this.insertPane = function(pscope, index) {
                    index = index || 0;
                    var layout = $element[0].getElementsByClassName('layout')[0];
                    if (index < panes.length - 1) {
                        panes.splice(index, 0, pscope);
                        layout.insertBefore(pscope.element[0], panes[index+1].element[0]);
                    }
                    else {
                        panes.push(pane);
                        layout.appendChild(pane.element[0]);
                    }
                };
                /** movePane transfers a pane from this layout to another
                 *
                 * arguments:
                 *  - ptitle: the title of the pane to move
                 *  - newLayout: the controller for the other layout
                 *  - newIndex (optional): the new position for the pane (defaults to 0)
                 */
                this.movePane = function(paneTitle, newLayout, newIndex) {
                    var i, index;
                    for (i = 0; i<panes.length; i++) {
                        if (panes[i].title === paneTitle) {
                            index = i;
                            break;
                        }
                    }
                    if (index !== undefined) {
                        pane = panes.splice(index, 1)[0];
                        newLayout.insertPane(pane, newIndex);
                        return pane;
                    }
                    else
                        console.log("pane not found");
                };
                /** removePane removes a pane from the layout
                 *
                 * arguments:
                 *  - paneTitle: the title of the pane to remove
                 *  - purge: if false (default), the element is moved into a temporary document
                 *           if true, the element is removed from the DOM
                 *           (note: purging can make the ACE editor unusable)
                 */
                this.removePane = function(paneTitle, purge) {
                    var i, index;
                    for (i = 0; i<panes.length; i++) {
                        if (panes[i].title === paneTitle) {
                            index = i;
                            break;
                        }
                    }
                    if (index !== undefined) {
                        pane = panes.splice(index, 1)[0];
                        // XXX if the element is actually removed, ace gets
                        // angry when it is inserted elsewhere
                        if (purge)
                            pane.element.remove();
                        else
                            tmpDoc.appendChild(pane.element[0]);
                        return pane;
                    }
                    else
                        console.log("pane not found");
                };

                /** hasPane checks to see if the pane exists
                 *
                 * arguments:
                 *  - panetitle: the title of the pane to move
                 *  - newLayout: the controller for the other layout
                 *  - newIndex (optional): the new position for the pane (defaults to 0)
                 */
                this.hasPane = function(paneTitle) {
                    var i;
                    for (i = 0; i<panes.length; i++) {
                        if (panes[i].title === paneTitle) {
                            return true;
                        }
                    }
                    return false;
                }

                /** resetPaneSizes makes the size match the set size of the pane
                 */
                this.resetPaneSizes = function() {
                    panes.forEach( function (pane) {
                        size = pane.size || "";
                        pane.element.css({
                            "-ms-flex-positive": size,
                            "-webkit-flex-grow": size,
                            "-moz-flex-grow": size,
                            "flex-grow": size,
                        });
                    });
                }

                $scope.select = function (pane) {
                    switch ($scope.layout) {
                        case 'tab':
                            angular.forEach(panes, function(pane) {
                                pane.hide = true;
                            });
                            pane.hide = false;
                            break;
                        case 'stack':
                            pane.hide = !pane.hide;
                            break;
                    }
                };

                $scope.keyHandler = function ($event, pane) {
                    var panes = $scope.getPanes();
                    var index = panes.indexOf(pane);
                    var tabs = $element[0].getElementsByClassName('tabs')[0].children;
                    if ($event.which >= 37 && $event.which <= 40) {
                        if ($event.which <= 38) {
                            // Left or up
                            var newIndex = Math.max(index-1, 0)
                        }
                        else {
                            // Right or down
                            var newIndex = Math.min(index+1, tabs.length-1)
                        }
                        angular.forEach(tabs, function(tab) {
                            tab.setAttribute('aria-selected', 'false');
                        });
                        tabs[newIndex].setAttribute('aria-selected', 'true');
                        tabs[newIndex].focus();
                        if ($scope.layout === 'tab')
                            $scope.select(panes[newIndex]);
                    }

                };

                // TODO add some memory of the whether things are hidden or not in
                // different views
                $scope.switchLayout = function() {
                    switch ($scope.layout) {
                        case 'stack':       
                            // switch to tabbed
                            firstShown = false;
                            angular.forEach(panes, function(pane) {
                                if (!pane.exclude) {
                                    pane.hide = true;
                                    firstShown = firstShown || !(pane.hide = false);
                                }
                            });
                            $scope.layout = 'tab';
                            break;
                        case 'tab':
                            // switch to stacked
                            angular.forEach(panes, function(pane) {
                                if (!pane.exclude)
                                    pane.hide = false;
                            });
                            $scope.layout = 'stack';
                            break;
                    }
                }; 
                $scope.inclusions = function(pane, index) {return !pane.exclude};
                $scope.getPanes = function() {
                    var ps = [];
                    for (var i in $scope.panes) {
                        if (!$scope.panes[i].exclude)
                            ps.push($scope.panes[i]);
                    }
                    return ps;
                };

                var shownPanes = [];
                function updateShownPanes() {
                    var result = [];
                    for (var i in panes) {
                        if (!panes[i].hide) result.push(panes[i]);
                    };
                    // The new result is checked with the old, so that $watch
                    // doesn't need to do deep object comparisons and copies
                    var same = result.length == shownPanes.length;
                    if (same) {
                        for (i in result) same = same && result[i] == shownPanes[i];
                    }
                    if (!same) shownPanes = result;
                    return shownPanes;
                }
                $scope.$watch(updateShownPanes, function (newPanes, oldPanes) {
                    for (var i=0; i < newPanes.length - 1; i++) {
                        // Create a new divider if it doesn't exist
                        if (!dividers[i]) {
                            var d = angular.element(document.createElement('div'));
                            dividers.push(new FlexDivider(d, newPanes[i].element, newPanes[i+1].element, newPanes));
                            newPanes[i].element.parent()[0].insertBefore(d[0], newPanes[i+1].element[0]);
                        }
                        else {
                            dividers[i].prev = newPanes[i].element;
                            dividers[i].next = newPanes[i+1].element;
                            dividers[i].panes = newPanes;
                            newPanes[i].element.parent()[0].insertBefore(dividers[i].getElement()[0], newPanes[i+1].element[0]);
                        }
                        dividers[i].show();
                    }
                    for (; i < dividers.length; i++) {
                        dividers[i].hide();
                    }
                    if ($scope.getPanes().length === 0) 
                        $element.addClass('ng-hide');
                    else
                        $element.removeClass('ng-hide');
                });
            }],
            link: function (scope, ielement, iattr, parentCtrl) {
                if (parentCtrl) {
                    parentCtrl.addLayout(scope);
                }
                else
                    console.log('no controller found');
            },
            template: '<div class="controls" ng-show="controls"><button style="position:absolute; right: 0;top:0.2rem; font-size:.9em;" ng-click="switchLayout()" ng-show="getPanes().length > 1" class="toggle" ng-class="{inactive: layout==\'stack\'}" aria-pressed="{{layout==\'tab\'}}">Tab view</button><div class="tabs" role="tablist" aria-multiselectible="{{layout==\'stack\'}}"><button ng-repeat="pane in panes | filter: inclusions" ng-click="select(pane)" ng-keydown="keyHandler($event, pane)" class="toggle" ng-class="{inactive: pane.hide}" role="tab" aria-controls="{{pane.id}}" aria-expanded="{{!pane.hide}}" tabindex="{{pane.hide && layout==\'tab\' ? -1 : 0 }}">{{pane.title || ("Pane " + ($index+1))}}</button></div></div> <div class="layout" ng-transclude> </div>'
        }
    }


    function paneDirective () {
        return {
            restrict: 'A',
            require: '^layout',
            transclude: true,
            scope: {
                title: "=pane",
                exclude: "=",
                hide: "=",
                size: "="
            },
            link: function (scope, element, attrs, layoutCtrl, transclude) {
                layoutCtrl.addPane(scope, element);

                scope.$watch('exclude', function(exclude) {
                    if (exclude)
                        scope.hide = true;
                });

                scope.$watch('hide', function(hide) {
                    if (hide) {
                        element.addClass('ng-hide');
                    }
                    else {
                        element.removeClass('ng-hide');
                    }
                });

                scope.$watch('size', function(size) {
                    element.css({
                        "-ms-flex-positive": size,
                        "-webkit-flex-grow": size,
                        "-moz-flex-grow": size,
                        "flex-grow": size,
                    });
                }, true);

                // Transclude here, so that there's no extra element created
                var transcludedContent, transclusionScope;
                transclude(function(clone, scope) {
                    element.append(clone);
                    transcludedContent = clone;
                    transclusionScope = scope;
                });

            },
        }
            
            
    }

    function consoleDirective () {
        return {
            restrict: 'A',
            link: function (scope, out) {
                console.log('linking to ', out);
                window.onmessage = function (e) {
                    out[0].appendChild(document.createTextNode(e.data));
                }
            }
        }
    }
})();
