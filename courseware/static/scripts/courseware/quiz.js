angular.module('quizApp', ['uwSlideShow', 'glossaryModule'])
// The Question is really just a slide!
.directive('uwQuestion', function() {
    return {
        require: '^uwSlideShow',
        restrict: 'EA',
        replace: true,
        scope: {
            question: '='
        },
        controller: ['$scope', '$sce', function ($scope, $sce) {
            // Some initial values
            $scope.answer = [];
            $scope.hints = [];

            $scope.success = function () {
                return $scope.correct;
            };

            $scope.fail = function () {
                return $scope.incorrect;
            };

            /** Create the object that gets saved to sessionStorage
             */
            var properties = ['answer', 'correct', 'incorrect'];
            $scope.save = function() {
                var data = {};
                for (var i in properties) 
                    data[properties[i]] = $scope[properties[i]];
                storage.saveSlide(moduleId, $scope.question.id, data)
            };
            $scope.load = function() {
                var data = storage.loadSlide(moduleId, $scope.question.id);
                for (var i in properties)
                    $scope[properties[i]] = data[properties[i]];
                // XXX it's easier to just recheck the answer than to determine
                // the hints over again
                if ($scope.correct || $scope.incorrect) 
                    checkAnswer();
            };

            /** Helper function to remove any duplicates from a list
             */
            var unique = function(list) {
                var result = [];
                for (var i in list) {
                    if (result.indexOf(list[i]) < 0)
                        result.push(list[i]);
                }
                return result;
            }

            function checkAnswer () {
                switch ($scope.question.singleAnswer) {
                    case "1":   // Only one acceptible correct answer
                        var answer = parseInt($scope.answer, 10);
                        $scope.hints = [];
                        if (answer>=0 && answer < $scope.question.choices.length) {
                            var choice = $scope.question.choices[answer];
                            $scope.correct  = choice.isCorrect;
                            $scope.incorrect = !$scope.correct;
                            if ($scope.correct && choice.explanation) 
                                $scope.hints.push(choice.explanation);
                            if ($scope.incorrect && choice.hint) 
                                $scope.hints.push(choice.hint);

                        }
                        break;
                    case "M":   // There can be more than one answer selected
                        var isCorrect = true;
                        var missingCorrect = false;
                        var hints = [];
                        var choices = $scope.question.choices;
                        for (var i in choices) {
                            correct = (!!$scope.answer[i]) === choices[i].isCorrect;
                            missingCorrect = missingCorrect || (!correct && choices[i].isCorrect);
                            isCorrect = isCorrect && correct;
                            if (correct && choices[i].explanation) 
                                hints.push(choices[i].explanation);
                            if (!correct && choices[i].hint) 
                                hints.push(choices[i].hint);
                        }
                        if (missingCorrect)
                            hints.push("You missed at least one correct option.");
                        // XXX With all the hints and explanations mixed, we don't need them to be unique any more
                        // $scope.hints = unique(hints);
                        $scope.hints = (hints);
                        $scope.correct = isCorrect;
                        $scope.incorrect = !isCorrect;
                        break;
                    case "S":   // Short-form (text) answer
                        var answer = $scope.answer;
                        console.log(answer);
                        var isCorrect = false;
                        $scope.hints = [];
                        $scope.question.choices.forEach( function (choice) {
                            // XXX Hints and explanations are displayed for all potential answers
                            isCorrect = isCorrect || (choice.text === answer);
                            if (choice.hint && !isCorrect ) $scope.hints.push(choice.hint);
                            if (choice.explanation && isCorrect) $scope.hints.push(choice.explanation);
                        });
                        $scope.correct = isCorrect;
                        $scope.incorrect = !isCorrect;
                        break;
                    default:
                        console.log("unrecognized question type");
                }
            }

            $scope.checkAnswer = function () {
                checkAnswer();
                $scope.$emit('attempt');
            };

            $scope.trust = $sce.trustAsHtml;
        }],
        link: function(scope, element, attrs, progressCtrl) {
            progressCtrl.addSlide(scope);
        },
        templateUrl: '/static/scripts/courseware/templates/uw-question.html'
    };
})
/* A directive to create the combo list without making additional scopes
 */
.directive('comboList', ['$compile', '$parse', function($compile, $parse) {

    return {
        restrict: 'A',
        link: function(scope, element, attrs) {
            var createDOM = function () {
                var choices = scope.$eval(attrs.comboList);
                var singleAnswer = scope.$eval(attrs.singleAnswer);

                if (singleAnswer === "S") {
                    var html = '<input ng-model="' + attrs.value + '" tabindex="100"></input>';
                    element.append($compile(html)(scope));
                }
                else {      // XXX We're assuming that anything that isn't short-form is single or multi
                    singleAnswer = singleAnswer === "1";
                    for (var i=0; i<choices.length; i++) {
                        var html = '<li><input id="{{question.id}}-'+i+'" type="';
                        html += singleAnswer? 'radio' : 'checkbox';
                        html += '" ng-model="' + attrs.value;
                        html += singleAnswer? '" name="'+attrs.value+ '" value="'+i+'"' : '['+i+']"';
                        html += 'tabindex="10' + i + '"';
                        html += '><label for="{{question.id}}-'+i+'">' + choices[i].text + '</label></li>';
                        element.append($compile(html)(scope));
                    }
                }
            };
            // TODO: add all the necessary watches
            scope.$watch(attrs.comboList, createDOM);
        }
    }
}])
;
