(function SlideShowModule () {

    /** SlideShowController handles most of the logic of slide transitions
     *
     * This controller can be used for many multi-step pages such as
     * - explorations
     * - quizzes
     * - slideshows
     */
    function SlideShowController($scope) {
        var slides = $scope.slides = [];
        var curIndex = 0;
        // -1 if first, 0 if middle, 1 if last slide
        $scope.firstLast = -1;

        var checkComplete = function () {
            slides[curIndex].save()
            var i;
            var complete = true;
            for (i in slides) {
                complete = complete && (slides[i].success() || slides[i].fail());
            }
            if (complete) {
                // XXX storage should become a service at some point
                // XXX moduleId needs to be set (globally?) in template
                storage.setModuleCompleted(moduleId);
            }
        }

        /** Updates the current URL with which slide we are on
         */
        var updateUrl = function(index) {
            window.history.replaceState({slide: index}, "Slide change", './#'+(index+1));
        }

        /** Handle the general logic of switching slides
         */
        var switchTo = function(index) {
            slides[curIndex].save()
            if (!index && index !== 0)
                return 'invalid index';
            curIndex = Math.min(slides.length - 1, Math.max(0, index));
            if (curIndex == 0)
                $scope.firstLast = -1;
            else if (curIndex == slides.length - 1) 
                $scope.firstLast = 1;
            else
                $scope.firstLast = 0;
            angular.forEach(slides, function(slide) {
                slide.selected = false;
            });

            var slide = slides[curIndex];
            slide.selected = true;
            slide.read = true;
            updateUrl(curIndex);

            $scope.$emit('changeSlide', slide);
        };
        window.onpopstate = function (d) { 
            // This is for the case when a user changes the hash manually
            switchTo(parseInt(location.hash.slice(1))-1);
            $scope.$apply();
        }

        /* Slide navigation functions
         * select:      jumps
         * nextSlide:   goes forward
         * prevSlide:   goes back
         */
        $scope.select = function(slide, firstCommit) {
            var index = slides.indexOf(slide);
            switchTo(index);
        };

        $scope.prevStep = function() {
            checkComplete();
            document.location.href = '../' + ($scope.prevModule || '');
        };

        $scope.nextStep = function() {
            checkComplete();
            document.location.href = '../' + ($scope.nextModule || '');
        };
        
        $scope.prevSlide = function() {
            var index = curIndex - 1;
            if (index < 0) index = 0;
            switchTo(index);
        }
        
        $scope.nextSlide = function() {
            slides[curIndex].selected = false;
            var index = curIndex + 1;
            if (index >= slides.length && slides.length>0 ) index = slides.length - 1;
            switchTo(index);
        }

        /* Message Handlers for outside control
         */
        $scope.$on('nextSlide', function () {
            $scope.nextSlide();
        });
        $scope.$on('attempt', function () {
            checkComplete();
        });
        
        /* Provides an interface for the slide to add itself to this scope
         */
        this.addSlide = function(slide) {
            slides.push(slide);
            slide.load();
        };

        /** After loading all the slides, if there is a 'check-last-slide' directive,
         * then this function will be called to choose the correct slide.
         */
        this.doneLoading = function() {
            var hash = location.hash;
            if (hash) {
                var index = parseInt(hash.slice(1)) -1;
                switchTo(index);
            } 
            else {
                var index = 0;
                switchTo(index);
            }
        };
    }
    SlideShowController.$inject = ['$scope'];

    /** The actual slideshow directive
     *
     * Most logic is handled by SlideShowController
     */
    function slideShowDirective () {
        return {
            restrict: 'EA',
            transclude: true,
            scope: {prevModule: '=previous',
                nextModule: '=next'
            },
            controller: SlideShowController,
            link: function(scope) {
                scope.prevText = scope.prevModule ? "Previous step" : "Back";
                scope.nextText = scope.nextModule ? "Next step" : "Back";
            },
            templateUrl: '/static/scripts/courseware/templates/uw-slide-show.html'
        };
    }

    /** The last slide directive is necessary for telling the slideshow when
     * all slides have been loaded
     *
     * It is used alongside an `ngRepeat` directive and `uwSlide` (or equivalent)
     */
    function lastSlideDirective () {
        return {
            restrict: 'A',
            require: '^uwSlideShow',
            link: function (scope, ielement, iattr, slideShowCtrl) {
                if (scope.$last)
                    slideShowCtrl.doneLoading(scope);
            }
        }
    }


    /** The slide directive is an example, and used in lecture slides.
     *
     * Any type of slide used in an exploration or quiz must implement at least
     * this much behaviour.
     */
    function slideDirective() {
        return {
            require: '^uwSlideShow',
            restrict: 'A',
            transclude: true,
            scope: {slide: '=uwSlide'},
            controller: ['$scope', function($scope) {
                // Determine the status of the slide
                $scope.success = function () {
                    return $scope.read;
                }
                $scope.fail = function () {
                    return false;
                }

                // Which properties get saved
                properties = ['read'];
                $scope.save = function() {
                    var data = {};
                    for (var i in properties) 
                        data[properties[i]] = $scope[properties[i]];
                    storage.saveSlide(moduleId, $scope.slide.id, data)
                };
                $scope.load = function() {
                    var data = storage.loadSlide(moduleId, $scope.slide.id);
                    for (var i in properties)
                        $scope[properties[i]] = data[properties[i]];
                };

                // XXX We need something to indicate the the slide has been seen
                $scope.$watch('selected', function (newValue) {
                    if (newValue)
                        $scope.$emit('attempt');
                });
            }],
            link: function(scope, element, attrs, slideShowCtrl) {
                slideShowCtrl.addSlide(scope);
            },
            template: '<div class="uw-slide" ng-show="selected">' +
                      '<div bind-html-compile="slide.text"></div></div>'
        };
    }

    // Finally, definition of the angular module
    angular.module('uwSlideShow', ['glossaryModule'])
    .directive('uwSlideShow', slideShowDirective)
    .directive('checkLastSlide', lastSlideDirective)
    .directive('uwSlide', slideDirective)
    ;

})();
