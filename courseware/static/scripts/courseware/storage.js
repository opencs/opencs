var storage;
(function (storage) {
    /* Storage setup for courses:
     * 
     * Units are the most complicated, because they can become complete at any
     * time, as such there are 2 related keys:
     *
     * - unit-{{id}}-modules: list of module id's in the unit
     * - unit-{{id}}-complete: set to true if the unit is complete (undefined otherwise)
     *
     * modules only have one associated key:
     *
     * - module{{id}}-complete: whether the module is complete
     *
     * The way it works is that getUnitCompleted will look for
     * 'unit-{{id}}-complete'. If that key is not found, then it will check all
     * internal modules for completion.
     */

    /** Get the completion state of a unit
     *
     * arguments:
     *  - unitId: the id of the unit to check
     *  - callback: a callback function taking one argument: the completion status of the unit
     */
    storage.getUnitCompleted = function(unitId, callback) {
        var key = 'unit-' + unitId;
        if (sessionStorage.getItem(key + '-complete')) {
            callback(true);
        }
        else {
            var modList = sessionStorage.getItem(key + '-modules');
            if (modList)
                checkUnit(unitId, JSON.parse(modList), callback);
            else
                storage.getModuleList(unitId, callback);
        }
    };

    /** Poll the api for a list of modules in the unit
     */
    storage.getModuleList = function(unitId, callback) {
        var oReq = new XMLHttpRequest();
        oReq.onload = reqListener.bind(oReq, callback);
        oReq.unitId = unitId;
        oReq.open("get", "/api/v1/module/?unit__id=" + unitId, true);
        oReq.send();
    };

    // This is just to handle the response to our api request
    function reqListener(callback) {
        var reply = JSON.parse(this.responseText);
        var modList = [];
        reply.objects.forEach( function (mod) {
            modList.push(mod.id);
        });
        sessionStorage.setItem('unit-' + this.unitId + '-modules', JSON.stringify(modList));
        checkUnit(this.unitId, modList, callback);
    }

    // The actual method that checks for completion.
    function checkUnit(unitId, modList, callback) {
        var complete = true;
        for (var i in modList) {
            complete = complete && storage.getModuleCompleted(modList[i]);
        }
        if (complete)
            sessionStorage.setItem('unit-' + unitId + '-complete', 'true');
        callback(complete);
    }

    /** Save the completion state of a module
     */
    storage.setModuleCompleted = function(moduleId, remove ) {
        var key = 'module-' + moduleId + '-complete';
        if (remove)
            sessionStorage.removeItem(key);
        else
            sessionStorage.setItem(key, 'true');
    };

    /** Get the completion state of a module
     */
    storage.getModuleCompleted = function(moduleId) {
        var key = 'module-' + moduleId + '-complete';
        return !!(sessionStorage.getItem(key));
    };

    /** Save certain properties of a slide in a slide-show
     */
    storage.saveSlide = function (moduleId, slideId, slideData) {
        var key = moduleId + '-' + slideId + '-status';
        sessionStorage.setItem(key, JSON.stringify(slideData));
    };

    /** retrieve all saved slides for a slide-show
     */
    storage.loadSlide = function (moduleId, slideId) {
        var key = moduleId + '-' + slideId + '-status';
        return JSON.parse(sessionStorage.getItem(key)) || [];
    };

})(storage || (storage = {}));
