(function () {
    'use strict';

    function webContainerService($resource) {
        return $resource('/web-container/:wid/');
    }
    webContainerService.$inject = ['$resource'];

    /* The main controller for an exploration page. A few things to note for use:
     *      - the ACE editor should be inserted with 
     *              <div ui-ace="aceOptions" ng-model="codeInput"></div>
     *      - can't think of anything else at the moment
     */
    function WebExploreCtrl($scope, containerService) {
        $scope.mv = this;
        var _this = this;

        $scope.cs = containerService;
        var webConsole, webPreview;

        // For the logic of hiding/excluding panes
        $scope.hide = {};
        $scope.exclude = {};
        $scope.inputSize = {html: 6, css: 6, js: 6};

        $scope.runText = 'Load page';

        this.layouts = [];
        this.addLayout = function(layoutScope) {
            _this.layouts.push(layoutScope.mv);
        };
        this.transferPane = function(paneTitle, oldLayoutIndex, newLayoutIndex, newIndex) {
            this.layouts[oldLayoutIndex].movePane(paneTitle, this.layouts[newLayoutIndex], newIndex);
        };

        var wrappers = [];
        this.addWrapper = function( welement ) {
            wrappers.push(welement);
        }
        function resizeWrappers ( size ) {
            if (wrappers[0]) {
                wrappers[0].css({
                    "-ms-flex-positive": size,
                    "-webkit-flex-grow": size,
                    "-moz-flex-grow": size,
                    "flex-grow": size,
                });
            }
        }

        // this will point to the ace instance, so that functions can change its behaviour
        var _ace = {};
        // the initialCode must be defined to access its members
        $scope.initialCode = {};
        $scope.codeInput = { html: "", css: "", js: "" };
        // And this stores the current slide's scope so that there are fewer callbacks
        var curSlide = {slide: {}, success:function(){return false;}};

        // For the checkmark
        $scope.success = function() {
            return curSlide.success();
        };
        $scope.loading = 0;

        /** This function is supposed to save the user's code.
         * Code will be saved if the code input is changed since last save/update.
         * initial code is saved after slide transitions, and on `restoreCode`
         * which is called on slide transitions and pressing the 'restore' button 
         * */
        $scope.saveCode = function (slideScope) {
            slideScope.savedCode = slideScope.savedCode || {};
            slideScope.hideCode = slideScope.hideCode || {};
            for (var i in slideScope.slide.codeBlocks) {
                var codeType = slideScope.slide.codeBlocks[i].filetype;

                if ($scope.initialCode[codeType] != $scope.codeInput[codeType]) {
                    slideScope.savedCode[codeType] = $scope.codeInput[codeType];
                }
                if ($scope.hide[codeType] !== undefined)
                    slideScope.hideCode[codeType] = $scope.hide[codeType];
            }
        };
        $scope.loadCode = function (slideScope) {
            // XXX Even though '$scope.savedCode' is defined in the ExpSlide controller,
            // 'slideScope.savedCode is undefined' errors were happening on slide transitions
            slideScope.savedCode = slideScope.savedCode || {};
            slideScope.hideCode = slideScope.hideCode || {};
            for (var i in slideScope.slide.codeBlocks) {
                var codeType = slideScope.slide.codeBlocks[i].filetype;
                
                if (slideScope.savedCode[codeType]) {
                    $scope.codeInput[codeType] = slideScope.savedCode[codeType];
                    $scope.initialCode[codeType] = slideScope.savedCode[codeType];
                }
                else {
                    $scope.mv.restoreCode(slideScope.slide.codeBlocks[i]);
                }
                if (slideScope.hideCode[codeType] === undefined)
                    $scope.hide[codeType] =  !slideScope.slide.codeBlocks[i].show;
                else
                    $scope.hide[codeType] =  slideScope.hideCode[i];
                // This sets the relative size of the various inputs.
                $scope.inputSize[codeType] = slideScope.slide.codeBlocks[i].size;
            }
            ['html', 'css', 'js'].forEach(function (codeType) {
                $scope.exclude[codeType] = !slideScope.slide.codeBlocks[codeType];
            });
            $scope.renderedSrc = '';
            //webConsole[0].innerHTML = "";
            // Slide-dependent layout changes
            $scope.hide['preview'] = !slideScope.slide.showRendered;
            $scope.hide['console'] = !slideScope.slide.showConsole;
            $scope.runText = slideScope.slide.showRendered ? 'Load page' : 'Run';
            // if only javascript is included, move the console to the right side
            if ($scope.exclude['html'] && $scope.exclude['css']) {
                _this.transferPane('js', 0, 1, 0);
                // XXX Setting the pane size to the value in the DB isn't working
                // so we're abandoning it for now (jladan)
            }
            else if (_this.layouts[1].hasPane('js')) {
                _this.transferPane('js', 1, 0, 0);
            }
            if ($scope.exclude['css'] && $scope.exclude['js']) {
                _this.transferPane('html', 0, 1, 0);
                // XXX Setting the pane size to the value in the DB isn't working
                // so we're abandoning it for now (jladan)
            }
            else if (_this.layouts[1].hasPane('html')) {
                _this.transferPane('html', 1, 0, 0);
            }
            // Make sure that the sizes of all panes are correct
            _this.layouts[0].resetPaneSizes();
            _this.layouts[1].resetPaneSizes();
            // Make the widths of the two panels as defined
            resizeWrappers(slideScope.slide.leftPaneSize);
        };
        /* Slide navigation event handlers
         * (when the slide changes in the 'slideshow', it emits this event)
         */
        $scope.$on('changeSlide', function(e, slideScope) {
            e.stopPropagation()
            $scope.saveCode(curSlide)
            // Now make any display updates as necessary (like setting codeInput)
            curSlide = slideScope;
            // XXX loadCode also takes care of restoring if one of the filetypes is unchanged
            $scope.loadCode(curSlide);
        });

        /** ExplorationsScope.restoreCode updates all code inputs for the current slide
         */
        $scope.restoreCode = function() {
            webConsole[0].innerHTML = "";
            $scope.renderedSrc = "";
            for (var i in curSlide.slide.codeBlocks) {
                _this.restoreCode(curSlide.slide.codeBlocks[i]);
            }
        };

        /** restoreCode handles all of the logic for initializing a code input
         * corresponding to a specific codeBlock object.
         *
         * It references codeInputs on previous slides for that codeType, and
         * replaces/keeps/updates the current input as required.
         */
        this.restoreCode = function(codeBlock) {
            var codeType = codeBlock.filetype;
            if (codeBlock.replace === 'r')
                $scope.codeInput[codeBlock.filetype] = codeBlock.code;
            else {
                // We go back through the slides until we find one that has a saved codeInput
                // or until we find a 'replace' property
                // TODO Convert this to the multi-source version
                var slides = $scope.$$childHead.slides;
                $scope.codeInput[codeType] = "";
                var changed = false;
                var i = slides.indexOf(curSlide) -1;
                for (; i>=0; i--) {
                    var slideBlock = slides[i].slide.codeBlocks[codeType];
                    if (slideBlock.replace === 'r') {
                        $scope.codeInput[codeType] = slideBlock.code;
                        changed = true;
                    }
                    $scope.codeInput[codeType] = slides[i].savedCode[codeType] || $scope.codeInput[codeType];
                    if (changed || $scope.codeInput[codeType])
                        break;
                }
                if (curSlide.slide.clear === 'p')
                    $scope.codeInput[codeType] = codeBlock.code + $scope.codeInput[codeType];
                if (curSlide.slide.clear === 'a')
                    $scope.codeInput[codeType] += codeBlock.code;

            }
            $scope.initialCode[codeType] = $scope.codeInput[codeType];
        };

        /* RunCode sends the request for code execution to the server
         */
        $scope.runCode = function () {
            // clear the console log buffer
            window.consoleBuffer = [];
            $scope.loading = 0;
            // collect all the file contents
            var submission = {files: [], addLog: curSlide.slide.showConsole};
            for (var i in curSlide.slide.codeBlocks) {
                var f = {};
                f.content = $scope.codeInput[i];
                f.name = curSlide.slide.codeBlocks[i].filename;
                submission.files.push(f);
            }

            // delete the previous rendering if it exists
            if ($scope.renderInfo) {
                containerService.delete({wid: $scope.renderInfo.id});
            }
            // Just to show something happened
            $scope.loading = 10;

            // now submit it and update the iframe src
            $scope.renderInfo = containerService.save({}, submission, function() {
                webConsole[0].innerHTML = "";
                $scope.renderedSrc = $scope.renderInfo.url;
                if (!!$scope.preview && !$scope.preview.closed)
                    $scope.popout();
                curSlide.doneExec = true;
                curSlide.cleanTest = true;
                $scope.$broadcast('attempt');
                $scope.loading += 40;
            });


            // make sure the focus is back on the editor
            // XXX Don't know which input to focus on
            //_ace.focus();
        };

        $scope.popout = function () {
            var frameWindow = webPreview[0].contentWindow;
            var specs = 'width=' + frameWindow.innerWidth + ', height=' + frameWindow.innerHeight;
            specs += ', top=' + (frameWindow.screenTop + webPreview[0].offsetTop) + ', left=' +(frameWindow.screenLeft + webPreview[0].offsetLeft);
            console.log(specs);
            $scope.preview = window.open($scope.renderedSrc, 'preview', specs)
        }


        this.addPreview = function (element) {
            webPreview = element;
            // For indicating the final load
            webPreview[0].addEventListener('load', function () {
                $scope.loading = Math.min(100, $scope.loading + 50);
                if ($scope.renderedSrc === '')
                    $scope.loading = 0;
                $scope.$apply();
            });
        };
        


        /* Configuration for the code input box
         */
        // TODO Decide which page element gets primary focus
        $scope.aceOptions = {}
        $scope.aceOptions.html = {
            mode: 'html',
            theme: 'opencs',
            // This sets up the Ace component
            onLoad: function (aceInstance) {
                _ace['html'] = aceInstance;
                aceInstance.$blockScrolling = Infinity;
                // XXX For usability (keyboard navigation), tab-based commands must be removed
                aceInstance.commands.removeCommands(['indent', 'outdent']);
            },
        };
        $scope.aceOptions.css = {
            mode: 'css',
            theme: 'opencs',
            // This sets up the Ace component
            onLoad: function (aceInstance) {
                _ace['css'] = aceInstance;
                aceInstance.$blockScrolling = Infinity;
                // XXX For usability (keyboard navigation), tab-based commands must be removed
                aceInstance.commands.removeCommands(['indent', 'outdent']);
            },
        };
        $scope.aceOptions.js = {
            mode: 'javascript',
            theme: 'opencs',
            // This sets up the Ace component
            onLoad: function (aceInstance) {
                _ace['js'] = aceInstance;
                aceInstance.$blockScrolling = Infinity;
                // XXX For usability (keyboard navigation), tab-based commands must be removed
                aceInstance.commands.removeCommands(['indent', 'outdent']);
            },
        };
        // The editor needs to be told to resize when its bounding box is resized
        $scope.$on('resize', function () {
            for (var i in _ace) 
                _ace[i].resize();
        });

        this.addOutput = function (element) {
            webConsole = element;
        };

    }
    WebExploreCtrl.$inject = ['$scope', 'webContainerService'];

    function expSlide() {
        return {
            require: '^uwSlideShow',
            restrict: 'A',
            replace: true,
            scope: {
                slide: '=uwExpSlide'
            },
            controller: ['$scope', function ($scope) {
                // the initial and saved code must be defined on the slide's scope.
                $scope.savedCode = {};
                /* Slide Metadata functions
                 * Rather than directly set these values, they're based on multiple bools
                 * indicating the state of the process/tests
                 *
                 * Functions: 
                 *      success():    did the code work?
                 *      fail():       did the code not work?
                 */
                $scope.success = function () {
                    return $scope.doneExec && $scope.cleanTest;
                };

                $scope.fail = function () {
                    return $scope.doneExec && !$scope.cleanTest;
                };
                $scope.toggleHint = function () {
                    $scope.showHint = true;
                };
                // The hints are copied directly to scope, so that more can be added by the tests
                $scope.hints = $scope.slide.hints.slice();

                /** Create the object that gets saved to sessionStorage
                 */
                var properties = ['doneExec', 'cleanTest', 'savedCode', 'hideCode'];
                $scope.save = function() {
                    // Make sure the code is saved first
                    $scope.$parent.saveCode($scope);
                    var data = {};
                    for (var i in properties) 
                        data[properties[i]] = $scope[properties[i]];
                    storage.saveSlide(moduleId, $scope.slide.id, data)
                };
                $scope.load = function() {
                    var data = storage.loadSlide(moduleId, $scope.slide.id);
                    for (var i in properties)
                        $scope[properties[i]] = data[properties[i]];
                };

            }],
            link: function(scope, element, attrs, progressCtrl) {
                progressCtrl.addSlide(scope);
            },
            templateUrl: '/static/scripts/courseware/templates/uw-exp-slide.html'
        };
    }

    function feedbackDirective() {
        return {
            restrict: 'A',
            scope: {
                feedback: '=',
            },
            controller: ['$scope', function($scope) {
                $scope.$watch('feedback', function(h) { if (h) $scope.update(h);}, true);
            }],
            link: function (scope, element, attrs) {
                scope.element = element;
                scope.update = function (hints) {
                    var elm = element[0];
                    // Clear everything in the element, then add the header
                    while (elm.firstChild) {
                        elm.removeChild(elm.firstChild);
                    }
                    // Add the header
                    var tmp = document.createElement('h2');
                    tmp.innerHTML = 'Feedback on your program:';
                    elm.appendChild(tmp);
                    var list = document.createElement('ul');
                    elm.appendChild(list);
                    // Show all feedback
                    for (var i = 0; i < hints.length; i++) {
                        tmp = document.createElement('li');
                        tmp.innerHTML = hints[i];
                        list.appendChild(tmp);
                    }
                };
            }
        }
    }

    function hintsDirective($compile) {
        return {
            restrict: 'A',
            scope: {
                hints: '=',
            },
            controller: ['$scope', function($scope) {
                $scope.$watch('hints', function(h) { if (h) $scope.update(h);}, true);
                $scope.counter = 1;
                $scope.nextHint = function () {
                    $scope.counter++;
                    $scope.counter = Math.min($scope.counter, $scope.hints.length);
                    $scope.update($scope.hints);
                };

            }],
            link: function (scope, element, attrs) {
                scope.element = element;
                var nextButton = $compile('<button ng-click="nextHint()" ng-show="counter < hints.length">Next Hint</button>')(scope);
                scope.update = function (hints) {
                    var elm = element[0];
                    // Clear everything in the element, then add the header
                    while (elm.firstChild) {
                        elm.removeChild(elm.firstChild);
                    }
                    var tmp = document.createElement('h2');
                    if (hints.length == 1)
                        tmp.innerHTML = 'Hint:';
                    else
                        tmp.innerHTML = 'Hints:';
                    elm.appendChild(tmp);
                    var list = document.createElement('ul');
                    elm.appendChild(list);
                    // Show one hint at a time
                    for (var i = 0; i < scope.counter; i++) {
                        tmp = document.createElement('li');
                        tmp.innerHTML = hints[i];
                        list.appendChild(tmp);
                    }
                    // add the 'next hint' button if there are any left
                    elm.appendChild(nextButton[0]);
                };
            }
        }
    }
    hintsDirective.$inject = ['$compile'];

    function webSrcDirective () {
        return {
            restrict: 'A',
            require: '?^ngController',
            scope: {webSrc: "="},
            link: function (scope, ielement, iattr, exploreCtrl) {
                if (exploreCtrl)
                    exploreCtrl.addPreview(ielement);
                else
                    console.log('no parent controller for webSrc');
                scope.$watch('webSrc', function (newSrc, oldSrc) {
                    if (newSrc)
                        ielement.attr('src', newSrc);
                    else
                        ielement.attr('src', '');
                });
            }
        }
    }

    function consoleDirective ($window, $document) {
        // Takes a list of outputs (like console.log) and outputs them one after the other
        function formOutputList(argList) {
            var output = $document[0].createDocumentFragment();
            for (var i in argList) {
                output.appendChild( formOutput(argList[i]));
                output.appendChild($document[0].createTextNode( '\n' ));
            }
            return output;
        }

        var reArgs = /\([^)]*\)/;

        // Creates a span element to represent the argument
        function formOutput (arg) {
            var outElement = $document[0].createElement('span')
            var outText = '';
            switch (typeof arg) {
                case 'string':
                    // Functions from the other page are formatted to strings first
                    outElement.className = "wc-string";
                    outElement.appendChild(
                        $document[0].createTextNode(arg));
                    break;
                case 'function':
                    var fnString = arg.toString()
                    var pieces = fnString.split(' ');
                    var args = reArgs.exec(fnString);
                    outElement.className = "wc-function";
                    var tmp = $document[0].createElement('span')
                    tmp.className = 'wc-type';
                    tmp.appendChild($document[0].createTextNode(pieces[0] + ' '));
                    outElement.appendChild(tmp)
                    tmp = $document[0].createElement('span')
                    tmp.className = 'wc-name';
                    tmp.appendChild($document[0].createTextNode(pieces[1].split('(',1)[0]));
                    outElement.appendChild(tmp)
                    tmp = $document[0].createElement('span')
                    tmp.className = 'wc-args';
                    tmp.appendChild($document[0].createTextNode(args[0]));
                    outElement.appendChild(tmp)
                    tmp = $document[0].createElement('span')
                    tmp.className = 'wc-block';
                    tmp.appendChild($document[0].createTextNode(' ' +
                        fnString.slice(fnString.indexOf('{'))
                    ));
                    outElement.appendChild(tmp)
                    break;
                case 'undefined':
                    outElement.className = "wc-undefined";
                    outElement.appendChild(
                        $document[0].createTextNode('undefined'));
                    break;
                case 'object':
                    if (arg === null) {
                        outElement.className = "wc-null";
                        outElement.appendChild(
                            $document[0].createTextNode('null'));
                        break;
                    }
                    else if (arg.wcFunction) {
                        outElement.className = "wc-function";
                        var pieces = arg.wcFunction.split(' ')
                        var tmp = $document[0].createElement('span')
                        tmp.className = 'wc-type';
                        tmp.appendChild($document[0].createTextNode(pieces[0] + ' '));
                        outElement.appendChild(tmp)
                        tmp = $document[0].createElement('span')
                        tmp.className = 'wc-name';
                        tmp.appendChild($document[0].createTextNode(pieces[1] + ' '));
                        outElement.appendChild(tmp)
                        tmp = $document[0].createElement('span')
                        tmp.className = 'wc-block';
                        tmp.appendChild($document[0].createTextNode(
                            arg.wcFunction.slice(arg.wcFunction.indexOf('{'))
                        ));
                        outElement.appendChild(tmp)
                    }
                    else if (Array.isArray(arg)) {
                        outElement.className = 'wc-array';
                        var tmp = $document[0].createElement('span');
                        tmp.className = 'wc-type';
                        tmp.appendChild($document[0].createTextNode('Array'));
                        outElement.appendChild(tmp);
                        outElement.appendChild(
                            $document[0].createTextNode(' ['));
                        for (var i=0; i < arg.length ; i++) {
                            outElement.appendChild(formOutput(arg[i]));
                            outElement.appendChild(
                                $document[0].createTextNode(', '));
                        }
                        // get rid of that last comma
                        outElement.lastChild.remove();
                        outElement.appendChild(
                            $document[0].createTextNode(']'));
                    }
                    // This doesn't seem to work
                    else if (arg instanceof Error) {
                        outElement.className = 'wc-error';
                        var tmp = $document[0].createElement('span');
                        tmp.className = 'wc-type';
                        tmp.appendChild($document[0].createTextNode('Error'));
                        outElement.appendChild(tmp);
                        outElement.appendChild(
                            $document[0].createTextNode(' "' + arg.message + '"'));
                    }
                    else {
                        outElement.className = 'wc-object';
                        var tmp = $document[0].createElement('span');
                        tmp.className = 'wc-type';
                        tmp.appendChild($document[0].createTextNode('Object'));
                        outElement.appendChild(tmp);
                        outElement.appendChild(
                            $document[0].createTextNode(' {'));
                        for (var key in arg) {
                            tmp = $document[0].createElement('span');
                            tmp.className = 'wc-key';
                            tmp.appendChild($document[0].createTextNode(key));
                            outElement.appendChild(tmp);
                            outElement.appendChild(
                                $document[0].createTextNode(': '));
                            outElement.appendChild(formOutput(arg[key]));
                            outElement.appendChild(
                                $document[0].createTextNode(', '));
                        }
                        // get rid of that last comma
                        outElement.lastChild.remove();
                        outElement.appendChild(
                            $document[0].createTextNode('}'));
                    }
                    break;
                case 'number':
                    outElement.className = "wc-number";
                default:
                    outElement.appendChild(
                        $document[0].createTextNode(arg.toString()));
            }

            return outElement;
        }

        return {
            restrict: 'AC',
            require: '?^ngController',
            link: function (scope, out, iattr, exploreCtrl) {
                exploreCtrl.addOutput(out);
                window.consoleBuffer = [];
                window.onmessage = function (e) {
                    if (e.data.type == "log") {
                        var textNode = formOutputList(consoleBuffer.shift());
                        out[0].appendChild(textNode);
                    }
                }
            }
        }
    }
    consoleDirective.$inject = ['$window', '$document'];

    function progressDirective () {
        return {
            restrict: 'AC',
            scope: {
                value: '=',
                max: '=',
                min: '=',
            },
            link: function (scope, ielement) {
                ielement.attr('role', 'progressbar');
                ielement.attr('aria-valuemin', ''+ scope.min);
                ielement.attr('aria-valuemax', ''+ scope.max);

                var indicator = angular.element(document.createElement('div'));
                indicator.addClass('progress-indicator');
                var val = angular.element(document.createElement('div'));
                val.addClass('progress-val');
                ielement.append(indicator, val);

                scope.$watch('value', function(newVal, oldVal) {
                    // remove transitions, so that it gets cleared fast
                    if (oldVal > newVal)
                        indicator.css('transition', 'none');
                    else
                        indicator.css('transition', '');
                    indicator.css('width', newVal+'%');
                    ielement.attr('value-now', '' + newVal);
                    val.text( newVal )
                });
            }
        }
    }

    function slideWrapperDirective () {
        return {
            restrict: 'C',
            require: '?^ngController',
            link: function (scope, ielement, iattr, parentCtrl) {
                if (parentCtrl) {
                    parentCtrl.addWrapper(ielement);
                }
                else
                    console.log('no controller found for slide-wrapper');
            }
        }
    }

    var exploreApp = angular.module('explorationApp', ['uwSlideShow','ui.ace','panelLayout', 'glossaryModule']);
    exploreApp.config([ '$resourceProvider', function($resourceProvider) {
        // Don't strip trailing slashes from calculated URLs
        $resourceProvider.defaults.stripTrailingSlashes = false;
    }]);
    exploreApp.factory('webContainerService', webContainerService);
    exploreApp.controller('exploreCtrl', WebExploreCtrl);
    exploreApp.directive('uwExpSlide', expSlide);
    exploreApp.directive('hints', hintsDirective);
    exploreApp.directive('feedback', feedbackDirective);
    exploreApp.directive('webConsole', consoleDirective);
    exploreApp.directive('webSrc', webSrcDirective);
    exploreApp.directive('progressBar', progressDirective);
    exploreApp.directive('slideWrapper', slideWrapperDirective);

})();
