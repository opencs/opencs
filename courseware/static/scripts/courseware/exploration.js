'use strict';

var exploreApp = angular.module('explorationApp', ['uwSlideShow','ui.ace','panelLayout', 'glossaryModule']);

/* The main controller for an exploration page. A few things to note for use:
 *      - the ACE editor should be inserted with 
 *              <div ui-ace="aceOptions" ng-model="codeInput"></div>
 *      - can't think of anything else at the moment
 */
exploreApp.controller('exploreCtrl',['$scope', function ($scope) {
    $scope.mv = this;

    // this will point to the ace instance, so that functions can change its behaviour
    var _ace;
    $scope.codeInput = {};
    // And this stores the current slide's scope so that there are fewer callbacks
    var curSlide = {slide: {}, success:function(){return false;}};


    // For the checkmark
    $scope.success = function() {
        return curSlide.success();
    };

    /* At some point, this should probably become a service, and
     * use ng-model instead of manipulating the output directly
     */
    var pythonSocket = ps;
    var codeOutput;

    /* listeners for code completion and on-server test results */
    pythonSocket.addEventListener('finished', function(e) {
        if (e.data.error && !curSlide.slide.allowException) {
            curSlide.cleanTest = false;
        }
        curSlide.doneExec = true;
        $scope.$broadcast('attempt');
        $scope.$apply();        // Let angular know something has changed
    });
    pythonSocket.addEventListener('fresult', function(e) {
        curSlide.cleanTest = curSlide.cleanTest && e.data.success;
        if (e.data.traceback) {
            var tb = document.createElement('span');
            tb.className = 'error-text';
            tb.appendChild(document.createTextNode(e.data.traceback));
            codeOutput.appendChild(tb);
        }
        console.log(e.data.traceback);
        for (var i in e.data.results)
            if (e.data.results[i]) curSlide.feedback.push(e.data.results[i]);
        $scope.$broadcast('attempt');
        $scope.$apply();        // Let angular know something has changed
    });
    pythonSocket.addEventListener('ioresult', function(e) {
        curSlide.cleanTest = curSlide.cleanTest && e.data.success;
        console.log(e.data.results);
        if (! e.data.success) {
            // Set up the hints here
            for (var i in curSlide.slide.tests) {
                // find the test
                // XXX I really don't like that the processing of the test is
                // isolated from the test itself
                if (curSlide.slide.tests[i].testType = "stdio") {
                    var inputs = JSON.parse(curSlide.slide.tests[i].inputValues);
                    break;
                }
            }
            for (var i in e.data.results) {
                if (!e.data.results[i]) {
                    if (inputs[i] === "")
                        curSlide.feedback.push('Your program did not produce the correct output.');
                    else
                        curSlide.feedback.push('When run with the input "'+ inputs[i]+ '", your program did not produce the correct output.');
                }
            }
        }
        $scope.$broadcast('attempt');
        $scope.$apply();        // Let angular know something has changed
    });
    /* Debug listener */
    pythonSocket.addEventListener('error', function(e) {
        console.debug('pythonSocket', e.msg);
    });
    /**/

    /** This function is supposed to save the user's code.
     * Code will be saved if the code input is changed since last save/update.
     * initial code is saved after slide transitions, and on `restoreCode`
     * which is called on slide transitions and pressing the 'restore' button 
     * */
    $scope.saveCode = function (slideScope) {
        if ($scope.initialCode != $scope.codeInput['py']) {
            slideScope.savedCode = $scope.codeInput['py'];
        }
    };
    /* Slide navigation event handlers
     * (when the slide changes in the 'slideshow', it emits this event)
     */
    $scope.$on('changeSlide', function(e, slideScope) {
        e.stopPropagation()
        // XXX I imagine a race condition could happen with the messages
        // I should probably change this to use callbacks/promises
        pythonSocket.sendInterrupt()
        codeOutput.innerHTML = ""
        $scope.saveCode(curSlide)
        // XXX Now handled by the progress-nav:  saveSlide(curSlide);
        // Now make any display updates as necessary (like clearing codeInput)
        curSlide = slideScope;
        if (curSlide.savedCode) {
            $scope.codeInput['py'] = curSlide.savedCode;
            $scope.initialCode = curSlide.savedCode;
        }
        else {
            $scope.restoreCode();
        }
    });

    /* Go to the next slide.
     * XXX This was here for the 'continue' button, but that button has since been removed.
     */
    $scope.nextSlide = function () {
        $scope.$broadcast('nextSlide');
    };

    $scope.restoreCode = function() {
        if (curSlide.slide.clear === 'r')
            $scope.codeInput['py'] = curSlide.slide.code;
        else {
            // We go back through the slides until we find one that has a saved codeInput
            // or until we find a 'replace' property
            var slides = $scope.$$childHead.slides;
            $scope.codeInput['py'] = "";
            var changed = false;
            var i = slides.indexOf(curSlide) -1;
            for (; i>=0; i--) {
                if (slides[i].slide.clear === 'r') {
                    $scope.codeInput['py'] = slides[i].slide.code;
                    changed = true;
                }
                $scope.codeInput['py'] = slides[i].savedCode || $scope.codeInput['py'];
                if (changed || $scope.codeInput['py'])
                    break;
            }
            if (curSlide.slide.clear === 'p')
                $scope.codeInput['py'] = curSlide.slide.code + $scope.codeInput['py'];
            if (curSlide.slide.clear === 'a')
                $scope.codeInput['py'] += curSlide.slide.code;

        }
        $scope.initialCode = $scope.codeInput['py'];
        codeOutput.innerHTML = "";
    };

    /* I think there should be some way of displaying the errors for tests
     * but there's no spec for it.
     */
    var showError = function (s) {
        console.debug('code error:' + s);
    };
    /* RunCode is like the workhorse:
     * run code and handle all the messages from the server
     */
    $scope.runCode = function () {
        // Set the test result and code execution states
        curSlide.doneExec = false;
        curSlide.cleanTest = true;

        curSlide.feedback = [];
        pythonSocket.exec($scope.codeInput['py']);
        // run various tests!
        curSlide.slide.tests.forEach( function(test) {
            switch (test.testType) {
                case "cont":
                    var cs = JSON.parse(test.inputValues);
                    var os = JSON.parse(test.outputValues);
                    console.debug("exploration-contains", cs, os)
                    var success = true;
                    for (var i in cs) {
                        if ($scope.codeInput['py'].indexOf(cs[i]) < 0) {
                            curSlide.feedback.push("To be marked as correct, your program must use <code>"+cs[i]+"</code>.");
                            success = false;
                        }
                    }
                    for (var i in os) {
                        if ($scope.codeInput['py'].indexOf(os[i]) >= 0) {
                            curSlide.feedback.push("To be marked as correct, your program cannot use <code>"+os[i]+"</code>.");
                            success = false;
                        }
                    }
                    curSlide.cleanTest = curSlide.cleanTest && success;
                    break;
                case "stdio":
                    pythonSocket.sendIotest($scope.codeInput['py'],
                                            JSON.parse(test.inputValues), 
                                            JSON.parse(test.outputValues));
                    // XXX Return messages handled above
                    break;
                case "func":
                    pythonSocket.sendFtest($scope.codeInput['py'],
                                            test.inputValues);
                    // XXX Return messages handled above
                    break;
                case "ns":
                    var listener = function(e) {
                        var result = parseFloat(codeOutput.innerHTML);
                        var goal = parseFloat(test.outputValues);
                        if (Math.abs(result - goal) > 0.000001) {
                            curSlide.feedback.push('Your program did not calculate the correct number.');
                            curSlide.cleanTest = false;
                            $scope.$apply();
                        } else if (!result) {
                            curSlide.feedback.push('Your program did not print a number.');
                            curSlide.cleanTest = false;
                            $scope.$apply();
                        }
                        e.target.removeEventListener('finished', listener);
                    };
                    pythonSocket.addEventListener('finished', listener);
                    break;
                default:
                    console.debug('Exploration', 'Test type unrecognized');
            }
        });

        // make sure the focus is back on the editor
        _ace.focus();
    };

    /** stopCode sends the interrupt signal
     */
    $scope.stopCode = function () {
        pythonSocket.sendInterrupt();
    };

    /* Configuration for the code input box
     */
    // This version activates scope
    var runCode = function () {
        $scope.$apply($scope.runCode);
    }
    $scope.aceOptions = {
        mode: 'python3',
        theme: 'opencs',
        
        // This sets up the Ace component
        onLoad: function (aceInstance) {
            _ace = aceInstance;
            _ace.$blockScrolling = Infinity;
            _ace.commands.addCommand({
                    name: 'runCode',
                    bindKey: {win: 'Shift-Enter', mac: 'Shift-Enter'},
                    exec: runCode,
                    readOnly: true
                });
            // XXX For usability (keyboard navigation), tab-based commands must be removed
            _ace.commands.removeCommands(['indent', 'outdent']);
            _ace.focus();
        },
    };
    // The editor needs to be told to resize when its bounding box is resized
    $scope.$on('resize', function () {
        _ace.resize();
    });

    /* For binding an output to the python socket
     */
    $scope.addOutput = function (element) {
        pythonSocket.setOutput(element[0]);
        codeOutput = element[0];
    };
}])
.directive('uwExpSlide', function() {
    return {
        require: '^uwSlideShow',
        restrict: 'A',
        replace: true,
        scope: {
            slide: '='
        },
        controller: ['$scope', '$sce', function ($scope, $sce) {
            $scope.trust = $sce.trustAsHtml;
            /* Slide Metadata functions
             * Rather than directly set these values, they're based on multiple bools
             * indicating the state of the process/tests
             *
             * Functions: 
             *      success():    did the code work?
             *      fail():       did the code not work?
             */
            $scope.success = function () {
                return $scope.doneExec && $scope.cleanTest;
            };

            $scope.fail = function () {
                return $scope.doneExec && !$scope.cleanTest;
            };
            $scope.toggleHint = function () {
                $scope.showHint = true;
                console.debug("exp-ctrl", "show hint");
            };

            /** Create the object that gets saved to sessionStorage
             */
            var properties = ['doneExec', 'cleanTest', 'savedCode'];
            $scope.save = function() {
                // Make sure the code is saved first
                $scope.$parent.saveCode($scope);
                var data = {};
                for (var i in properties) 
                    data[properties[i]] = $scope[properties[i]];
                storage.saveSlide(moduleId, $scope.slide.id, data)
            };
            $scope.load = function() {
                var data = storage.loadSlide(moduleId, $scope.slide.id);
                for (var i in properties)
                    $scope[properties[i]] = data[properties[i]];
            };

        }],
        link: function(scope, element, attrs, progressCtrl) {
            progressCtrl.addSlide(scope);
        },
        templateUrl: '/static/scripts/courseware/templates/uw-exp-slide.html'
    };
})
.directive('feedback', function($compile) {
    return {
        restrict: 'A',
        scope: {
            feedback: '=',
        },
        controller: ['$scope', function($scope) {
            $scope.$watch('feedback', function(h) { if (h) $scope.update(h);}, true);
        }],
        link: function (scope, element, attrs) {
            scope.element = element;
            scope.update = function (hints) {
                var elm = element[0];
                // Clear everything in the element, then add the header
                while (elm.firstChild) {
                    elm.removeChild(elm.firstChild);
                }
                // Add the header
                var tmp = document.createElement('h2');
                tmp.innerHTML = 'Feedback on your program:';
                elm.appendChild(tmp);
                var list = document.createElement('ul');
                elm.appendChild(list);
                // Show all feedback
                for (var i = 0; i < hints.length; i++) {
                    tmp = document.createElement('li');
                    tmp.innerHTML = hints[i];
                    list.appendChild(tmp);
                }
            };
        }
    }
})
.directive('hints', ['$compile', function($compile) {
    return {
        restrict: 'A',
        scope: {
            hints: '=',
        },
        controller: ['$scope', function($scope) {
            $scope.$watch('hints', function(h) { if (h) $scope.update(h);}, true);
            $scope.counter = 1;
            $scope.nextHint = function () {
                $scope.counter++;
                $scope.counter = Math.min($scope.counter, $scope.hints.length);
                $scope.update($scope.hints);
            };

        }],
        link: function (scope, element, attrs) {
            scope.element = element;
            var nextButton = $compile('<button ng-click="nextHint()" ng-show="counter < hints.length">Next Hint</button>')(scope);
            scope.update = function (hints) {
                var elm = element[0];
                // Clear everything in the element, then add the header
                while (elm.firstChild) {
                    elm.removeChild(elm.firstChild);
                }
                var tmp = document.createElement('h2');
                if (hints.length == 1)
                    tmp.innerHTML = 'Hint:';
                else
                    tmp.innerHTML = 'Hints:';
                elm.appendChild(tmp);
                var list = document.createElement('ul');
                elm.appendChild(list);
                // Show one hint at a time
                for (var i = 0; i < scope.counter; i++) {
                    tmp = document.createElement('li');
                    tmp.innerHTML = hints[i];
                    list.appendChild(tmp);
                }
                // add the 'next hint' button if there are any left
                elm.appendChild(nextButton[0]);
            };
        }
    }
}])
.directive('codeOutput', function () {
    return {
        restrict: 'A',
        link: function(scope, element, attrs) {
            scope.addOutput(element);
        }
    };
})
;
