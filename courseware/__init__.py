
r"""
A django app for organizing the contents of a course. 

In a full set-up this app depends on the `quiz`, `exploration`, `slide-show`, and `glossary` modules.
"""
