from tastypie.resources import ModelResource
from tastypie import fields
from tastypie.constants import ALL, ALL_WITH_RELATIONS
from OpenCS.api import CamelCaseJSONSerializer

from .models import Course, Unit, Module

# XXX for generic foreign keys
from tastypie.contrib.contenttypes.fields import GenericForeignKeyField
from exploration.models import Exploration
from exploration.api import ExplorationResource
from glossary.models import Glossary
from glossary.api import GlossaryResource
from slides.models import SlideShow, VideoLesson
from slides.api import SlideShowResource, VideoLessonResource
from quiz.models import Quiz
from quiz.api import QuizResource

from django.conf.urls import url
class CourseResource(ModelResource):
    units = fields.ToManyField('courseware.api.UnitResource', attribute='unit_set')

    class Meta:
        resource_name = 'course'
        queryset = Course.objects.all()
        serializer = CamelCaseJSONSerializer(2)
        filtering = {
                'id': ALL,
                'slug': ALL,
                }

    def prepend_urls(self):
        return [
            url(r"^(?P<resource_name>%s)/(?P<slug>[\w\d-]+)/$" % self._meta.resource_name, self.wrap_view('dispatch_detail'), name="api_dispatch_detail"),
        ]

class UnitResource(ModelResource):
    modules = fields.ToManyField('courseware.api.ModuleResource', attribute='module_set', full=True, full_list=False)
    course = fields.ToOneField(CourseResource, attribute='course')

    class Meta:
        resource_name = 'unit'
        queryset = Unit.objects.all()
        serializer = CamelCaseJSONSerializer(2)
        excludes = ['height', 'width']
        filtering = {
                'id': ALL,
                'position': ALL,
                'course': ALL_WITH_RELATIONS,
                }

class ModuleResource(ModelResource):
    unit = fields.ToOneField(UnitResource, attribute='unit')
    content_object = GenericForeignKeyField({
        Exploration: ExplorationResource,
        VideoLesson: VideoLessonResource,
        Quiz: QuizResource,
        SlideShow: SlideShowResource,
        Glossary: GlossaryResource,
        }, 'content_object')

    class Meta:
        resource_name = 'module'
        queryset = Module.objects.all()
        serializer = CamelCaseJSONSerializer(2)
        filtering = {
                'id': ALL,
                'position': ALL,
                'unit': ALL_WITH_RELATIONS,
                }


def register_to_api(api):
    """ Helper function to register these resources with an Api instance"""
    resources = [CourseResource, 
                 UnitResource, 
                 ModuleResource,
                 ]
    for resource in resources:
        api.register(resource())
