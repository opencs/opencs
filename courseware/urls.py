from django.conf.urls import patterns, url

from courseware import views

urlpatterns = patterns('', 
        url(r'^$', views.index, name='index'),
        url(r'^(?P<course_slug>[-_\w]+)/$', views.course, name='course'),
        url(r'^(?P<course_slug>[-_\w]+)/(?P<unit_number>\d+)/$', views.unit, name='unit'),
        url(r'^(?P<course_slug>[-_\w]+)/(?P<unit_number>\d+)/(?P<module_number>\d+)/$', views.module, name='module'),
        url(r'^(?P<course_slug>[-_\w]+)/(?P<unit_number>\d+)/(?P<module_number>\d+)/transcript$', views.transcript, name='transcript'),
        url(r'^(?P<course_slug>[-_\w]+)/glossary/$', views.glossary, name='glossary'),
        url(r'^(?P<course_slug>[-_\w]+)/built-ins/$', views.builtins, name='builtins'),
        url(r'^(?P<course_slug>[-_\w]+)/videos/$', views.video_index, name='videos'),
        )
