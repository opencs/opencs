# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('courseware', '0003_module_icon'),
    ]

    operations = [
        migrations.AlterField(
            model_name='module',
            name='icon',
            field=models.CharField(blank=True, max_length=11, default='', choices=[('', 'Default'), ('quiz', 'Quiz'), ('exploration', 'Exploration'), ('videolesson', 'Video'), ('python', 'Python Video')]),
        ),
    ]
