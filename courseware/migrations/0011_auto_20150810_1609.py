# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('courseware', '0010_auto_20150806_2049'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='course',
            options={'ordering': ['position']},
        ),
        migrations.AddField(
            model_name='course',
            name='hidden',
            field=models.BooleanField(default=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='course',
            name='position',
            field=models.PositiveSmallIntegerField(verbose_name='Position', default=0),
            preserve_default=True,
        ),
    ]
