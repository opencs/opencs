# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('courseware', '0005_auto_20150319_2100'),
    ]

    operations = [
        migrations.AlterField(
            model_name='course',
            name='description',
            field=models.TextField(verbose_name='Course Description', blank=True),
        ),
        migrations.AlterField(
            model_name='module',
            name='description',
            field=models.TextField(blank=True),
        ),
        migrations.AlterField(
            model_name='unit',
            name='description',
            field=models.TextField(blank=True),
        ),
    ]
