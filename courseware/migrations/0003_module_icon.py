# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('courseware', '0002_auto_20150227_0059'),
    ]

    operations = [
        migrations.AddField(
            model_name='module',
            name='icon',
            field=models.CharField(max_length=11, choices=[('', 'Default'), ('quiz', 'Quiz'), ('exploration', 'Exploration'), ('videolesson', 'Video'), ('python', 'Python Video')], default=''),
            preserve_default=True,
        ),
    ]
