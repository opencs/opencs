# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('contenttypes', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Course',
            fields=[
                ('id', models.AutoField(auto_created=True, serialize=False, primary_key=True, verbose_name='ID')),
                ('title', models.CharField(max_length=128, verbose_name='Course Title')),
                ('slug', models.SlugField(verbose_name='Slug', unique=True)),
                ('description', models.TextField(verbose_name='Course Description')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Module',
            fields=[
                ('id', models.AutoField(auto_created=True, serialize=False, primary_key=True, verbose_name='ID')),
                ('object_id', models.PositiveIntegerField()),
                ('position', models.PositiveSmallIntegerField(verbose_name='Position')),
                ('title', models.CharField(max_length=128)),
                ('description', models.TextField()),
                ('content_type', models.ForeignKey(to='contenttypes.ContentType')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Unit',
            fields=[
                ('id', models.AutoField(auto_created=True, serialize=False, primary_key=True, verbose_name='ID')),
                ('position', models.PositiveSmallIntegerField(verbose_name='Position')),
                ('title', models.CharField(max_length=128)),
                ('description', models.TextField()),
                ('course', models.ForeignKey(to='courseware.Course')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.AddField(
            model_name='module',
            name='unit',
            field=models.ForeignKey(to='courseware.Unit'),
            preserve_default=True,
        ),
    ]
