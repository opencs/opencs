# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('glossary', '0004_auto_20150818_2158'),
        ('courseware', '0013_auto_20150817_1743'),
    ]

    operations = [
        migrations.AddField(
            model_name='course',
            name='builtins',
            field=models.ForeignKey(to='glossary.BuiltInCollection', on_delete=django.db.models.deletion.SET_NULL, null=True, blank=True),
            preserve_default=True,
        ),
    ]
