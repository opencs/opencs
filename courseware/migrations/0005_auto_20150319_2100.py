# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import courseware.models


class Migration(migrations.Migration):

    dependencies = [
        ('courseware', '0004_auto_20150319_1734'),
    ]

    operations = [
        migrations.AddField(
            model_name='unit',
            name='height',
            field=models.IntegerField(editable=False, default=180),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='unit',
            name='image',
            field=models.ImageField(height_field='height', width_field='width', default='images/unit/default.png', upload_to=courseware.models._upload_path),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='unit',
            name='width',
            field=models.IntegerField(editable=False, default=150),
            preserve_default=False,
        ),
    ]
