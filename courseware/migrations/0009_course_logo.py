# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import courseware.models


class Migration(migrations.Migration):

    dependencies = [
        ('courseware', '0008_course_glossary'),
    ]

    operations = [
        migrations.AddField(
            model_name='course',
            name='logo',
            field=models.ImageField(width_field='logo_width', height_field='logo_height', upload_to=courseware.models._course_upload_path, null=True, blank=True),
            preserve_default=True,
        ),
    ]
