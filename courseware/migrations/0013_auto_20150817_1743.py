# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('courseware', '0012_auto_20150811_1941'),
    ]

    operations = [
        migrations.AlterField(
            model_name='course',
            name='glossary',
            field=models.ForeignKey(to='glossary.Glossary', blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL),
        ),
    ]
