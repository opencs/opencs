# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('glossary', '0002_auto_20150410_1448'),
        ('courseware', '0007_auto_20150619_1506'),
    ]

    operations = [
        migrations.AddField(
            model_name='course',
            name='glossary',
            field=models.ForeignKey(null=True, blank=True, to='glossary.Glossary'),
            preserve_default=True,
        ),
    ]
