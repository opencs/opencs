# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('courseware', '0001_initial'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='module',
            options={'ordering': ['position']},
        ),
        migrations.AlterModelOptions(
            name='unit',
            options={'ordering': ['position']},
        ),
    ]
