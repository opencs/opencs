# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('courseware', '0011_auto_20150810_1609'),
    ]

    operations = [
        migrations.AddField(
            model_name='course',
            name='logo_height',
            field=models.IntegerField(default=0, editable=False),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='course',
            name='logo_width',
            field=models.IntegerField(default=0, editable=False),
            preserve_default=False,
        ),
    ]
