# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('courseware', '0006_auto_20150326_2104'),
    ]

    operations = [
        migrations.AlterField(
            model_name='module',
            name='icon',
            field=models.CharField(blank=True, choices=[('', 'Default'), ('quiz', 'Quiz'), ('demo', 'Demo'), ('exploration', 'Exploration'), ('slideshow', 'Slides'), ('videolesson', 'Video'), ('python', 'Python Video')], default='', max_length=11),
        ),
    ]
