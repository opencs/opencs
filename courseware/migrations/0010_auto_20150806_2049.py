# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('courseware', '0009_course_logo'),
    ]

    operations = [
        migrations.AlterField(
            model_name='module',
            name='icon',
            field=models.CharField(default='', blank=True, max_length=11, choices=[('', 'Default'), ('quiz', 'Quiz'), ('demo', 'Demo'), ('exploration', 'Exploration'), ('solution', 'Solution'), ('slideshow', 'Slides'), ('videolesson', 'Video'), ('python', 'Python Video')]),
        ),
    ]
