from django.shortcuts import render, get_object_or_404
from django.template import RequestContext
from django.http import Http404

from OpenCS.contexts import pysock_context

from courseware.models import Course, Unit, Module
from glossary.models import Glossary
from slides.models import VideoLesson

from quiz.api import serialize_quiz
from exploration.api import serialize_exploration
from slides.api import serialize_slideshow

from django.contrib.contenttypes.models import ContentType

def index(request):
    """ View for the index of all courses.

    (currently used as the full website's index)
    """
    course_list = Course.objects.all()
    context = RequestContext(request, {'course_list': course_list})
    return render(request, "courseware/index.html", context_instance=context)

def course(request, course_slug):
    """ View function to display a list of units in the course.
    """
    course = get_object_or_404(Course, slug=course_slug)
    unit_list = course.unit_set.order_by('position')
    context = RequestContext(request, {'course':course, 'unit_list':unit_list})
    return render(request, "courseware/course_index.html", context_instance=context)

def glossary(request, course_slug):
    """ View function to display the glossary for a given course
    """
    course = get_object_or_404(Course, slug=course_slug)
    g = course.glossary
    if g:
        return render(request, 'courseware/glossary.html', {'glossary': g, 'course': course})
    else:
        raise Http404

def builtins(request, course_slug):
    """ View function to display the glossary for a given course
    """
    course = get_object_or_404(Course, slug=course_slug)
    b = course.builtins
    if b:
        return render(request, 'courseware/builtins.html', {'collection': b, 'course': course})
    else:
        raise Http404

def video_index(request, course_slug):
    """ Index view of all video modules in the course
    """
    video_type = ContentType.objects.get_for_model(VideoLesson)
    try:
        course = Course.objects.get(slug=course_slug)
        units = course.unit_set.filter(module__content_type__pk=video_type.pk).distinct()
    except:
        raise Http404
    context = RequestContext(request, {'units': units})
    return render(request, 'courseware/video_index.html', context_instance=context)


def unit(request, course_slug, unit_number):
    """ View function to display a list of modules in the unit
    """
    try:
        unit = Unit.objects.filter(course__slug=course_slug).get(position=unit_number)
    except Unit.DoesNotExist:
        raise Http404

    # The course is used in rendering the template
    course = unit.course
    module_list = unit.module_set.order_by('position')

    # Special case for single-module units:
    if len(module_list) == 1:
        return _module(request, course, unit, module_list[0])

    # Determine the next unit
    next_u = None
    units = course.unit_set.all()
    index = 0
    for i in range(0, len(units)):
        if units[i].id == unit.id:
            index = i
    if index < len(units) - 1:
        next_u = units[index+1]

    print(next_u)

    context = RequestContext(request,
            {'course': course, 'unit': unit, 'next_unit': next_u, 'module_list':module_list})
    return render(request, "courseware/unit_index.html", context_instance=context)

def module(request, course_slug, unit_number, module_number):
    """ View function to display a given module

    It is necessary to determine the type of model, and choose the correct template.
    In addition, data is serialized and passed to the template.
    """
    try:
        module = Module.objects.filter(unit__course__slug=course_slug)
        module = module.filter(unit__position=unit_number).get(position=module_number)
    except Module.DoesNotExist:
        raise Http404

    # The course and unit are used in rendering the template
    course = module.unit.course
    unit = module.unit

    return _module(request, course, unit, module)


def transcript(request, course_slug, unit_number, module_number):
    """ View function to display a given module

    It is necessary to determine the type of model, and choose the correct template.
    In addition, data is serialized and passed to the template.
    """
    try:
        module = Module.objects.filter(unit__course__slug=course_slug)
        module = module.filter(unit__position=unit_number).get(position=module_number)
    except Module.DoesNotExist:
        raise Http404
     
    # This view only applies to a VideoLesson
    if module.content_type.model != 'videolesson':
        raise Http404

    # The course and unit are used in rendering the template
    course = module.unit.course
    unit = module.unit
    # Determine the previous and next modules
    (prev_m, next_m) = _get_sibling_modules(unit, module)

    context = RequestContext(request,
                             {'course': course, 'unit': unit, 'module': module,
                                 module.content_type.model: module.content_object,
                                 'next_module': next_m, 
                                 'prev_module': prev_m, 
                                 },
                             )
    return render(request, 'courseware/video_transcript.html', context_instance=context)

def _module(request, course, unit, module):
    """ Helper function to display a module
    
    This provides the actual rendering after its data has been retrieved.
    """

    # Determine the previous and next modules
    (prev_m, next_m) = _get_sibling_modules(unit, module)
    
    serialized = {}
    template_name = module.content_type.model
    if module.content_type.model == 'quiz':
        serialized = serialize_quiz(module.content_object)
    if module.content_type.model == 'exploration':
        serialized = serialize_exploration(module.content_object)
        #XXX Should probably refer to the constant associated with the class
        if module.content_object.language == 'web':
            template_name = 'web-' + template_name
    if module.content_type.model == 'slideshow':
        serialized = serialize_slideshow(module.content_object)
    context = RequestContext(request,
                             {'course': course, 'unit': unit, 'module': module,
                                 module.content_type.model: module.content_object,
                                 'serialized': serialized,
                                 'next_module': next_m, 
                                 'prev_module': prev_m, 
                                 },
                             [pysock_context])
    template = "courseware/%s.html" % template_name
    return render(request, template, context_instance=context)

def _get_sibling_modules(unit, module):
    """ Helper function to get next and previous modules in a unit
    """
    prev_m = None
    next_m = None
    modules = unit.module_set.all()
    index = 0
    for i in range(0, len(modules)):
        if modules[i].id == module.id:
            index = i
    if index > 0:
        prev_m = modules[index-1]
    if index < len(modules) - 1:
        next_m = modules[index+1]

    return (prev_m, next_m)

