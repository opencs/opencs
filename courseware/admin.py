from django.contrib import admin

from courseware.models import Course, Unit, Module

@admin.register(Module)
class ModuleAdmin(admin.ModelAdmin):
    list_display = ('content_type', 'icon', 'position', 'unit', 'course')
    list_editable = ('icon', 'position', 'unit',)
    list_filter = ('unit__course__title', 'content_type',)
    ordering = ('unit', 'position')

    def course(self, obj):
        c = obj.unit.course
        return c.title
    course.admin_order_field = 'unit__course__slug'

class ModuleInline(admin.StackedInline):
    model = Module
    extra = 0

@admin.register(Unit)
class UnitAdmin(admin.ModelAdmin):
    list_display = ('__str__', 'course', 'position')
    list_editable = ('course', 'position')
    inlines = [ModuleInline, ]


@admin.register(Course)
class CourseAdmin(admin.ModelAdmin):
    list_display = ('title', 'position', 'hidden',)
    list_editable = ('position', 'hidden',)

    fields = ('title',
              'description',
              'hidden',
              'position',
              'glossary',
              'builtins',
              'logo',
              )

class ModuleContentAdmin(admin.ModelAdmin):
    """ Base class for admin interfaces for any module content

    Main use is to be imported by other admin modules.
    """
    list_display = ('title', 'course', 'unit', 'module')
    list_filter = (
            'modules__unit__course__title',
            )

    def course(self, obj):
        m = obj.modules.all()
        if m:
            m = m[0]
            return m.unit.course.title
        else:
            return "unassigned"
    course.admin_order_field = 'modules__unit__course__slug'
    def unit(self, obj):
        m = obj.modules.all()
        if m:
            m = m[0]
            return m.unit.position
        else:
            return "unassigned"
    unit.admin_order_field = 'modules__unit__position'
    def module(self, obj):
        m = obj.modules.all()
        if m:
            m = m[0]
            return m.position
        else:
            return "unassigned"
    module.admin_order_field = 'modules__position'

