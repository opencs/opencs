#!/usr/bin/env bash

python3.4 /srv/opencs/manage.py import_builtins  "$@" --settings=OpenCS.settings.production
