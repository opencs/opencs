#!/usr/bin/env bash

python3.4 /srv/opencs/manage.py check_glossary  "$@" --settings=OpenCS.settings.production
